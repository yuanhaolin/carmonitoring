//
//  custom.m
//  ChinaNewsByIphone
//
//  Created by Symbio on 1/19/12.
//  Copyright 2012 symbio. All rights reserved.
//

#import "Custom.h"

@implementation UIImage(UIImageUIUitl)

+ (UIImage *)imageNamedCustom:(NSString *)name
{
	UIImage *img=[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:name]];
	return img;
}

@end

@implementation UINavigationBar (Custom)
- (void)drawRect:(CGRect)rect {
	[[UIImage imageNamed:@"tit_bg.png"] drawInRect:CGRectMake(0, 0, 320, 44)];
}

@end

@implementation UIToolbar (Custom)
- (void)setBackgroundViewToDefault {
    UIImageView *bgImageView = (UIImageView *)[self viewWithTag:101];
    if (nil == bgImageView) {
        bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        [bgImageView setTag:1000001];
        [bgImageView setImage:[UIImage imageNamed:@"tit_bg.png"]];
        [self addSubview:bgImageView];
        [bgImageView release];
    }
    
//    [self sendSubviewToBack:bgImageView];
}

- (void)drawRect:(CGRect)rect {
	[[UIImage imageNamed:@"tit_bg.png"] drawInRect:CGRectMake(0, 0, 320, 44)];
}

@end

@implementation UITableView (Private)
+ (void) showDefaultLoadView {
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(<#CGFloat x#>, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)]
}

@end