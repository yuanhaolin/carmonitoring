//
//  CustomTabBar.h
//  PowerSupply
//
//  Created by Evan Tan on 5/9/12.
//  Copyright (c) 2012 symbio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomTabBarDelegate
- (void)customTabBar:(id)customTabBar selectIndex:(int)selectIndex;

@end

@interface CustomTabBar : UITabBarController <UIGestureRecognizerDelegate> {
    UIImage *_imageBG;
    UIImage *_imageSelectedBG;
    
    NSArray *_selectedImages;
    
    id _myDelegate;
}
@property (nonatomic,retain) UIImage *imageBG;
@property (nonatomic,retain) UIImage *imageSelectedBG;
@property (nonatomic,retain) NSArray *selectedImages;
@property (nonatomic,retain) id myDelegate;

- (void)setCustomSelectIndex:(int)index_;

@end