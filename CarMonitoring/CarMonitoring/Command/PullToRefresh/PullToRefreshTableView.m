//
//  PullToRefreshTableView.m
// 
//  Version 1.0
//
//  Created by hsit on 12-1-30.
//  Copyright (c) 2012年 QQ:115940737. All rights reserved.
//

#import "PullToRefreshTableView.h"
#import <QuartzCore/QuartzCore.h>
#import "AppManager.h"

/**
 *
 * StateView 顶部和底部状态视图
 *
 **/

@implementation StateView

@synthesize indicatorView;
@synthesize arrowView;
@synthesize stateLabel;
@synthesize timeLabel;
@synthesize viewType;
@synthesize currentState;

- (id)initWithFrame:(CGRect)frame viewType:(int)type{
    CGFloat width = frame.size.width;
    
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, width, k_STATE_VIEW_HEIGHT)];
    
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        //  设置当前视图类型
        viewType = type == k_VIEW_TYPE_HEADER ? k_VIEW_TYPE_HEADER : k_VIEW_TYPE_FOOTER;
        self.backgroundColor = [UIColor clearColor];
        
        //  初始化加载指示器（菊花圈）  
        indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((k_STATE_VIEW_INDICATE_WIDTH - 20) / 2, (k_STATE_VIEW_HEIGHT - 20) / 2, 20, 20)];
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        indicatorView.hidesWhenStopped = YES;
        [self addSubview:indicatorView];
        
        //  初始化箭头视图
        arrowView = [[UIImageView alloc] initWithFrame:CGRectMake((k_STATE_VIEW_INDICATE_WIDTH - 32) / 2, (k_STATE_VIEW_HEIGHT - 32) / 2, 20, 32)];
        NSString * imageNamed = type == k_VIEW_TYPE_HEADER ? @"whiteArrow.png" : @"whiteArrow_up.png";
        UIImage *image = [UIImage imageNamed:imageNamed];
        arrowView.image = image;
        [self addSubview:arrowView];
        
        //  初始化状态提示文本
        stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 20)];
        stateLabel.backgroundColor = [UIColor clearColor];
        stateLabel.textAlignment = UITextAlignmentCenter;
        stateLabel.text = type == k_VIEW_TYPE_HEADER ? @"下拉可以刷新" : @"上拖加载更多";
        stateLabel.font = [UIFont systemFontOfSize:14];
        [stateLabel setShadowColor:UIColorFromRGB(0x222222)];
        [stateLabel setShadowOffset:CGSizeMake(1., 1.)];
        [self addSubview:stateLabel];
        [stateLabel setTextColor:[UIColor grayColor]];
        
        //  初始化更新时间提示文本
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 14, width, k_STATE_VIEW_HEIGHT - 20)];
        timeLabel.font = [UIFont systemFontOfSize:14.0f];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textAlignment = UITextAlignmentCenter;
        timeLabel.text = @"-";
        [timeLabel setTextColor:[UIColor grayColor]];
        [timeLabel setShadowColor:UIColorFromRGB(0x222222)];
        [timeLabel setShadowOffset:CGSizeMake(1., 1.)];
        [self addSubview:timeLabel];
    }
    return self;
}

- (void)changeState:(int)state{
    [indicatorView stopAnimating];
    arrowView.hidden = NO;
    [UIView beginAnimations:nil context:nil];
    //brant
    [self setHidden:NO];
    //end
    switch (state) {
        case k_PULL_STATE_NORMAL:
            currentState = k_PULL_STATE_NORMAL;
            stateLabel.text = viewType == k_VIEW_TYPE_HEADER ? @"下拉可以刷新" : @"上拖加载更多";
            stateLabel.font = [UIFont systemFontOfSize:14];
            //  旋转箭头
            arrowView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            break;
        case k_PULL_STATE_DOWN:
            currentState = k_PULL_STATE_DOWN;
            stateLabel.text = @"释放刷新数据";
            stateLabel.font = [UIFont systemFontOfSize:14];
            arrowView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            break;
            
        case k_PULL_STATE_UP:
            currentState = k_PULL_STATE_UP;
            stateLabel.text = @"释放加载数据";
            stateLabel.font = [UIFont systemFontOfSize:14];
            arrowView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            break;
            
        case k_PULL_STATE_LOAD:
            currentState = k_PULL_STATE_LOAD;
            stateLabel.text = viewType == k_VIEW_TYPE_HEADER ? @"正在刷新.." : @"正在加载..";
            stateLabel.font = [UIFont systemFontOfSize:14];            
            [indicatorView startAnimating];
            arrowView.hidden = YES;
            break;
            
        case k_PULL_STATE_END:
            currentState = k_PULL_STATE_END;
            stateLabel.text = viewType == k_VIEW_TYPE_HEADER ? stateLabel.text : @"已加载全部数据";
            stateLabel.font = [UIFont systemFontOfSize:14];     
            //brant
            [self setHidden:YES];
            //end
            arrowView.hidden = YES;
            break;
            
        default:
            currentState = k_PULL_STATE_NORMAL;
            stateLabel.text = viewType == k_VIEW_TYPE_HEADER ? @"下拉可以刷新" : @"上拖加载更多";
            stateLabel.font = [UIFont systemFontOfSize:14];            
            arrowView.layer.transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            break;
    }
    [UIView commitAnimations];
}

- (void)updateTimeLabel{
    NSDate * date = [NSDate date];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterFullStyle];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    timeLabel.text = [NSString stringWithFormat:@"更新于 %@", [formatter stringFromDate:date]];
    timeLabel.font = [UIFont systemFontOfSize:14];
    [formatter release];
}

- (void)dealloc{
    [indicatorView release];
    [arrowView release];
    [stateLabel release];
    [timeLabel release];
    
    [super dealloc];
}

- (void)setIsShowIcon:(BOOL)yesOrNo {
    if (yesOrNo) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width-51.5)/2, -65, 51.5, 57)];
        [imageView setTag:1000001];
        [imageView setImage:[UIImage imageNamed:@"sx.png"]];
        [self addSubview:imageView];
        [imageView release];
    }
    else {
        [[self viewWithTag:1000001] removeFromSuperview];
    }
}

@end


/**
 *
 * PullToRefreshTableView 拖动刷新/加载 表格视图
 *
 **/


@implementation PullToRefreshTableView
@synthesize footerView;

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIView *viewBGHeader = [[UIView alloc] initWithFrame:CGRectMake(0, -400, self.frame.size.width, 400)];
        [viewBGHeader setBackgroundColor:UIColorFromRGB(0x323232)];
        [self addSubview:viewBGHeader];
        [viewBGHeader release];
        
        headerView = [[StateView alloc] initWithFrame:CGRectMake(0, -40, self.frame.size.width, self.frame.size.height) viewType:k_VIEW_TYPE_HEADER];
        [self addSubview:headerView];
        [headerView setIsShowIcon:YES];
        
        UIView *view = [[[UIView alloc] init] autorelease];
        [self setTableFooterView:view];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        //IOS5 +
//        [self.panGestureRecognizer addTarget:self action:@selector(panPiece:)];
        
        UIView *viewBGHeader = [[UIView alloc] initWithFrame:CGRectMake(0, -400, self.frame.size.width, 400)];
        [viewBGHeader setBackgroundColor:UIColorFromRGB(0x323232)];
        [self addSubview:viewBGHeader];
        [viewBGHeader release];
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        headerView = [[StateView alloc] initWithFrame:CGRectMake(0, -40, frame.size.width, frame.size.height) viewType:k_VIEW_TYPE_HEADER];
//        footerView = [[StateView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 30) viewType:k_VIEW_TYPE_FOOTER];
        [self addSubview:headerView];
        [headerView setIsShowIcon:YES];
        
        
//        UIView *view = [[[UIView alloc] init] autorelease];
        [self setTableFooterView:footerView];
        
    }
    return self;
}


- (void)reloadData{
    [super reloadData];
    
    NSInteger dataSourceHeight = [self numberOfRowsInSection:0]*self.rowHeight;
    NSInteger tableViewHeight = self.frame.size.height;
    
    if (dataSourceHeight>tableViewHeight) {
        if (!footerView) {
//             footerView = [[StateView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) viewType:k_VIEW_TYPE_FOOTER];
            [self setTableFooterView:footerView];
        }
    }else {
//        if (footerView) {
//            [footerView removeFromSuperview];
//        }
    }
}

- (void)dealloc{
    [_reloadDataTimer invalidate];_reloadDataTimer=nil;
    [headerView release];headerView=nil;
    [footerView release];footerView=nil;
    [super dealloc];
}

- (void)tableViewDidDragging{
    CGFloat offsetY = self.contentOffset.y;
    if (offsetY<-40)
        [headerView setFrame:CGRectMake(0, offsetY, headerView.frame.size.width, headerView.frame.size.height)];
    //  判断是否正在加载
    if (headerView.currentState == k_PULL_STATE_LOAD ||
        footerView.currentState == k_PULL_STATE_LOAD) {
        return;
    }
    //  改变“下拉可以刷新”视图的文字提示
    if (offsetY < -k_STATE_VIEW_HEIGHT - 10) {
        [headerView changeState:k_PULL_STATE_DOWN];
    } else {
        [headerView changeState:k_PULL_STATE_NORMAL];
    }
    //  判断数据是否已全部加载
    if (footerView.currentState == k_PULL_STATE_END) {
        return;
    }
    //  计算表内容大小与窗体大小的实际差距
    CGFloat differenceY = self.contentSize.height > self.frame.size.height ? (self.contentSize.height - self.frame.size.height) : 0;
    //  改变“上拖加载更多”视图的文字提示
    if (offsetY > differenceY + k_STATE_VIEW_HEIGHT / 3 * 2) {
        [footerView changeState:k_PULL_STATE_UP];
    } else {
        [footerView changeState:k_PULL_STATE_NORMAL];
    }
}

- (int)tableViewDidEndDragging{
    CGFloat offsetY = self.contentOffset.y;
    
    //  判断是否正在加载数据
    if (headerView.currentState == k_PULL_STATE_LOAD ||
        footerView.currentState == k_PULL_STATE_LOAD) {
        return k_RETURN_DO_NOTHING;
    }
    //  改变“下拉可以刷新”视图的文字及箭头提示
    if (offsetY < -k_STATE_VIEW_HEIGHT - 10) {
        [headerView changeState:k_PULL_STATE_LOAD];
        self.contentInset = UIEdgeInsetsMake(k_STATE_VIEW_HEIGHT, 0, 0, 0);
        return k_RETURN_REFRESH;
    }
    //  改变“上拉加载更多”视图的文字及箭头提示
    CGFloat differenceY = self.contentSize.height > self.frame.size.height ? (self.contentSize.height - self.frame.size.height) : 0;
    if (footerView.currentState != k_PULL_STATE_END && 
        offsetY > differenceY + k_STATE_VIEW_HEIGHT / 3 * 2) {
        [footerView changeState:k_PULL_STATE_LOAD];
        return k_RETURN_LOADMORE;
    }
    
    return k_RETURN_DO_NOTHING;
}

#pragma mark UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSLog(@"%lf",scrollView.contentOffset.y);
}

- (void)reloadDataTimer:(NSTimer *)number {
    if (nil == headerView) {
        return;
    }
    BOOL dataIsAllLoaded = [[number userInfo] boolValue];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    self.contentInset = UIEdgeInsetsZero;
    [UIView commitAnimations];
    
    [headerView changeState:k_PULL_STATE_NORMAL];
    //  如果数据已全部加载，则禁用“上拖加载”
    if (dataIsAllLoaded) {
        [footerView changeState:k_PULL_STATE_END];
        UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10)] autorelease];
        [self setTableFooterView:view];
    } else {
        [self setTableFooterView:footerView];
        [footerView changeState:k_PULL_STATE_NORMAL];
    }
    //  更新时间提示文本
    [headerView updateTimeLabel];
    [footerView updateTimeLabel];
    
    [self reloadData];
    _reloadDataTimer = nil;
//    [self setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)reloadData:(BOOL)dataIsAllLoaded{
//    [_reloadDataTimer invalidate];_reloadDataTimer=nil;
//    _reloadDataTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"reloadDataTimer:") userInfo:[NSNumber numberWithBool:dataIsAllLoaded] repeats:NO];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    self.contentInset = UIEdgeInsetsZero;
    [UIView commitAnimations];
    
    [headerView changeState:k_PULL_STATE_NORMAL];
    //  如果数据已全部加载，则禁用“上拖加载”
    if (dataIsAllLoaded) {
        [footerView changeState:k_PULL_STATE_END];
        UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10)] autorelease];
        [self setTableFooterView:view];
    } else {
        [self setTableFooterView:footerView];
        [footerView changeState:k_PULL_STATE_NORMAL];
    }
    //  更新时间提示文本
    [headerView updateTimeLabel];
    [footerView updateTimeLabel];
    
    [self reloadData];
}

- (void)beginLoad {
//    [self setContentOffset:CGPointMake(0, -60) animated:YES];
    [self reloadData];
    [headerView changeState:k_PULL_STATE_LOAD];
    [footerView changeState:k_PULL_STATE_END];
    self.contentInset = UIEdgeInsetsMake(k_STATE_VIEW_HEIGHT, 0, 0, 0);
}

//#pragma mark -
//#pragma mark UIView methods
//- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
//    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
//        //        beginPoint = [gestureRecognizer locationInView:self.view];
//    }
//	else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
//        CGPoint touchPoint = [gestureRecognizer locationInView:self];
//        NSLog(@"%lf",self.contentOffset.y);
//	}
//	else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
//        
//	}
//}
//
//- (void)panPiece:(UIPanGestureRecognizer *)gestureRecognizer
//{
//    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
//}

@end