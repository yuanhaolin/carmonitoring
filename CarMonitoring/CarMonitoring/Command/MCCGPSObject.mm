//
//  MCCGPSObject.m
//  MyChevyCar
//
//  Created by Work on 5/11/13.
//  Copyright (c) 2013 YY. All rights reserved.
//

#import "MCCGPSObject.h"
#import <MapKit/MapKit.h>

@interface MCCGPSObject () {
    CLLocationManager *_locationManger;
    CLGeocoder *_geCoder;
    
    NSString *_cityName;
    CLLocationCoordinate2D _currentLocationCoordinate2D;
    
    NSString *_strAddress;
}

@end

static MCCGPSObject *_pGPSObject;
@implementation MCCGPSObject
@synthesize coor;
@synthesize canLocateCity;

+ (id)getInstance {
    @synchronized (self) {
        if (nil == _pGPSObject) {
            _pGPSObject = [[MCCGPSObject alloc] init];
        }
    }
    
    return _pGPSObject;
}

//- (void)viewDidLoad {
//    [super viewDidLoad];
//}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [_map viewWillAppear];
//    _map.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
//}
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [_map viewWillDisappear];
//    _map.delegate = nil; // 不用时，置nil
//}

- (void)startGPS {
    _locationManger = [[CLLocationManager alloc] init];
    _locationManger.delegate = self;
    
    _locationManger.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManger startUpdatingLocation];
    
//    if (nil == _map) {
//        searchAddress = [[BMKSearch alloc] init];
//        
//        _map = [[BMKMapView alloc] init];
//        _map.showsUserLocation = YES;
//        [self addSubview:_map];
//        [_map release];
//    }
//    [_map viewWillAppear];
//    [_map setDelegate:self];
//    searchAddress.delegate = self;
}

- (void)pauseGPS {
    [_map viewWillDisappear];
    [_map setDelegate:nil];
    searchAddress.delegate = nil;
}

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    [_locationManger release];
    [_cityName release];
    [_strAddress release];
    [super dealloc];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation

{
    CLLocationDistance l = newLocation.coordinate.latitude;//得到经度
    CLLocationDistance v = newLocation.coordinate.longitude;//得到纬度

//    NSLog(@"%f %f", l,v);
//    CLLocation *new = [[CLLocation alloc] initWithLatitude: 11.0 longitude: 12.0];
//
//    CLLocationDistance u = [newLocation distanceFromLocation: new];
//    NSLog(@"%f", u);
    
    _currentLocationCoordinate2D = newLocation.coordinate;
    
    [self startedReverseGeoderWithLatitude: l longitude: v];
}

- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error
{
    if (error==0) {
        //启动GPS
        if ([result.addressComponent.city length]<1) {
            return;
        }
        NSString *tmpCity=[result.addressComponent.city substringToIndex:result.addressComponent.city.length-1];
        MCCGPSObject *tmp = [MCCGPSObject getInstance];
        [tmp setCurrentCityName:tmpCity];
        
        NSString *tmpAddress = result.strAddr;
        [_strAddress release];_strAddress=nil;
        _strAddress = [tmpAddress retain];
        
        if (![[[NSUserDefaults standardUserDefaults]  objectForKey:@"isOpenRemote"] isEqualToString:@"on"]) {
            canLocateCity = NO;
        }else {
            canLocateCity = YES;
        }
    }
}

- (void)startedReverseGeoderWithLatitude:(double)latitude longitude:(double)longitude{
    CLLocationCoordinate2D coordinate2D;
    coordinate2D.longitude = longitude;
    coordinate2D.latitude = latitude;
    
    @synchronized (self) {
        _currentLocationCoordinate2D = coordinate2D;
    }
    
    if (isIOS5) {
        CLGeocodeCompletionHandler handler = ^(NSArray *place, NSError *error)
        {
            for (CLPlacemark *placemark in place)
            {
                @synchronized (self) {
                    [_cityName release];_cityName=nil;
                    _cityName = [placemark.locality retain];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_CITY object:nil];
                }
                
//                cityStr=placemark.thoroughfare;
//                cityName=placemark.locality;
//                NSLog(@"城市：%@",cityStr);
//                _street.text = cityStr;//根据经纬度获得的所在街道(下面loc)
//                _city.text = cityName;//根据经纬度获得的所在城市(下面loc)
                break;
            }
            [_geCoder release];_geCoder=nil;
        };
        
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:coordinate2D.latitude longitude:coordinate2D.longitude];
        
        _geCoder = [[CLGeocoder alloc] init];
        [_geCoder reverseGeocodeLocation:loc completionHandler:handler];
        [loc release];
    }
}

- (BMKOverlayView *)mapView:(BMKMapView *)mapView viewForOverlay:(id <BMKOverlay>)overlay
{
    return nil;
}

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    return nil;
}

- (void)mapView:(BMKMapView *)mapView annotationViewForBubble:(BMKAnnotationView *)view;
{
    return;
}

-(void)mapView:(BMKMapView *)mapView didUpdateUserLocation:(BMKUserLocation *)userLocation{
    if (userLocation != nil) {
        coor.latitude= userLocation.location.coordinate.latitude;
        coor.longitude= userLocation.location.coordinate.longitude;
        
        [searchAddress reverseGeocode:coor];
        
        mapView.showsUserLocation = YES;
	}
}

-(void)mapView:(BMKMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{
    //NSLog(@"fail");
}

-(void)mapViewDidStopLocatingUser:(BMKMapView *)mapView
{
    //NSLog(@"stop location");
}

#pragma mark attr

-(void)setCurrentCityName:(NSString *)tmpCity {
    [_cityName release];_cityName=nil;
    _cityName = [tmpCity retain];
    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_CITY object:nil];
}

- (NSString *)getCurrentAddress {
    return _strAddress;
}

- (NSString *)getCurrentCityName {
    return _cityName;
}

- (CLLocationCoordinate2D)getLocationCoordinate2D {
    return _currentLocationCoordinate2D;
}

- (void)mapViewWillStartLocatingUser:(BMKMapView *)mapView
{
	NSLog(@"start locate");
}

- (BOOL)isGPS {
    if (self.coor.latitude == 0 && self.coor.longitude == 0) {
        return NO;
    }
    return YES;
}

@end
