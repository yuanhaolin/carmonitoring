//
//  CMMyUserInfoManager.m
//  CarMonitoring
//
//  Created by Work on 1/9/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMMyUserInfoManager.h"

static CMMyUserInfoManager *_pMyUserInfo;
@implementation CMMyUserInfoManager

+ (CMMyUserInfoManager *)getInstance {
    @synchronized (self) {
        if (nil == _pMyUserInfo) {
            _pMyUserInfo = [[CMMyUserInfoManager alloc] init];
        }
    }
    
    return _pMyUserInfo;
}

- (UserInfo_Builder *)getUserInfo {
    NSString *path = doc_cache_path(@"myUserInfo");
    NSData *protobufData = [NSData dataWithContentsOfFile:path];
    
    UserInfo *protobuf = [UserInfo parseFromData:protobufData];
    
    UserInfo_Builder *userInfo = [protobuf toBuilder];
    
    return userInfo;
}

- (void)setUserInfo:(NSData *)data {
    NSString *path = doc_cache_path(@"myUserInfo");
    [data writeToFile:path atomically:YES];
}

- (VehicleInfo *)getVehicleInfoFromVehicleId:(int32_t)vehicleId {
    VehicleInfo *vInfo = nil;
    
    PBArray *userInfoVehicles = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    for (VehicleInfo *myUserVehicle in userInfoVehicles) {
        if (vehicleId == myUserVehicle.vehicleId) {
            vInfo = myUserVehicle;
        }
    }
    
    return vInfo;
}

@end
