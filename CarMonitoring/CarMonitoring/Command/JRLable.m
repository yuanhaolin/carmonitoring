//
//  JRLable.m
//  JRPiazza
//
//  Created by yuan haolin on 11-8-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "JRLable.h"

@interface JRLable () {
    UIView *_view;
}

@end

@implementation JRLable
@synthesize lable=_lable;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        _lable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
		[_lable setNumberOfLines:0];
		[_lable setBackgroundColor:[UIColor clearColor]];
		[self addSubview:_lable];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
	[_lable release];
    [super dealloc];
}

#pragma mark attr
- (void)setText:(NSString *)text {
    if (nil == _lable) {
        _lable = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
		[_lable setNumberOfLines:0];
		[_lable setBackgroundColor:[UIColor clearColor]];
		[self addSubview:_lable];
    }
	[_lable setText:text];
}

- (void)autoContentSize {
	NSString *text = _lable.text;
	
	CGSize size = [text sizeWithFont:_lable.font constrainedToSize:CGSizeMake(self.frame.size.width, 10000) lineBreakMode:NSLineBreakByCharWrapping];

	[_lable setFrame:CGRectMake(0, 0, self.frame.size.width, size.height)];
    
	[self setContentSize:CGSizeMake(self.frame.size.width, size.height)];
}

- (void)autoContentSelfFrame {
    NSString *text = _lable.text;
	
	CGSize size = [text sizeWithFont:_lable.font constrainedToSize:CGSizeMake(self.frame.size.width, 10000) lineBreakMode:NSLineBreakByCharWrapping];
    
    float hig = size.height>=self.frame.size.height?self.frame.size.height:size.height;
    
	[_lable setFrame:CGRectMake(0, 0, self.frame.size.width, hig)];
}

- (CGRect)lableFrame {
    return CGRectMake(0, 0, self.frame.size.width, self.contentSize.height);
}

+ (CGSize)textSize:(CGSize)widthSize text:(NSString *)strtext font:(UIFont *)font {
    CGSize size = [strtext sizeWithFont:font constrainedToSize:widthSize lineBreakMode:NSLineBreakByCharWrapping];
    
	return size;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    if (nil == _view) {
        _view = [[UIView alloc] initWithFrame:self.bounds];
        [_view setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_view];
        [_view release];
    }
    [_view setFrame:self.bounds];
    [self bringSubviewToFront:_view];
}

@end
