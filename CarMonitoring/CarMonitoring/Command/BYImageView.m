//
//  SSImageView.m
//  Senzari_iPhone
//
//  Created by Brant.Yuan on 11-6-1.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define RAND(_max_)  rand() % (_max_+1);

#define VIEW_ACTIVITY_TAG	10000001
#define IMAGEVIEW_TAG		100012
#define BG_VIEW_TAG         100013

#import "BYImageView.h"
#import "ASINetworkQueue.h"
#import "MyDefaultProgressView.h"
#import "ASINetworkQueue.h"

@interface BYImageView () {
    float _bgViewY;
    
    MyDefaultProgressView *_progressView;
}

@end

@interface BYImageView (Private)
- (void)changeImageView;

@end

@implementation BYImageView (Private)
- (void)changeImageView {
    CGSize bgImgSize = _bgImageView.image.size;
    if (bgImgSize.width/selfSize.width>bgImgSize.height/selfSize.height) {
        //以宽为标准设置size
        if (bgImgSize.width/selfSize.width>=bgImgSize.height/selfSize.height) {
			//以宽为标准设置size
			float wid = selfSize.width;
			
			float scale = selfSize.width/bgImgSize.width;
			float hig = scale*bgImgSize.height;
			
			[_bgImageView setFrame:CGRectMake((self.frame.size.width-wid)/2, (self.frame.size.height-hig)/2, wid, hig)];
		}
		else {
			//以高为标准设置size
			float hig = selfSize.height;
			
			float scale = selfSize.height/bgImgSize.height;
			float wid = scale*bgImgSize.width;
			
			[_bgImageView setFrame:CGRectMake((self.frame.size.width-wid)/2, (self.frame.size.height-hig)/2, wid, hig)];
		}
//        float wid = selfSize.width;
//        
//        float scale = selfSize.width/bgImgSize.width;
//        int hig = scale*bgImgSize.height;
//        
//        if (0 == _bgViewY) {
//            [_bgImageView setFrame:CGRectMake((self.frame.size.width-wid)/2, (self.frame.size.height-hig)/2, wid, hig)];
//        }
//        else {
//            [_bgImageView setFrame:CGRectMake((self.frame.size.width-wid)/2, _bgViewY, wid, hig)];
//        }
//    }
//    else {
//        //以高为标准设置size
//        float hig = selfSize.height;
//        
//        float scale = selfSize.height/bgImgSize.height;
//        int wid = scale*bgImgSize.width;
//        
//        if (0 == _bgViewY) {
//            [_bgImageView setFrame:CGRectMake((self.frame.size.width-wid)/2, (self.frame.size.height-hig)/2, wid, hig)];
//        }
//        else {
//            [_bgImageView setFrame:CGRectMake((self.frame.size.width-wid)/2, _bgViewY, wid, hig)];
//        }
    }
    
	UIImageView *imageView = (UIImageView *)[self viewWithTag:IMAGEVIEW_TAG];
	if (nil == imageView.image) {
        [_bgImageView setAlpha:1.];
		return;
	}
    else {
        [_bgImageView setAlpha:0.];
    }
	
	CGSize imgSize = [imageView.image size];
	
	if (BYImageAll == _type) {
		if (imgSize.width/selfSize.width>=imgSize.height/selfSize.height) {
			//以宽为标准设置size
			float wid = selfSize.width;
			
			float scale = selfSize.width/imgSize.width;
			float hig = scale*imgSize.height;
			
			[imageView setFrame:CGRectMake((self.frame.size.width-wid)/2, (self.frame.size.height-hig)/2, wid, hig)];
		}
		else {
			//以高为标准设置size
			float hig = selfSize.height;
			
			float scale = selfSize.height/imgSize.height;
			float wid = scale*imgSize.width;
			
			[imageView setFrame:CGRectMake((self.frame.size.width-wid)/2, (self.frame.size.height-hig)/2, wid, hig)];
		}
	}
	else if (BYImageClipAutoFrame == _type){
		if (0 == selfSize.width || 0 == selfSize.height) {
			return;
		}
		
		float wid = 0;
		float hig = 0;
		float x=0;
		float y=0;
		if (imgSize.width/selfSize.width<=imgSize.height/selfSize.height) {
			//以宽为标准设置size
			wid = self.frame.size.width;
			hig = imgSize.height*(self.frame.size.width/imgSize.width);
			
			int tx = (self.frame.size.width-wid);
			x = RAND(tx);
			
			int ty = (self.frame.size.height-hig);
			y = RAND(ty);
		}
		else {
			wid = imgSize.width*(self.frame.size.height/imgSize.height);
			hig = self.frame.size.height;
			
			int tx = (self.frame.size.width-wid);
			x = RAND(tx);
			
			int ty = (self.frame.size.height-hig);
			y = RAND(ty);
		}
		[imageView setFrame:CGRectMake(-x, -y, wid, hig)];
	}
	else {
		if (0 == selfSize.width || 0 == selfSize.height) {
			return;
		}
		
		float wid = 0;
		float hig = 0;
		float x=0;
		float y=0;
		if (imgSize.width/selfSize.width<=imgSize.height/selfSize.height) {
			//以宽为标准设置size
			wid = self.frame.size.width;
			hig = imgSize.height*(self.frame.size.width/imgSize.width);
			
			x = 0;
			
			y =0;
		}
		else {
			wid = imgSize.width*(self.frame.size.height/imgSize.height);
			hig = self.frame.size.height;
			
			x = 0;
			
			y = 0;
		}
		[imageView setFrame:CGRectMake(-x, -y, wid, hig)];
	}
    
    if ([_url length]>0) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *imgPath = [_service getImageWithLocal:_url];
        if (![fileManager fileExistsAtPath:imgPath]) {
            [self setBackgroundColor:[UIColor clearColor]];
        }
        else {
            [self setBackgroundColor:[UIColor clearColor]];
        }
    }
}

@end



@implementation BYImageView
@synthesize isLoaded=_isLoaded;
@synthesize isImaged=_isImaged;
@synthesize type=_type;
@synthesize action=_action;
@synthesize selector=_selector;
@synthesize delegate=_delegate;
@synthesize backgroundImageView=_bgImageView;
@synthesize isShowProgress;
@synthesize isShowActivietView;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
		selfSize = frame.size;
		
        _service = [[Service alloc] init];
		[_service setDelegate:self];
        
		[self setBackgroundColor:[UIColor clearColor]];
        
        _bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_bgImageView setTag:BG_VIEW_TAG];
        [_bgImageView setBackgroundColor:[UIColor clearColor]];
//        [_bgImageView setImage:[UIImage imageNamed:@"moren.png"]];
        [self addSubview:_bgImageView];
        [_bgImageView release];
		
		UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
		[imageView setTag:IMAGEVIEW_TAG];
		[imageView setBackgroundColor:[UIColor clearColor]];
		[self addSubview:imageView];
		[imageView release];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (void)dealloc {
	[_url release];
    [_action release];
	[_delegate release];
    [super dealloc];
}

- (void)removeFromSuperview {
	[_service.downQueue cancelAllOperations];
	[_service.downQueue reset];
	[_service release];
    
    [_progressView removeFromSuperview];
    [_progressView release];_progressView=nil;
	
	[super removeFromSuperview];
}

- (void)setType:(BYImageViewViewEnum)type {
    _type = type;
    
    if (nil == _bgImageView) {
        _bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [_bgImageView setTag:BG_VIEW_TAG];
        [_bgImageView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_bgImageView];
        [_bgImageView release];
    }
}

#pragma mark attr
- (void)setIsLoaded:(BOOL)loaded {
	_isLoaded = loaded;
	
	if (loaded) {
		UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)[self viewWithTag:VIEW_ACTIVITY_TAG];
		[activityView stopAnimating];
		[activityView removeFromSuperview];
        
        [_progressView removeFromSuperview];
        [_progressView release];_progressView=nil;
	}
	else {
		int wid = 30;
		
		UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)[self viewWithTag:VIEW_ACTIVITY_TAG];
		if (nil == activityView && self.isShowActivietView) {
			activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((self.frame.size.width-wid)/2, (self.frame.size.height-wid)/2, wid, wid)];
			[activityView setBackgroundColor:[UIColor clearColor]];
			[activityView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
			[activityView setTag:VIEW_ACTIVITY_TAG];
			[self addSubview:activityView];
			[activityView release];
		}
		
		[activityView startAnimating];
	}
}

- (void)setImageUrl:(NSString *)url
{
    if (nil ==_service) {
        _service = [[Service alloc] init];
		[_service setDelegate:self];
    }
    [self setClipsToBounds:YES];
    
    UIImageView *imageView = (UIImageView *)[self viewWithTag:IMAGEVIEW_TAG];
    if (nil == imageView) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [imageView setTag:IMAGEVIEW_TAG];
        [imageView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:imageView];
        [imageView release];
    }
    
    [imageView setImage:nil];
    
	[_url release];
	_url = nil;
	_url = [url retain];
    
    [_service resetDownQueue];
	
	if (0 == [url length] || nil == url) {
		_isImaged = NO;
	}
	else {
		_isImaged = YES;
	}
	
    [self setIsLoaded:NO];
    
    [self changeImageView];
    if (nil != _url) {
        NSString *imagePath = [_service getImageWithLocal:_url];
        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        if (nil == image) {
            if (nil == _progressView && self.isShowProgress) {
                _progressView = [[MyDefaultProgressView alloc] initWithFrame:CGRectMake(self.frame.size.width/3., (self.frame.size.height-20)/2., self.frame.size.width/3., 20)];
                [self addSubview:_progressView];
            }
            [_service getImage:_url delegate:_progressView];
            [_service.downQueue go];
        }
        else {
            [self setImage:image];
            [self setIsLoaded:YES];
        }
    }
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
	
	selfSize = frame.size;
    
    UIImageView *imageView = (UIImageView *)[self viewWithTag:IMAGEVIEW_TAG];
    [imageView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
	
	int wid = 30;
	UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)[self viewWithTag:VIEW_ACTIVITY_TAG];
	[activityView setFrame:CGRectMake((self.frame.size.width-wid)/2, (self.frame.size.height-wid)/2, wid, wid)];
	
	[self changeImageView];
}

#pragma mark SSService delegate
-(void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
	switch (tag) {
		case GET_IMAGE_TAG:
		{
			[self setIsLoaded:YES];
			
			UIImageView *imageView = (UIImageView *)[self viewWithTag:IMAGEVIEW_TAG];
			
			UIImage *img = [UIImage imageWithData:entity];
			
			[imageView setImage:img];
            
            [imageView setAlpha:0.];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDuration:0.3];
            [imageView setAlpha:1.];
            [UIView commitAnimations];
            
            [self changeImageView];
			if (0 != [(NSData *)entity length]) {
				if ([_delegate respondsToSelector:@selector(imageLoadFinish:)]) {
					[_delegate imageLoadFinish:self];
				}
			}
		}
			break;
	}
}

#pragma mark attr
- (void)setImage:(UIImage *)image {
	UIImageView *imageView = (UIImageView *)[self viewWithTag:IMAGEVIEW_TAG];
    if (nil == imageView) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [imageView setTag:IMAGEVIEW_TAG];
        [imageView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:imageView];
        [imageView release];
    }
    
	[imageView setImage:image];
	
	[self changeImageView];
    
    if ([_delegate respondsToSelector:@selector(imageLoadFinish:)]) {
        [_delegate imageLoadFinish:self];
    }
}

- (UIView *)imageView {
	UIImageView *imageView = (UIImageView *)[self viewWithTag:IMAGEVIEW_TAG];
	return imageView;
}

- (void)btnTapping:(id)sender {
	if ([_action respondsToSelector:_selector]) {
		[_action performSelector:_selector withObject:self];
	}
}

- (void)setSelector:(SEL)value {
	if (nil != value) {
		UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:NSSelectorFromString(@"btnTapping:")];
		tapGestureRecognizer.delegate = self;
		[self addGestureRecognizer:tapGestureRecognizer];
		[tapGestureRecognizer release];
	}
	_selector = value;
}

- (void)setBackgroundImageViewWithY:(float)y_ {
    _bgViewY = y_;
    [_bgImageView setFrame:CGRectMake(_bgImageView.frame.origin.x, _bgViewY, _bgImageView.frame.size.width, _bgImageView.frame.size.height)];
}

#pragma mark UITouch
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    [super touchesEnded:touches withEvent:event];
//
//    if ([_action respondsToSelector:_selector]) {
//        [_action performSelector:_selector withObject:self];
//    }
//}
@end
