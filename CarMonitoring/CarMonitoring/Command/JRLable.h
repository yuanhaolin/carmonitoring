//
//  JRLable.h
//  JRPiazza
//
//  Created by yuan haolin on 11-8-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface JRLable : UIScrollView {
	UILabel *_lable;
}
@property (nonatomic,retain) UILabel *lable;

- (void)setText:(NSString *)text;
- (void)autoContentSize;
- (void)autoContentSelfFrame;

- (CGRect)lableFrame;
+ (CGSize)textSize:(CGSize)widthSize text:(NSString *)strtext font:(UIFont *)font;

@end
