//
//  custom.h
//  ChinaNewsByIphone
//
//  Created by Symbio on 1/19/12.
//  Copyright 2012 symbio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UINavigationBar (Custom)

@end

@interface UIToolbar (Custom) 
- (void)setBackgroundViewToDefault;

@end

@interface UIImage(UIImageUIUitl)
+ (UIImage *)imageNamedCustom:(NSString *)name;
@end

@interface UITableView(Custom)
+ (void) showDefaultLoadView;

@end
