//
//  UIUitl.m
//  NoTableVideo
//
//  Created by haolin yuan on 5/13/12.
//  Copyright (c) 2012 symbio. All rights reserved.
//

#import "UIUitl.h"

@implementation UIUitl
+ (void)toFrameAnimation:(UIView *)view toFrame:(CGRect)rect {
    [UIView beginAnimations:@"toFrameAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    [view setFrame:rect];
    [UIView commitAnimations];
}

+ (void)scrollViewToTop:(UIScrollView *)scrollView {
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

+ (void)view2Alpha:(UIView *)view alpha:(float)alpha {
    [UIView beginAnimations:@"toFrameAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    [view setAlpha:alpha];
    [UIView commitAnimations];
}

@end
