//
//  UIUitl.h
//  NoTableVideo
//
//  Created by haolin yuan on 5/13/12.
//  Copyright (c) 2012 symbio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIUitl : NSObject
+ (void)toFrameAnimation:(UIView *)view toFrame:(CGRect)rect;
+ (void)scrollViewToTop:(UIScrollView *)scrollView;
+ (void)view2Alpha:(UIView *)view alpha:(float)alpha;

@end
