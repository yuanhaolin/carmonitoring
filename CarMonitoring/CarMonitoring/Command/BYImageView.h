/*
 Copyright (C) 2007 The Android Open Source Project
 *
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 *
 http://www.apache.org/licenses/LICENSE-2.0
 *
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import <UIKit/UIKit.h>
#import "Service.h"

@protocol BYImageViewDelegate
- (void)imageLoadFinish:(id)imageView;

@end


typedef enum _BYImageViewViewEnum {
	BYImageAll,
	BYImageClipAutoFrame,
    BYImageClip,
}BYImageViewViewEnum;

@interface BYImageView : UIView <UIGestureRecognizerDelegate> {
	Service *_service;
	
	NSString *_url;
	BOOL _isLoaded;
    BOOL _isImaged;	//是否有图片
	
	BYImageViewViewEnum _type;
	
    id _action;
    SEL _selector;
	id _delegate;
	
	CGSize selfSize;
    
    UIImageView *_bgImageView;
}
@property (nonatomic,assign) BOOL isLoaded;
@property (nonatomic,assign) BOOL isImaged;
@property (nonatomic,assign) BOOL isShowActivietView;
@property (nonatomic,assign) BOOL isShowProgress;
@property (nonatomic,assign) BYImageViewViewEnum type;
@property (nonatomic,retain) id action;
@property (nonatomic,assign) SEL selector;
@property (nonatomic,retain) id delegate;
@property (nonatomic,assign) UIImageView *backgroundImageView;

- (void)setImageUrl:(NSString *)url;
- (void)setImage:(UIImage *)image;
- (UIView *)imageView;
- (void)setBackgroundImageViewWithY:(float)y_;

@end
