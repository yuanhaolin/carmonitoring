//
//  CMMyUserInfoManager.h
//  CarMonitoring
//
//  Created by Work on 1/9/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cm.pb.h"

@interface CMMyUserInfoManager : NSObject

+ (CMMyUserInfoManager *)getInstance;

- (UserInfo_Builder *)getUserInfo;
- (void)setUserInfo:(NSData *)data;

- (VehicleInfo *)getVehicleInfoFromVehicleId:(int32_t)vehicleId;

@end
