//
//  MCCGPSObject.h
//  MyChevyCar
//
//  Created by Work on 5/11/13.
//  Copyright (c) 2013 YY. All rights reserved.
//

#define REFRESH_CITY @"REFRESH_CITY"
#import "BMapKit.h"
#import "BMKMapManager.h"

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface MCCGPSObject : UIView <CLLocationManagerDelegate,BMKGeneralDelegate,BMKMapViewDelegate,BMKSearchDelegate> {
    BMKSearch *searchAddress;
    
    IBOutlet BMKMapView *_map;
}
@property (nonatomic) CLLocationCoordinate2D coor;
@property (nonatomic,assign) BOOL canLocateCity;

+ (id)getInstance;

- (void)startGPS;
- (void)pauseGPS;

- (NSString *)getCurrentCityName;
- (void)setCurrentCityName:(NSString *)tmpCity;

- (NSString *)getCurrentAddress;

- (CLLocationCoordinate2D)getLocationCoordinate2D;
- (BOOL)isGPS;

@end
