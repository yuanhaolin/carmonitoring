//
//  CustomTabBar.m
//  PowerSupply
//
//  Created by Evan Tan on 5/9/12.
//  Copyright (c) 2012 symbio. All rights reserved.
//


#define BTN_BEGIN_TAG   100001
#define VIEW_SELECTED_BG    101
#define VIEW_ICON_TAG   102
#define LAB_TITLE_TAG   103
#define VIEW_SELECTED_BOTTOM_ICON   105
#define IMAGE_BG_TAG    106

#import "CustomTabBar.h"
#import "UIUitl.h"

@interface CustomTabBar () {
    
}

@end

@interface CustomTabBar (Private)

@end

@implementation CustomTabBar (Private)


@end

@implementation CustomTabBar
@synthesize imageBG=_imageBG;
@synthesize imageSelectedBG=_imageSelectedBG;
@synthesize selectedImages=_selectedImages;
@synthesize myDelegate=_myDelegate;

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)setCustomSelectIndex:(int)index_ {
    int index = index_;
    
    if ([_myDelegate respondsToSelector:@selector(customTabBar:selectIndex:)]) {
        [_myDelegate customTabBar:self selectIndex:index];
    }
    
    [self setSelectedIndex:index];
    
    for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UITabBar class]]){
            UIImageView *imgViewBG = (UIImageView *)[view viewWithTag:IMAGE_BG_TAG];
            [imgViewBG setFrame:CGRectMake(0, 0, 320, 49.5)];
            
            UIImageView *imgBG = (UIImageView *)[view viewWithTag:VIEW_SELECTED_BG];
            [imgBG setFrame:CGRectMake(index*64, 0, 64, 49.5)];
            
            UIImageView *bottomIconView = (UIImageView *)[view viewWithTag:VIEW_SELECTED_BOTTOM_ICON];
            [UIUitl toFrameAnimation:bottomIconView toFrame:CGRectMake(index*64, 0, 64, 49.5)];
            
            NSArray *array = view.subviews;
            int idx = 0;
            for (UIView *v in array) {
                if ([NSStringFromClass([v class]) isEqualToString:@"UITabBarButton"]) {
                    UIViewController *viewController = (UIViewController *)[self.viewControllers objectAtIndex:idx];
                    
                    UIButton *btn = (UIButton *)[view viewWithTag:BTN_BEGIN_TAG+idx];
                    UIImageView *imgIcon = (UIImageView *)[btn viewWithTag:VIEW_ICON_TAG];
                    UILabel *lab = (UILabel *)[btn viewWithTag:LAB_TITLE_TAG];
                    
                    [imgIcon setImage:viewController.tabBarItem.image];
                    [lab setTextColor:UIColorFromRGB(0x878787)];
                    
                    if (idx == index) {
                        [imgIcon setImage:[UIImage imageNamed:[_selectedImages objectAtIndex:idx]]];
                        [lab setTextColor:UIColorFromRGB(0x0D6ABB)];
                    }
                    
                    idx++;
                }
            }
            break;
        }
    }
}

- (void)btnTapping:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    int index = btn.tag-BTN_BEGIN_TAG;
    
    if ([_myDelegate respondsToSelector:@selector(customTabBar:selectIndex:)]) {
        [_myDelegate customTabBar:self selectIndex:index];
    }
    
    [self setSelectedIndex:index];
    
    for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UITabBar class]]){
            UIImageView *imgViewBG = (UIImageView *)[view viewWithTag:IMAGE_BG_TAG];
            [imgViewBG setFrame:CGRectMake(0, 0, 320, 49.5)];
            
            UIImageView *imgBG = (UIImageView *)[view viewWithTag:VIEW_SELECTED_BG];
            [imgBG setFrame:CGRectMake(index*64, 0, 64, 49.5)];
            
            UIImageView *bottomIconView = (UIImageView *)[view viewWithTag:VIEW_SELECTED_BOTTOM_ICON];
            [UIUitl toFrameAnimation:bottomIconView toFrame:CGRectMake(index*64, 0, 64, 49.5)];
            
            NSArray *array = view.subviews;
            int idx = 0;
            for (UIView *v in array) {
                if ([NSStringFromClass([v class]) isEqualToString:@"UITabBarButton"]) {
                    UIViewController *viewController = (UIViewController *)[self.viewControllers objectAtIndex:idx];
                    
                    UIButton *btn = (UIButton *)[view viewWithTag:BTN_BEGIN_TAG+idx];
                    UIImageView *imgIcon = (UIImageView *)[btn viewWithTag:VIEW_ICON_TAG];
                    UILabel *lab = (UILabel *)[btn viewWithTag:LAB_TITLE_TAG];
                    
                    [imgIcon setImage:viewController.tabBarItem.image];
                    [lab setTextColor:UIColorFromRGB(0x878787)];
                    
                    if (idx == index) {
                        [imgIcon setImage:[UIImage imageNamed:[_selectedImages objectAtIndex:idx]]];
                        [lab setTextColor:UIColorFromRGB(0x0D6ABB)];
                    }
                    
                    idx++;
                }
            }
            break;
        }
    }
}

- (void)setViewControllers:(NSArray *)viewControllers {
    [self.view setClipsToBounds:YES];
    [self.view setClearsContextBeforeDrawing:YES];
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    [super setViewControllers:viewControllers];
    
    int index = 0;
    for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UITabBar class]]){
            UIImageView *imgViewBG = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 49.5)];
            [imgViewBG setTag:IMAGE_BG_TAG];
            [imgViewBG setImage:_imageBG];
            [view addSubview:imgViewBG];
            [imgViewBG release];
            [imgViewBG setBackgroundColor:UIColorFromRGB(0x626262)];
            
            NSArray *array = view.subviews;
            for (UIView *v in array) {
                if ([NSStringFromClass([v class]) isEqualToString:@"UITabBarButton"]) {
                    UIViewController *viewController = (UIViewController *)[viewControllers objectAtIndex:index];
                    
                    UIImageView *imgBG = (UIImageView *)[view viewWithTag:VIEW_SELECTED_BG];
                    if (0 == index && nil == imgBG) {
                        imgBG = [[UIImageView alloc] initWithFrame:CGRectMake(index*64, 0, 64, 49.5)];
                        [imgBG setTag:VIEW_SELECTED_BG];
                        [imgBG setImage:_imageSelectedBG];
                        [view addSubview:imgBG];
                        [imgBG release];
                        
//                        UIImageView *bottomIconView = [[UIImageView alloc] initWithFrame:CGRectMake(index*64, 0, 64, 49.5)];
//                        [bottomIconView setTag:VIEW_SELECTED_BOTTOM_ICON];
//                        [bottomIconView setImage:[UIImage imageNamed:@"nva_dq.png"]];
//                        [view addSubview:bottomIconView];
//                        [bottomIconView release];
                    }
                    
                    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    [btn setTag:BTN_BEGIN_TAG+index];
                    [btn setBackgroundColor:[UIColor clearColor]];
                    [btn setFrame:CGRectMake(index*64, -1.5, 64, 49.5)];
                    [view addSubview:btn];
                    [btn addTarget:self action:NSSelectorFromString(@"btnTapping:") forControlEvents:UIControlEventTouchUpInside];
                    
                    UIImageView *imgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 64, 49.5)];
                    [imgIcon setTag:VIEW_ICON_TAG];
                    [imgIcon setBackgroundColor:[UIColor clearColor]];
                    [imgIcon setImage:viewController.tabBarItem.image];
                    [btn addSubview:imgIcon];
                    [imgIcon release];
                    
                    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, btn.frame.size.width, 14)];
                    [lab setTag:LAB_TITLE_TAG];
                    [lab setBackgroundColor:[UIColor clearColor]];
                    [lab setFont:WFONT(11)];
                    [lab setTextColor:UIColorFromRGB(0x878787)];
                    [lab setTextAlignment:NSTextAlignmentCenter];
                    [lab setText:viewController.tabBarItem.title];
                    [btn addSubview:lab];
                    [lab release];
                    
                    if (0 == index) {
                        UIImageView *imageBGView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -12.5, 320, 12.5)];
                        [imageBGView setImage:[UIImage imageNamed:@"bottom_h_zz.png"]];
                        [view addSubview:imageBGView];
                        [imageBGView release];
                        
                        [view sendSubviewToBack:imageBGView];
                        
                        [imgIcon setImage:[UIImage imageNamed:[_selectedImages objectAtIndex:index]]];
                        [lab setTextColor:UIColorFromRGB(0x0D6ABB)];
                    }
                    
                    index++;
                }
            }
            break;
        }
    }
}

- (void) dealloc{
    [_imageBG release];
    [_imageSelectedBG release];
    [_selectedImages release];
    [_myDelegate release];
    [super dealloc];
}

@end
