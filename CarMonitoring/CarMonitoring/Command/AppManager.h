//
//  AppManager.h
//  NoTableVideo
//
//  Created by Evan Tan on 4/29/12.
//  Copyright (c) 2012 symbio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppManager : NSObject

+ (CGRect)getAppFrame;
+ (id) getAppDelegate;

+ (UIWindow *) getStateBar;

+ (NSString *)getAPPID;

+ (NSMutableArray *)getCarList;

+ (void)addCarModeString:(NSString *)str;

+ (void)removeLastCarModeString;

+ (void)removeAllCar;

@end
