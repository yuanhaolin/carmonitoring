//
//  StatusBarWindow.m
//  LeadInfo
//
//  Created by haolin yuan on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define VIEW_LOADING1_TAG   21010012    //不能删除Loading界面 

#import "StatusBarWindow.h"

@implementation StatusBarWindow

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@synthesize iconImage, textLabel, clickCount;

static const CGRect windowFrame0 = {{0, 0}, {320, 20}};
static const CGRect windowFrame1 = {{0, 0}, {320, 20}};
static const CGRect imageFrame0 = {{6, 1}, {16, 16}};
static const CGRect imageFrame1 = {{2, 1}, {16, 16}};
static const CGRect textFrame = {{60, 0}, {200, 20}};

+ (StatusBarWindow *)newStatusBarWindow {
    StatusBarWindow *statusBar = [[StatusBarWindow alloc] initWithFrame:windowFrame0];
    if (statusBar) {
        statusBar.windowLevel = UIWindowLevelStatusBar + 1;
        
//        UIImageView *background = [[UIImageView alloc] initWithFrame:windowFrame1];
//        background.image = [UIImage imageNamed:@"topBar.png"];
//        [statusBar addSubview:background];
//        [background release];
//        
//        UIImageView *icon = [[UIImageView alloc] initWithFrame:imageFrame0];
//        icon.image = [UIImage imageNamed:@"loading.png"];
//        statusBar.iconImage = icon;
//        [statusBar addSubview:icon];
//        [icon release];
//        
//        UILabel *text = [[UILabel alloc] initWithFrame:textFrame];
//        text.backgroundColor = [UIColor clearColor];
//        text.textAlignment = UITextAlignmentCenter;
//        statusBar.textLabel = text;
//        [statusBar addSubview:text];
//        [text release];
    }
    
    return statusBar;
}

- (void)removeAllViews {
    for (UIView *v in self.subviews) {
        if (VIEW_LOADING1_TAG == v.tag) {
            
        }
        else {
            [v removeFromSuperview];
        }
    }
}
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:1];
//    textLabel.text = [NSString stringWithFormat:@"已点击%d次状态栏", ++clickCount];
//    if (clickCount % 2) {
//        self.frame = windowFrame1;
//        iconImage.frame = imageFrame1;
//    } else {
//        self.frame = windowFrame0;
//        iconImage.frame = imageFrame0;
//    }
//    [UIView commitAnimations];
//}

@end
