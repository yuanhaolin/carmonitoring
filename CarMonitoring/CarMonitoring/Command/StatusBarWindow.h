//
//  StatusBarWindow.h
//  LeadInfo
//
//  Created by haolin yuan on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusBarWindow : UIWindow {
    UIImageView *iconImage;
    UILabel *textLabel;
    NSUInteger clickCount;
}


@property (nonatomic, assign) UIImageView *iconImage;
@property (nonatomic, assign) UILabel *textLabel;
@property (nonatomic) NSUInteger clickCount;

+ (StatusBarWindow *)newStatusBarWindow;
- (void)removeAllViews;

@end
