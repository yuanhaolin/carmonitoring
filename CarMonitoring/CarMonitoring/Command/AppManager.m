//
//  AppManager.m
//  NoTableVideo
//
//  Created by Evan Tan on 4/29/12.
//  Copyright (c) 2012 symbio. All rights reserved.
//

#import "AppManager.h"
#import "StatusBarWindow.h"
#import "AppDelegate.h"

static StatusBarWindow *_pStatusBarWindow;
static NSMutableArray *_pCarList;

@implementation AppManager
+ (CGRect)getAppFrame {
    CGRect appRect = CGRectZero;
    
    appRect = [[UIScreen mainScreen]applicationFrame];
//    if (isIPhone5) {
//        appRect = CGRectMake(0, 0, 320, 548);
//    }
//    else {
//        appRect = CGRectMake(0, 0, 320, 460);
//    }
//    
    if (isIOS7) {
        appRect.size.height = appRect.size.height+20;
    }
    
    return appRect;
}

+ (id) getAppDelegate
{
    id d = [[UIApplication sharedApplication] delegate];
    return d;
}

+ (UIWindow *) getStateBar {
    if (nil == _pStatusBarWindow) {
        _pStatusBarWindow = [StatusBarWindow newStatusBarWindow];
    }
    
    return _pStatusBarWindow;
}

+ (NSString *)getAPPID {
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:bundlePath];
    NSString *value = [dict objectForKey:@"APPID"];
    
    return value;
}

+ (NSMutableArray *)getCarList {
    return _pCarList;
}

+ (void)addCarModeString:(NSString *)str {
    if (nil == _pCarList) {
        _pCarList = [NSMutableArray new];
    }
    [_pCarList addObject:str];
}

+ (void)removeLastCarModeString {
    [_pCarList removeLastObject];
}

+ (void)removeAllCar {
    [_pCarList removeAllObjects];
}

@end
