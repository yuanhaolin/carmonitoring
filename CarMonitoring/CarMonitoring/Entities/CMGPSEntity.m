//
//  CMGPSEntity.m
//  CarMonitoring
//
//  Created by Work on 1/19/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMGPSEntity.h"

@implementation CMGPSEntity
@synthesize lat=_lat;
@synthesize lon=_lon;
@synthesize time=_time;
@synthesize licencePlateNumber=_licencePlateNumber;

- (void)dealloc
{
    [_licencePlateNumber release];
    [super dealloc];
}

@end
