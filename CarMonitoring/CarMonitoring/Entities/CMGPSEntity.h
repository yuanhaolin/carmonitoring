//
//  CMGPSEntity.h
//  CarMonitoring
//
//  Created by Work on 1/19/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMGPSEntity : NSObject {
    float _lat;
    float _lon;
    long long _time;
    NSString *_licencePlateNumber;
}
@property (nonatomic,assign) float lat;
@property (nonatomic,assign) float lon;
@property (nonatomic,assign) long long time;
@property (nonatomic,retain) NSString *licencePlateNumber;

@end
