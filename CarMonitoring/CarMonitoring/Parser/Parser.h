//
//  Parser.h
//  CarMonitoring
//
//  Created by Work on 1/9/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YWTFileTypeManager.h"

@interface Parser : NSObject
+ (void)dic2Data:(NSDictionary *)dic request:(id)request outData:(NSData **)outData FileType:(FileType)FileType;

+ (void)parserJson2Dic:(NSString *)strJson outEntity:(NSDictionary **)outEntity;

@end
