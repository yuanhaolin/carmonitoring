//
//  ChineseString.h
//  CarMonitoring
//
//  Created by Work on 3/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChineseString : NSObject {
    
}
@property (nonatomic,retain) NSString *string;
@property (nonatomic,retain) NSString *oldString;
@property (nonatomic,retain) NSString *pinYin;

@end
