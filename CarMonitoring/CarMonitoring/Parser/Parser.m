//
//  Parser.m
//  CarMonitoring
//
//  Created by Work on 1/9/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "Parser.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "ChineseString.h"

@implementation Parser
+ (void)dic2Data:(NSDictionary *)dic request:(id)request outData:(NSData **)outData FileType:(FileType)FileType {
    NSString *time = [NSString stringWithFormat:@"%.0f",[[NSDate date] timeIntervalSince1970]];
    
    NSMutableData *data = [NSMutableData data];
    
    NSStringEncoding encodeing = NSUTF8StringEncoding;
    
    NSArray *allKeys = [dic allKeys];
    for (NSString *key in allKeys) {
        id value = [dic objectForKey:key];
        
        if ([value isKindOfClass:[NSData class]]) {
            [data appendData:[[NSString stringWithFormat:@"------%@\r\n",time] dataUsingEncoding:encodeing]];
            [data appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",key,@"cccc.png"] dataUsingEncoding:encodeing]];
            [data appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n",[YWTFileTypeManager getFileType2String:FileType]] dataUsingEncoding:encodeing]];
            
            [data appendData:value];
            
            [data appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:encodeing]];
        }
        else {
            [data appendData:[[NSString stringWithFormat:@"------%@\r\n",time] dataUsingEncoding:encodeing]];
            [data appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:encodeing]];
            
            [data appendData:[[NSString stringWithFormat:@"%@\r\n",value] dataUsingEncoding:encodeing]];
        }
    }
    
    [data appendData:[[NSString stringWithFormat:@"------%@--\r\n",time] dataUsingEncoding:encodeing]];
    
    if ([request isKindOfClass:[ASIFormDataRequest class]]) {
        [request addRequestHeader:@"Content-Type" value:[NSString stringWithFormat:@"multipart/form-data; boundary=----%@",time]];
        [request addRequestHeader:@"Content-Length" value:[NSString stringWithFormat:@"%d",[data length]]];
    }
    else {
        [request setValue: [NSString stringWithFormat:@"multipart/form-data; boundary=----%@",time] forHTTPHeaderField:@"Content-Type"];
        [request setValue: [NSString stringWithFormat:@"%d",[data length]] forHTTPHeaderField:@"Content-Length"];
    }
    
    //    NSString *str = [[NSString alloc] initWithData:data encoding:encodeing];
    //    NSLog(@"\r\n%@",str);
    
    *outData = data;
}

+ (NSDictionary *)new2Dic2:(NSArray *)dics {
    NSMutableArray *array = [NSMutableArray arrayWithArray:dics];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:[NSString stringWithFormat:@"%@",[array objectAtIndex:0]] forKey:@"key"];
    [array removeObjectAtIndex:0];
    
    [dic setValue:[NSMutableArray arrayWithArray:array] forKey:@"value"];
    
    return dic;
}

+ (NSMutableArray *)new2Dic:(NSDictionary *)dics {
    //    if ([dic isKindOfClass:[NSArray class]]) {
    //        return dic;
    //    }
    NSMutableArray *array = [NSMutableArray array];
    //
    //    NSArray *keys = [dic allKeys];
    //    for (NSString *key in keys) {
    //        NSMutableDictionary *newDic = [NSMutableDictionary dictionary];
    //        [newDic setObject:key forKey:@"key"];
    //
    //        NSDictionary *dic2 = [dic objectForKey:key];
    //
    //        NSArray *newDic2 = [Parser new2Dic:dic2];
    //
    //        [newDic setObject:newDic2 forKey:@"value"];
    //
    //        [array addObject:newDic];
    //    }
    if ([dics isKindOfClass:[NSArray class]]) {
        NSMutableArray *oneArray = [NSMutableArray arrayWithArray:dics];
        for (NSDictionary *dic in oneArray) {
            if ([dic isKindOfClass:[NSArray class]]) {
                NSMutableArray *arrayNew = [NSMutableArray arrayWithArray:(NSArray *)dic];
                
                NSMutableDictionary *newDic = [NSMutableDictionary dictionary];
                
                [newDic setObject:[NSString stringWithFormat:@"%@",[arrayNew objectAtIndex:0]] forKey:@"key"];
                
                [arrayNew removeObjectAtIndex:0];
                
                NSMutableArray *newArray2 = [NSMutableArray array];
                [newDic setObject:newArray2 forKey:@"value"];
                
                [array addObject:newDic];
                
                for (NSArray *nextArray in arrayNew) {
                    if ([nextArray isKindOfClass:[NSArray class]]) {
                        NSDictionary *a = [self new2Dic2:nextArray];
                        
                        [newArray2 addObject:a];
                    }
                    else {
                        [newDic setObject:[NSArray arrayWithArray:arrayNew] forKey:@"value"];
                        
                        [newArray2 addObject:newDic];
                        break;
                    }
                }
            }
            else {
                
            }
            
        }
    }
    else {
        NSLog(@"");
    }
    
    return array;
}

+ (void)parserJson2Dic:(NSString *)strJson outEntity:(NSDictionary **)outEntity {
    NSDictionary *dic = [strJson JSONValue];
    
//    NSMutableDictionary *newDic = [Parser new2Dic:dic];
//    
//    NSString *strjsonNew = [newDic JSONRepresentation];
    ////
    *outEntity = dic;
    NSLog(@"");
}

@end
