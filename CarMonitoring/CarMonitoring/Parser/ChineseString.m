//
//  ChineseString.m
//  CarMonitoring
//
//  Created by Work on 3/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "ChineseString.h"

@implementation ChineseString
@synthesize string;
@synthesize oldString;
@synthesize pinYin;

- (void)dealloc
{
    [self setString:nil];
    [self setPinYin:nil];
    [super dealloc];
}

@end
