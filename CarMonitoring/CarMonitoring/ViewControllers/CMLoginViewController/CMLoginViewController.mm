//
//  CMHomeViewController.m
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMLoginViewController.h"
#import "AppManager.h"
#import "UIUitl.h"
#import "CMHomeViewController.h"
#import "AppDelegate.h"
#import "AppManager.h"
#import "CDTVNavigationController.h"
#import "CMServiceManager.h"

@interface CMLoginViewController () {
    NSTimer *_loginTimer;
}

@end

@implementation CMLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [textFieldUser setFont:WFONT(15)];
    [textFieldUser setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [textFieldPassword setFont:WFONT(15)];
    [textFieldPassword setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [btnLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:WFONT(15)];
    
    [btnNoPassword setTitleColor:UIColorFromRGB(0xededed) forState:UIControlStateNormal];
    [btnNoPassword.titleLabel setFont:WFONT(15)];
    
    NSString *userNamePath = doc_cache_path(@"userNamePath");
    NSString *userName = [NSString stringWithContentsOfFile:userNamePath encoding:NSUTF8StringEncoding error:nil];
    [textFieldUser setText:userName];
    
    NSString *passwordPath = doc_cache_path(@"passwordPath");
    NSString *password = [NSString stringWithContentsOfFile:passwordPath encoding:NSUTF8StringEncoding error:nil];
    [textFieldPassword setText:password];
    
    [_service getIsClose];
    [_service go];
//    [_service getLoginHTML];
    
//    [_service sendLoginHTTPS:@"lxtapp@gnssgis.com" password:@"lxtapp@gnssgis.com" tickid:@"94UHACI7GLENCAEYHV0UMRBP2WQC91GC"];
//    [_service go];
}

- (void)loginTimerOut:(id)sender {
    _loginTimer=nil;
    [[CMServiceManager getInstance] logout];
    
    [self showLoading:NO msg:@""];
    [self alertMsg:@"登录超时"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 监控键盘
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    // 键盘高度变化通知，ios5.0新增的
#ifdef __IPHONE_5_0
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    }
#endif
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // 监控键盘
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    // 键盘高度变化通知，ios5.0新增的
#ifdef __IPHONE_5_0
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    }
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBGTapping:(id)sender {
    [textFieldUser resignFirstResponder];
    [textFieldPassword resignFirstResponder];
}

- (IBAction)btnNoPassword:(id)sender {
    NSURL *url=[NSURL URLWithString:@"tel://4007-028-666"];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)btnLoginTapping:(id)sender {
    [[CMServiceManager getInstance] login:textFieldUser.text password:textFieldPassword.text];
    [[CMServiceManager getInstance] setMyDelegate:self];
//    [_service getLoginHTML];
    [self showLoading:YES msg:@"登录中"];
//    [_service scoketConn];
//    [_service sendLogin:@"lxtapp@gnssgis.com" password:@"lxtapp@gnssgis.com"];
//    [_service getLoginHTML];
//    [_service go];
    
    [textFieldUser resignFirstResponder];
    [textFieldPassword resignFirstResponder];
    
    NSString *userNamePath = doc_cache_path(@"userNamePath");
    [textFieldUser.text writeToFile:userNamePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    NSString *passwordPath = doc_cache_path(@"passwordPath");
    [textFieldPassword.text writeToFile:passwordPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    [_loginTimer invalidate];_loginTimer=nil;
    _loginTimer = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:NSSelectorFromString(@"loginTimerOut:") userInfo:nil repeats:NO];
}

#define mark UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textFieldUser == textField) {
        [textFieldPassword becomeFirstResponder];
    }
    else {
        NSString *userNamePath = doc_cache_path(@"userNamePath");
        [textFieldUser.text writeToFile:userNamePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        NSString *passwordPath = doc_cache_path(@"passwordPath");
        [textFieldPassword.text writeToFile:passwordPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        [[CMServiceManager getInstance] login:textFieldUser.text password:textFieldPassword.text];
        [[CMServiceManager getInstance] setMyDelegate:self];
        [self showLoading:YES msg:@"登录中"];
        
        [textFieldUser resignFirstResponder];
        [textFieldPassword resignFirstResponder];
        
        [_loginTimer invalidate];_loginTimer=nil;
        _loginTimer = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:NSSelectorFromString(@"loginTimerOut:") userInfo:nil repeats:NO];
    }
    
    return YES;
}

- (void)getMyUserInfo {
    [[CMServiceManager getInstance] getMyUserInfo];
}

- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    if ([typename_ isEqualToString:@"RequestResponse"]) {
        RequestResponse *rr = (RequestResponse *)entity;
        if (RequestResponse_ResponseTypeSuccess == rr.type) {
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"getMyUserInfo") userInfo:nil repeats:NO];
        }
        else {
            [self alertMsg:@"登录失败!"];
        }
    }
    else if ([typename_ isEqualToString:@"UserInfo"]) {
        [self showLoading:NO msg:@""];
        
        AppDelegate *dele = [AppManager getAppDelegate];
        
        CMHomeViewController *homeViewController = [[CMHomeViewController alloc] initWithNibName:@"CMHomeViewController" bundle:nil];
        [dele.nav pushViewController:homeViewController animated:YES];
        [homeViewController release];
        
        [_loginTimer invalidate];
        _loginTimer=nil;
    }
}

- (void)serviceFaild:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    [self showLoading:NO msg:@""];
    
    [_loginTimer invalidate];_loginTimer=nil;
}

#pragma mark Keyword
- (void)keyboardWillShow:(NSNotification *)aNotification {
    CGRect keyboardFrame;
    [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardFrame];
    
    if (!isIPhone5) {
        [UIUitl toFrameAnimation:viewLoginBg toFrame:CGRectMake(viewLoginBg.frame.origin.x, 70, viewLoginBg.frame.size.width, viewLoginBg.frame.size.height)];
    }
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    CGRect keyboardFrame;
    [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardFrame];
    
    if (!isIPhone5) {
        [UIUitl toFrameAnimation:viewLoginBg toFrame:CGRectMake(viewLoginBg.frame.origin.x, 111, viewLoginBg.frame.size.width, viewLoginBg.frame.size.height)];
    }
}

@end
