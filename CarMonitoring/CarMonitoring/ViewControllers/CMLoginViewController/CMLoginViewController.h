//
//  CMHomeViewController.h
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"

@interface CMLoginViewController : NVParentViewController {
    IBOutlet UITextField *textFieldUser;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UIButton *btnLogin;
    IBOutlet UIView *viewLoginBg;
    IBOutlet UIButton *btnNoPassword;
}

@end
