//
//  CDTVCarInfoModifyViewController.h
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"

@class VehicleInfo;
@interface CDTVCarInfoModifyViewController : NVParentViewController {
    IBOutlet UITextField *textTitleCPH;
    IBOutlet UITextField *textTitleCJH;
    IBOutlet UITextField *textTitleCX;
}

- (void)setItem:(VehicleInfo *)item_;

@end
