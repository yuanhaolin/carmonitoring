//
//  CDTVCarInfoModifyViewController.m
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CDTVCarInfoModifyViewController.h"
#import "Cm.pb.h"
#import "CMServiceManager.h"
#import "CMCarModelViewController.h"
#import "AppManager.h"
#import "AppDelegate.h"
#import "CDTVNavigationController.h"
#import "Parser.h"
#import "CMMyUserInfoManager.h"

@interface CDTVCarInfoModifyViewController () {
    VehicleInfo *_item;
}

@end

@implementation CDTVCarInfoModifyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"车辆详细信息"];
    
    [textTitleCJH setFont:WFONT(15)];
    [textTitleCJH setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [textTitleCPH setFont:WFONT(15)];
    [textTitleCPH setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [textTitleCX setFont:WFONT(15)];
    [textTitleCX setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(carEnter:) name:@"carEnter" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back:(id)sender {
    [_item release];_item=nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"carEnter" object:nil];
    
    [super back:sender];
}

- (void)carEnter:(id)sender {
    NSMutableArray *array = [NSMutableArray arrayWithArray:[AppManager getCarList]];
    
    [array addObject:[sender object]];
    
    NSString *carString = [array componentsJoinedByString:@" | "];
    
    [textTitleCX setText:carString];
}

- (IBAction)btnBGTapping:(id)sender {
    [textTitleCJH resignFirstResponder];
    [textTitleCPH resignFirstResponder];
    [textTitleCX resignFirstResponder];
}

- (IBAction)btnTapping:(id)sender {
    VehicleInfo_Builder *vib = [_item toBuilder];
    [vib setLicencePlateNumber:textTitleCPH.text];
    [vib setVin:textTitleCJH.text];
    [vib setModel:textTitleCX.text];
    
    VehicleInfo *vi = [vib buildPartial];
    
    [[CMServiceManager getInstance] sendModifyCar:vi];
    [[CMServiceManager getInstance] setMyDelegate:self];
    
    [self showLoading:YES msg:@"修改中..."];
}

- (void)setItemTimerOut:(id)sender {
    [textTitleCPH setText:_item.licencePlateNumber];
    [textTitleCJH setText:_item.vin];
    [textTitleCX setText:_item.model];
}

- (void)setItem:(VehicleInfo *)item_ {
    [_item release];_item=nil;
    _item = [item_ retain];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setItemTimerOut:") userInfo:nil repeats:NO];
}

- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    [self showLoading:NO msg:@"修改中..."];
    
    if ([typename_ isEqualToString:@"RequestResponse"]) {
        if (nil != entity) {
            VehicleInfo_Builder *vib = [_item toBuilder];
            [vib setLicencePlateNumber:textTitleCPH.text];
            [vib setVin:textTitleCJH.text];
            [vib setModel:textTitleCX.text];
            
            PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
            
            NSMutableArray *newMutableArray = [NSMutableArray new];
            for (VehicleInfo *vi in array) {
                if (vi.vehicleId == vib.vehicleId) {
                    [newMutableArray addObject:[vib buildPartial]];
                }
                else {
                    [newMutableArray addObject:vi];
                }
            }
            
            UserInfo_Builder *userInfo = [[CMMyUserInfoManager getInstance] getUserInfo];
            [userInfo setVehiclesArray:newMutableArray];
            
            [[CMMyUserInfoManager getInstance] setUserInfo:[[userInfo buildPartial] data]];
            
            [self alertMsg:@"修改成功!"];
            [self back:nil];
        }
    }
}

- (void)serviceFaild:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    if ([typename_ isEqualToString:@"RequestResponse"]) {
//        [self alertMsg:@"修改失败!"];
    }
    [self showLoading:NO msg:@"修改中..."];
}

- (IBAction)btnCarListTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    [AppManager removeAllCar];
    
    CMCarModelViewController *carModelViewController = [[CMCarModelViewController alloc] initWithNibName:@"CMCarModelViewController" bundle:nil];
    [dele.nav pushViewController:carModelViewController animated:YES];
    [carModelViewController release];
    NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"carList.json"];
    NSString *strJson = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *dics = nil;
    [Parser parserJson2Dic:strJson outEntity:&dics];
    
    [carModelViewController setDicItems:dics];
}

@end
