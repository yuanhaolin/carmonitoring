//
//  CDTVWebViewController.m
//  CDTV2
//
//  Created by Symbio on 5/16/13.
//  Copyright (c) 2013 Symbio. All rights reserved.
//

#import "CDTVWebViewController.h"
#import "AppManager.h"
#import "AppDelegate.h"
#import "BYImageView.h"

@interface CDTVWebViewController () {
    UIWebView *webView;
    NSString *_url;
    
    NSString *_title;
    
    UIActivityIndicatorView *activityView;
}

@end

@implementation CDTVWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [bgView setImage:[UIImage imageNamed:@"pic_bg.png"]];
    [self.view addSubview:bgView];
    [bgView release];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"车型选择"];
    
//    [_topToolbar.imageBGView setImage:[UIImage imageNamed:@"nav_topnewtitlebg2.png"]];

    CGRect appFrame = [AppManager getAppFrame];
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, appFrame.size.width, appFrame.size.height-_topToolbar.frame.size.height)];
    
//    if (isIOS7) {
//        [webView setFrame:CGRectMake(0, _topToolbar.frame.size.height-20, appFrame.size.width, appFrame.size.height-_topToolbar.frame.size.height+20)];
//    }
    
    [webView setDataDetectorTypes:UIDataDetectorTypeAll];
    [webView setDelegate:self];
    [self.view addSubview:webView];
    [webView release];
    
    activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((appFrame.size.width-30)/2., (appFrame.size.height-30)/2., 30, 30)];
    [self.view addSubview:activityView];
    [activityView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [activityView release];
    [activityView startAnimating];
    
//    if (isIOS7) {
//        [webView setFrame:CGRectMake(0, 24, appFrame.size.width, webView.frame.size.height+20)];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_url release];
    [_title release];
    [super dealloc];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView_ {
    //获取系统版本号
    if (nil != _url) {
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:bundlePath];
        NSString *value = [dict objectForKey:@"CFBundleVersion"];
        
        [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById(\"versionnumber\").innerText = \"%@\";",value]];
    }
    
    [activityView removeFromSuperview];activityView=nil;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}

- (void)setUrlTimerOut:(id)sender {
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
    
    [self setTopToolTitle:_title];
}

- (void)setUrl:(NSString *)url_ title:(NSString *)title_ {
    [_url release];_url=nil;
    _url = [url_ retain];
    
    [_title release];_title=nil;
    _title = [title_ retain];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setUrlTimerOut:") userInfo:self repeats:NO];
}

- (void)timerOut:(NSTimer *)timer {
    NSString *title_ = [timer userInfo];
    
    [self setTopToolTitle:title_];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
}

- (void)setAbout:(NSString *)url_ {
    [_url release];_url=nil;
    _url = [url_ retain];
    
//    [_service getHtml:_url];
    
    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:NSSelectorFromString(@"timerOut:") userInfo:@"关于" repeats:NO];
}

- (void)setFankui:(NSString *)url_ {
    [_url release];_url=nil;
    _url = [url_ retain];
    
//    [_service getHtml:_url];
    
    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:NSSelectorFromString(@"timerOut:") userInfo:@"用户反馈" repeats:NO];
}

#pragma mark Service delegate
- (void)service:(id)service tag:(int)tag entity:(id)entity userInfo:(NSDictionary *)userInfo {
//    switch (tag) {
//        case GET_HTML_TAG:
//        {
//            [webView loadHTMLString:entity baseURL:[NSURL URLWithString:_url]];
//        }
//            break;
//        default:
//            break;
//    }
}

- (void)serviceFaild:(id)service tag:(int)tag entity:(id)entity userInfo:(NSDictionary *)userInfo {
    
}

@end
