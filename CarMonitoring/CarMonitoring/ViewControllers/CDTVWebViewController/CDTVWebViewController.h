//
//  CDTVWebViewController.h
//  CDTV2
//
//  Created by Symbio on 5/16/13.
//  Copyright (c) 2013 Symbio. All rights reserved.
//

#import "NVParentViewController.h"

@interface CDTVWebViewController : NVParentViewController <UIWebViewDelegate>

- (void)setUrl:(NSString *)url_ title:(NSString *)title_;
- (void)setAbout:(NSString *)url_;
- (void)setFankui:(NSString *)url_;

@end
