//
//  CMMyInfoViewController.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 60
#define TABLEVIEW_TAG   1001

#import "CMMyInfoViewController.h"
#import "AppManager.h"
#import "CMMyInfoCellView.h"
#import "CMModifyMyInfoViewController.h"
#import "CDTVNavigationController.h"
#import "AppManager.h"
#import "AppDelegate.h"
#import "CMModifyPasswordViewController.h"
#import "Cm.pb.h"
#import "CMMyUserInfoManager.h"

@interface CMMyInfoViewController ()

@end

@implementation CMMyInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    CGRect appFrame = [AppManager getAppFrame];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"个人信息"];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [tableView setTag:TABLEVIEW_TAG];
    [tableView setRowHeight:ROW_HIG];
	[tableView setDataSource:self];
	[tableView setDelegate:self];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:tableView];
	[tableView release];
    [tableView setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 1;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        [cell.textLabel setTextColor:UIColorFromRGB(0x383838)];
        [cell.textLabel setFont:WFONT(16)];
        
        CMMyInfoCellView *cellView = [[CMMyInfoCellView alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [cellView setTag:1];
        [cell.contentView addSubview:cellView];
        [cellView release];
	}
    CMMyUserInfoManager *myUserInfoManager = [CMMyUserInfoManager getInstance];
    UserInfo_Builder *myUserInfo = [myUserInfoManager getUserInfo];
    UserInfo_Person *ownerInfoPerson = myUserInfo.owner;
    UserInfo_Person *contactInfoPerson = [myUserInfo.contact objectAtIndex:0];
    
    CMMyInfoCellView *cellView = (CMMyInfoCellView *)[cell.contentView viewWithTag:1];
    if (0 == indexPath.row%2) {
        [cellView.bgImageView setImage:[UIImage imageNamed:@"carlistbg1.png"]];
    }
    else {
        [cellView.bgImageView setImage:[UIImage imageNamed:@"carlistbg2.png"]];
    }
    if (4 == indexPath.row || 5 == indexPath.row) {
        [cellView.imgIconView setHidden:NO];
    }
    else {
        [cellView.imgIconView setHidden:YES];
    }
    switch (indexPath.row) {
        case 0:
        {
            [cellView.labTitle1 setText:@"姓名"];
            [cellView.labTitle2 setText:ownerInfoPerson.name];
        }
            break;
        case 1:
        {
            [cellView.labTitle1 setText:@"手机号码"];
            [cellView.labTitle2 setText:ownerInfoPerson.cellphone];
        }
            break;
        case 2:
        {
            [cellView.labTitle1 setText:@"联系人"];
            [cellView.labTitle2 setText:contactInfoPerson.name];
        }
            break;
        case 3:
        {
            [cellView.labTitle1 setText:@"联系人手机号码"];
            [cellView.labTitle2 setText:contactInfoPerson.cellphone];
        }
            break;
        case 4:
        {
            [cellView.labTitle1 setText:@"修改资料"];
            [cellView.labTitle2 setText:@""];
        }
            break;
        case 5:
        {
            [cellView.labTitle1 setText:@"修改密码"];
            [cellView.labTitle2 setText:@""];
        }
            break;
            
        default:
            break;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (4 == indexPath.row) {
        AppDelegate *dele = [AppManager getAppDelegate];
        
        CMModifyMyInfoViewController *carInfoModifyViewContorller = [[CMModifyMyInfoViewController alloc] initWithNibName:@"CMModifyMyInfoViewController" bundle:nil];
        [dele.nav pushViewController:carInfoModifyViewContorller animated:YES];
        [carInfoModifyViewContorller release];
    }
    else if (5 == indexPath.row) {
        AppDelegate *dele = [AppManager getAppDelegate];
        
        CMModifyPasswordViewController *passwordViewController = [[CMModifyPasswordViewController alloc] initWithNibName:@"CMModifyPasswordViewController" bundle:nil];
        [dele.nav pushViewController:passwordViewController animated:YES];
        [passwordViewController release];
    }
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
}

@end
