//
//  CMModifyPasswordViewController.h
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"

@interface CMModifyPasswordViewController : NVParentViewController {
    IBOutlet UITextField *labPassword1;
    IBOutlet UITextField *labPassword2;
    IBOutlet UITextField *labPassword3;
}

@end
