//
//  CMModifyPasswordViewController.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMModifyPasswordViewController.h"
#import "CMServiceManager.h"
#import "CMMyUserInfoManager.h"
#import "Cm.pb.h"

@interface CMModifyPasswordViewController ()

@end

@implementation CMModifyPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"修改密码"];

    [labPassword1 setFont:WFONT(15)];
    [labPassword1 setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [labPassword2 setFont:WFONT(15)];
    [labPassword2 setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [labPassword3 setFont:WFONT(15)];
    [labPassword3 setTextColor:UIColorFromRGB(0x6b6b6b)];
}

- (IBAction)btnBGTapping:(id)sender {
    [labPassword1 resignFirstResponder];
    [labPassword2 resignFirstResponder];
    [labPassword3 resignFirstResponder];
}

- (IBAction)btnTapping:(id)sender {
    if ([labPassword1.text length] == 0) {
        [self alertMsg:@"请输入旧密码！"];
        return;
    }
    if ([labPassword2.text length] == 0) {
        [self alertMsg:@"请输入新密码！"];
        return;
    }
    if ([labPassword2.text length] < 8) {
        [self alertMsg:@"新密码必须八位以上！"];
        return;
    }
    if (![labPassword2.text isEqualToString:labPassword3.text]) {
        [self alertMsg:@"两次输入密码不一致！"];
        return;
    }
    
    [labPassword1 resignFirstResponder];
    [labPassword2 resignFirstResponder];
    [labPassword3 resignFirstResponder];
    
    NSString *userNamePath = doc_cache_path(@"userNamePath");
    NSString *userName = [NSString stringWithContentsOfFile:userNamePath encoding:NSUTF8StringEncoding error:nil];
    
    ChangePassword_Builder *cpb = [ChangePassword_Builder new];
    [cpb setUserName:userName];
    [cpb setOldPassword:labPassword1.text];
    [cpb setNewPassword:labPassword2.text];
    
    [[CMServiceManager getInstance] sendModifyPassword:[cpb buildPartial]];
    [[CMServiceManager getInstance] setMyDelegate:self];
    
    [self showLoading:YES msg:@"修改中..."];
    
    [cpb release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    [self showLoading:NO msg:@"修改中..."];
    
    if ([typename_ isEqualToString:@"RequestResponse"]) {
        if (nil != entity) {
            [self alertMsg:@"修改成功!"];
            [self back:nil];
        }
    }
}

- (void)serviceFaild:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    [self showLoading:NO msg:@"修改中..."];
}

@end
