//
//  CMCarInfoListViewController.h
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"
#import "BMapKit.h"

@interface CMCarInfoListViewController : NVParentViewController <UITableViewDataSource,UITableViewDelegate,BMKSearchDelegate>

@end
