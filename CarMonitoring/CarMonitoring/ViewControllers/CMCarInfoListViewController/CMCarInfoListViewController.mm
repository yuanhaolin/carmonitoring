//
//  CMCarInfoListViewController.m
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 60
#define TABLEVIEW_TAG   1001

#import "CMCarInfoListViewController.h"
#import "AppManager.h"
#import "PullToRefreshTableView.h"
#import "CMCarInfoListCellView.h"
#import "CMCarInfoTopView.h"
#import "CDTVCarInfoDescViewController.h"
#import "AppDelegate.h"
#import "CDTVNavigationController.h"
#import "CDTVMapsViewController.h"
#import "CMHistoryViewController.h"
#import "CMMyUserInfoManager.h"
#import "CLLocation+YCLocation.h"
#import "CMServiceManager.h"

@interface CMCarInfoListViewController () {
    int _page;
    NSMutableArray *_list;
    
    NSMutableArray *_selectedList;
    
    CMCarInfoTopView *_topView;
    
    int _refreshIndex;
    
    BMKSearch *_baiduSearch;
    
    NSMutableArray *_addressList;
    
    NSTimer *_refreshAddressTimer;
}

@end

@interface CMCarInfoListViewController (Private)
- (void)refreshData;

- (void)changeView;

@end

@implementation CMCarInfoListViewController (Private)
- (void)refreshData {
    
}

- (void)changeView {
    [_topView.btnSXXX removeTarget:self action:NSSelectorFromString(@"btnSXXXTapping:") forControlEvents:UIControlEventTouchUpInside];
    [_topView.btnSSDT removeTarget:self action:NSSelectorFromString(@"btnSSDTTapping:") forControlEvents:UIControlEventTouchUpInside];
    [_topView.btnLSJB removeTarget:self action:NSSelectorFromString(@"btnLSJBTapping:") forControlEvents:UIControlEventTouchUpInside];
    [_topView.btnLSGJ removeTarget:self action:NSSelectorFromString(@"btnLSGJTapping:") forControlEvents:UIControlEventTouchUpInside];
    
    PullToRefreshTableView *tableView = (PullToRefreshTableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    if (0 == [_selectedList count]) {
        [tableView setTableHeaderView:nil];
    }
    else if (1 == [_selectedList count]) {
        [_topView setIsDan:YES];
        [tableView setTableHeaderView:_topView];
    }
    else {
        [_topView setIsDan:NO];
        [tableView setTableHeaderView:_topView];
    }
    
    [_topView.btnSXXX addTarget:self action:NSSelectorFromString(@"btnSXXXTapping:") forControlEvents:UIControlEventTouchUpInside];
    [_topView.btnSSDT addTarget:self action:NSSelectorFromString(@"btnSSDTTapping:") forControlEvents:UIControlEventTouchUpInside];
    [_topView.btnLSJB addTarget:self action:NSSelectorFromString(@"btnLSJBTapping:") forControlEvents:UIControlEventTouchUpInside];
    [_topView.btnLSGJ addTarget:self action:NSSelectorFromString(@"btnLSGJTapping:") forControlEvents:UIControlEventTouchUpInside];
}

@end

@implementation CMCarInfoListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    CGRect appFrame = [AppManager getAppFrame];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [bgView setImage:[UIImage imageNamed:@"pic_bg.png"]];
    [self.view addSubview:bgView];
    [bgView release];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"车辆信息"];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [tableView setTag:TABLEVIEW_TAG];
    [tableView setRowHeight:ROW_HIG];
	[tableView setDataSource:self];
	[tableView setDelegate:self];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:tableView];
	[tableView release];
    [tableView setBackgroundColor:[UIColor clearColor]];
    
    _topView = [[CMCarInfoTopView alloc] initWithFrame:CGRectMake(0, 0, 320, 98)];
    
    _page = 1;
    
    _list = [NSMutableArray new];
    _selectedList = [NSMutableArray new];
    
    //获取所有车辆信息
    [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    
    _baiduSearch = [[BMKSearch alloc]init];
    [_baiduSearch setDelegate:self];
    
    _addressList = [NSMutableArray new];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshAddress {
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    
    if ([array count] != [_addressList count]) {
        [_addressList removeAllObjects];
        
        if ([array count]>0) {
            VehicleInfo *vInfo = (VehicleInfo *)[array objectAtIndex:0];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:vInfo.lastReport.location.latitude longitude:vInfo.lastReport.location.longitude];
            CLLocation *newLocation = [location locationMarsFromEarth];
            CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
            
            CLLocationCoordinate2D pt = (CLLocationCoordinate2D){baiduLocation.coordinate.latitude, baiduLocation.coordinate.longitude};
            BOOL flag = [_baiduSearch reverseGeocode:pt];
            if (flag) {
                NSLog(@"ReverseGeocode search success.");
                
            }
            else{
                NSLog(@"ReverseGeocode search failed!");
            }
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _refreshIndex = 0;
    
    [_baiduSearch setDelegate:self];
    
    [[CMServiceManager getInstance] setMyDelegate:self];
    [[CMServiceManager getInstance] getMyUserInfo];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_baiduSearch setDelegate:nil];
    [[CMServiceManager getInstance] setMyDelegate:nil];
    
    [_refreshAddressTimer invalidate];_refreshAddressTimer=nil;
}

- (void)dealloc
{
    [_list release];
    [_selectedList release];
    [_topView release];
    [_addressList release];
    [super dealloc];
}

#pragma mark UIButton
- (IBAction)btnSSDTTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    CDTVMapsViewController *mapsViewController = [[CDTVMapsViewController alloc] initWithNibName:@"CDTVMapsViewController" bundle:nil];
    [dele.nav pushViewController:mapsViewController animated:YES];
    [mapsViewController release];
    [mapsViewController setIsHis:NO];
    
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    NSMutableArray *newArray = [NSMutableArray array];
    for (NSIndexPath *ip in _selectedList) {
        [newArray addObject:[array objectAtIndex:ip.row]];
    }
    
    [mapsViewController setItemMonitorVehicles:newArray];
}

- (IBAction)btnLSGJTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    CDTVMapsViewController *mapsViewController = [[CDTVMapsViewController alloc] initWithNibName:@"CDTVMapsViewController" bundle:nil];
    [dele.nav pushViewController:mapsViewController animated:YES];
    [mapsViewController release];
    [mapsViewController setIsHis:YES];
    
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    NSMutableArray *newArray = [NSMutableArray array];
    for (NSIndexPath *ip in _selectedList) {
        [newArray addObject:[array objectAtIndex:ip.row]];
    }
    
    [mapsViewController setItemMonitorVehicles:newArray];
}

- (IBAction)btnLSJBTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    NSMutableArray *newArray = [NSMutableArray array];
    for (NSIndexPath *ip in _selectedList) {
        [newArray addObject:[array objectAtIndex:ip.row]];
    }
    
    CMHistoryViewController *historeViewController = [[CMHistoryViewController alloc] init];
    [dele.nav pushViewController:historeViewController animated:YES];
    [historeViewController release];
    [historeViewController setItems:newArray];
}

- (IBAction)btnSXXXTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    NSMutableArray *newArray = [NSMutableArray array];
    for (NSIndexPath *ip in _selectedList) {
        [newArray addObject:[array objectAtIndex:ip.row]];
    }
    
    CDTVCarInfoDescViewController *carInfoDescViewController = [[CDTVCarInfoDescViewController alloc] init];
    [dele.nav pushViewController:carInfoDescViewController animated:YES];
    [carInfoDescViewController release];
    [carInfoDescViewController setItem:[newArray lastObject]];
}

- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error
{
    [_addressList addObject:result.strAddr];
    
    PullToRefreshTableView *tableView = (PullToRefreshTableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_refreshIndex inSection:0]];
    CMCarInfoListCellView *cellView = (CMCarInfoListCellView *)[cell.contentView viewWithTag:1];
    [cellView refreshAddress:result.strAddr];
    
    
    _refreshIndex ++;
    
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    if (_refreshIndex>=[array count]) {
        return;
    }
    
    VehicleInfo *vInfo = (VehicleInfo *)[array objectAtIndex:_refreshIndex];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:vInfo.lastReport.location.latitude longitude:vInfo.lastReport.location.longitude];
    CLLocation *newLocation = [location locationMarsFromEarth];
    CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
    
    CLLocationCoordinate2D pt = (CLLocationCoordinate2D){baiduLocation.coordinate.latitude, baiduLocation.coordinate.longitude};
    BOOL flag = [_baiduSearch reverseGeocode:pt];
    if (flag) {
        NSLog(@"ReverseGeocode search success.");
        
    }
    else{
        NSLog(@"ReverseGeocode search failed!");
    }
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 1;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    return [array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        [cell.textLabel setTextColor:UIColorFromRGB(0x383838)];
        [cell.textLabel setFont:WFONT(16)];
        
        CMCarInfoListCellView *cellView = [[CMCarInfoListCellView alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [cellView setTag:1];
        [cellView setDelegate:self];
        [cell.contentView addSubview:cellView];
        [cellView release];
	}
    CMCarInfoListCellView *cellView = (CMCarInfoListCellView *)[cell.contentView viewWithTag:1];
    if (0 == indexPath.row%2) {
        [cellView.bgImageView setImage:[UIImage imageNamed:@"carlistbg1.png"]];
    }
    else {
        [cellView.bgImageView setImage:[UIImage imageNamed:@"carlistbg2.png"]];
    }
    
    BOOL isSelect = NO;
    for (NSIndexPath *ip in _selectedList) {
        if (ip.row == indexPath.row) {
            isSelect = YES;
        }
    }
    [cellView setSelected:isSelect];
    
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    VehicleInfo *vInfo = [array objectAtIndex:indexPath.row];
    [cellView setItem:vInfo];
    
    if ([_addressList count]>indexPath.row) {
        [cellView refreshAddress:[_addressList objectAtIndex:indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BOOL isSelect = NO;
    for (NSIndexPath *ip in _selectedList) {
        if (ip.row == indexPath.row) {
            isSelect = YES;
        }
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CMCarInfoListCellView *cellView = (CMCarInfoListCellView *)[cell.contentView viewWithTag:1];
    [cellView setSelected:!isSelect];
    
    if (!isSelect) {
        [_selectedList addObject:indexPath];
    }
    else {
        for (NSIndexPath *ip in _selectedList) {
            if (ip.row == indexPath.row) {
                [_selectedList removeObject:ip];
                break;
            }
        }
    }
    
    [self changeView];
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
}

#pragma mark STPopBottomSelectionView delegate
- (void)popBottomSelectionView:(id)popBottomSelectionView index:(int)index_ {
    switch (index_) {
            case 0:
        {
            [self setTopToolTitle:@"最新优惠"];
        }
            break;
            case 1:
        {
            [self setTopToolTitle:@"即将到期"];
        }
            break;
        default:
            break;
    }
    
    [self refreshData];
}

#pragma mark Service delegate
- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    [self showLoading:NO msg:@"加载中..."];
    
    if ([typename_ isEqualToString:@"UserInfo"]) {
        [self showLoading:NO msg:@""];
        
        [_addressList removeAllObjects];
        [self refreshAddress];
        
        UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
        [tableView reloadData];
        
        [_refreshAddressTimer invalidate];_refreshAddressTimer=nil;
        _refreshAddressTimer = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:NSSelectorFromString(@"refreshAddress") userInfo:nil repeats:YES];
    }
    
    switch (tag) {
//            case GET_NOTEPOPLIST_TAG:
//        {
//            [_items removeAllObjects];
//            [_items addObjectsFromArray:entity];
//            
//            PullToRefreshTableView *tableView = (PullToRefreshTableView *)[self.view viewWithTag:TABLEVIEW_TAG];
//            [tableView reloadData:YES];
//        }
//            break;
        default:
            break;
    }
}

- (void)serviceFaild:(id)service tag:(int)tag entity:(id)entity userInfo:(NSDictionary *)userInfo {
    
}

@end
