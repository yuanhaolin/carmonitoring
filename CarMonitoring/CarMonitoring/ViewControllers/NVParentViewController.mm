//
//  CDParentViewController.m
//  CDCoupon
//
//  Created by Symbio on 3/24/12.
//  Copyright 2012 symbio. All rights reserved.
//

#define BTN_SEARCH_TAG  201
#define TOP_TITLE_TAG   202
#define TEXT_SEARCH_TAG 203
#define BTN_SEARCH_CLOSE_TAG    204
#define TABLEVIEW_SEARCH_TAG    206
#define TOPVIEW_TAG     208
#define IMGVIEW_TEXT_BG_TAG 209
#define SEARCH_BG_TAG   11106
#define VIEW_LOADING1_TAG   21110012

#import "NVParentViewController.h"
#import "AppManager.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "StatusBarWindow.h"
#import "CDTVNavigationController.h"
#import "CMHomeViewController.h"

@interface NVParentViewController () {
    
}

@end

@implementation NVParentViewController
@synthesize service=_service;
@synthesize delegate = _delegate;
@synthesize topToolbar=_topToolbar;
@synthesize bottomToolbar=_bottomToolbar;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if (nil == _service) {
            _service = [Service new];
            [_service setDelegate:self];
        }
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        if (nil == _service) {
            _service = [Service new];
            [_service setDelegate:self];
        }
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect appFrame = [AppManager getAppFrame];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    _topToolbar = [[CDTVTopBarView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [self.view addSubview:_topToolbar];
//    [_topToolbar release];
    [_topToolbar setHidden:YES];
    if (isIOS7) {
        [_topToolbar setFrame:CGRectMake(0, 0, _topToolbar.frame.size.width, 64)];
    }
    else {
        [_topToolbar setFrame:CGRectMake(0, -20, _topToolbar.frame.size.width, 44)];
    }
    
    _bottomToolbar = [[CDTVBottomBarView alloc] initWithFrame:CGRectMake(0, appFrame.size.height-44, 320, 44)];
    [self.view addSubview:_bottomToolbar];
    [_bottomToolbar setHidden:YES];
    
    UILabel *labTitle = (UILabel *)[_topToolbar viewWithTag:1];
    if (nil == labTitle) {
        labTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, self.view.frame.size.width-60*2, 44)];
        [labTitle setTag:1];
        [labTitle setBackgroundColor:[UIColor clearColor]];
        [labTitle setFont:WFONT(18)];
//        [labTitle setShadowColor:UIColorFromRGB(0x7f2700)];
//        [labTitle setShadowOffset:CGSizeMake(0, 0.5)];
        [labTitle setTextAlignment:NSTextAlignmentCenter];
        [_topToolbar addSubview:labTitle];
        [labTitle release];
        [labTitle setTextColor:[UIColor whiteColor]];
    }
    
    UIImage *nav_Image  = [UIImage imageNamed:@"navtop_bar_bg.png"];
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
        [self.navigationController.navigationBar setBackgroundImage:nav_Image forBarMetrics:UIBarMetricsDefault];
    }
    
    [self.view setBackgroundColor:UIColorFromRGB(0xededed)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    AppDelegate *dele = (AppDelegate *)[AppManager getAppDelegate];
//    [dele.indexViewController.leftARightShowsView setLeftSpace:0];
    
    StatusBarWindow *statusBarWindow = (StatusBarWindow *)[AppManager getStateBar];
    [statusBarWindow removeAllViews];
    [statusBarWindow setHidden:NO];
    
    UIButton *btnStatusBar = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnStatusBar setFrame:CGRectMake(0, 0, 320, 20)];
    [btnStatusBar setBackgroundColor:[UIColor clearColor]];
    [btnStatusBar addTarget:self action:NSSelectorFromString(@"topStatusClick:") forControlEvents:UIControlEventTouchUpInside];
    [statusBarWindow addSubview:btnStatusBar];
    
    [self.view bringSubviewToFront:_topToolbar];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}
//

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)setTopToolbarWithHidden:(BOOL)yesOrNo {
    [_topToolbar setHidden:yesOrNo];
    
    if (yesOrNo) {
        [_topToolbar setFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
        [_topToolbar setHidden:YES];
    }
    else {
//        [_topToolbar setFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [self.view bringSubviewToFront:_topToolbar];
        [_topToolbar setHidden:NO];
        
        if (isIOS7) {
            [_topToolbar setFrame:CGRectMake(0, 0, _topToolbar.frame.size.width, 64)];
        }
        else {
            [_topToolbar setFrame:CGRectMake(0, -20, _topToolbar.frame.size.width, 44)];
        }
    }
}

- (void)setTopToolTitle:(NSString *)title {
    UILabel *labTitle = (UILabel *)[_topToolbar viewWithTag:1];
    [labTitle setText:title];
}

- (void)setTopToolRightTitle:(NSString *)title {
    UILabel *labTitle = (UILabel *)[_topToolbar viewWithTag:1];
    [labTitle setTextAlignment:NSTextAlignmentLeft];
    
    [labTitle setText:title];
}

- (void)setTopToolBackBarWithHidden:(BOOL)yesOrNo {
    if (!yesOrNo) {
        [_topToolbar setLeftButton:[UIImage imageNamed:@"ico_back.png"] target:self action:NSSelectorFromString(@"back:")];
    }
    else {
        [_topToolbar.topLeftView setHidden:YES];
    }
}

- (void)setTopToolToHomeBarWithHidden:(BOOL)yesOrNo {
    if (!yesOrNo) {
        [_topToolbar setRightButton:[UIImage imageNamed:@"maps_tohomeicon.png"] target:self action:NSSelectorFromString(@"toHomeTapping:")];
    }
    else {
        [_topToolbar.topLeftView setHidden:YES];
    }
}

- (void)setBottomToolbarWithHidden:(BOOL)yesOrNo {
    [_bottomToolbar setHidden:yesOrNo];
    
    CGRect appFrame = [AppManager getAppFrame];
    
    if (yesOrNo) {
        [_bottomToolbar setFrame:CGRectMake(0, appFrame.size.height, self.view.frame.size.width, 0)];
        [_bottomToolbar setHidden:YES];
    }
    else {
        [self.view bringSubviewToFront:_bottomToolbar];
        [_bottomToolbar setHidden:NO];

        [_bottomToolbar setFrame:CGRectMake(0, appFrame.size.height-44, _bottomToolbar.frame.size.width, 44)];
    }
}

- (void)setBottomToolBackBarWithHidden:(BOOL)yesOrNo {
    if (!yesOrNo) {
        [_bottomToolbar setLeftButton:[UIImage imageNamed:@"scenic_backbutton.png"] target:self action:NSSelectorFromString(@"back:")];
    }
    else {
        [_bottomToolbar.topLeftView setHidden:YES];
    }
}

- (void)setBottomToolToHomeBarWithHidden:(BOOL)yesOrNo {
    if (!yesOrNo) {
        [_bottomToolbar setRightButton:[UIImage imageNamed:@"scenic_tohomebutton_h.png"] target:self action:NSSelectorFromString(@"toHomeTapping:")];
    }
    else {
        [_bottomToolbar.topLeftView setHidden:YES];
    }
}

- (void)toHomeTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    NSArray *viewControllers = [dele.nav viewControllers];
    
    for (NVParentViewController *pViewController in viewControllers) {
        if ([pViewController isKindOfClass:[CMHomeViewController class]]) {
            [dele.nav popToViewController:pViewController animated:YES];
            break;
        }
        else {
            [pViewController clearData];
        }
    }
}

- (void)setTopToolBackBarWithTitle:(NSString *)title {
    UIButton *btnBack = (UIButton *)[_topToolbar viewWithTag:2];
    if (nil == btnBack) {
        btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setTag:2];
        [btnBack setFrame:CGRectMake(10, 6, 47, 32)];
        [btnBack addTarget:self action:NSSelectorFromString(@"back:") forControlEvents:UIControlEventTouchUpInside];
        
        [_topToolbar addSubview:btnBack];
    }
    
    CGSize titleSize = [title sizeWithFont:btnBack.titleLabel.font constrainedToSize:CGSizeMake(1000, 32) lineBreakMode:NSLineBreakByCharWrapping];
    
    [btnBack setTitle:title forState:UIControlStateNormal];
    
    [btnBack setBackgroundImage:[[UIImage imageNamed:@"home_default.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:10] forState:UIControlStateNormal];
    [btnBack setBackgroundImage:[[UIImage imageNamed:@"home_press.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:10] forState:UIControlStateHighlighted];
    
    [btnBack setFrame:CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, titleSize.width, btnBack.frame.size.height)];
}

- (void)setTopToolRightBarWithTitle:(NSString *)title {
    UIButton *btnBack = (UIButton *)[_topToolbar viewWithTag:3];
    if (nil == btnBack) {
        btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setTag:3];
        [btnBack setFrame:CGRectMake(320-10-47, 6, 47, 32)];
        [btnBack addTarget:self action:NSSelectorFromString(@"back:") forControlEvents:UIControlEventTouchUpInside];
        
        [_topToolbar addSubview:btnBack];
    }
    
    CGSize titleSize = [title sizeWithFont:btnBack.titleLabel.font constrainedToSize:CGSizeMake(1000, 32) lineBreakMode:NSLineBreakByCharWrapping];
    
    [btnBack setTitle:title forState:UIControlStateNormal];
    
    [btnBack setBackgroundImage:[[UIImage imageNamed:@"home_default.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:10] forState:UIControlStateNormal];
    [btnBack setBackgroundImage:[[UIImage imageNamed:@"home_press.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:10] forState:UIControlStateHighlighted];
    
    [btnBack setFrame:CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, titleSize.width, btnBack.frame.size.height)];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [_topToolbar release];_topToolbar=nil;
    [_bottomToolbar release];_bottomToolbar=nil;
    //	[_waitView removeFromSuperview];
    [super dealloc];
}

- (void)alertMsg:(NSString *)msg {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

- (void)back:(id)sender {
    [_service.downQueue cancelAllOperations];
	[_service.downQueue reset];
	[_service release];_service=nil;
	
	[_delegate release];_delegate=nil;
	
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightTapping:(id)sender {
    
}

- (void)clearData {
    [_service.downQueue cancelAllOperations];
	[_service.downQueue reset];
	[_service release];_service=nil;
	
	[_delegate release];_delegate=nil;
}

- (void)showLoading:(BOOL)yesOrNo msg:(NSString *)msg {
    CGRect rect = [AppManager getAppFrame];
//    rect.size.height+=20;
    
    UIView *loadingView = [self.view viewWithTag:VIEW_LOADING1_TAG];
    if (nil == loadingView) {
        loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
        [loadingView setTag:VIEW_LOADING1_TAG];
        [loadingView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:loadingView];
        [loadingView release];
        [loadingView setClipsToBounds:YES];
        
        UIView *loadingBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, loadingView.frame.size.width, 100000)];
        [loadingBGView setBackgroundColor:[UIColor blackColor]];
        [loadingBGView setAlpha:0.6];
        [loadingView addSubview:loadingBGView];
        [loadingBGView release];
        
        UIView *loadingView1 = [[UIView alloc] init];
        [loadingView1 setTag:1002];
        [loadingView1 setAlpha:0.6];
        [loadingView1 setBackgroundColor:[UIColor blackColor]];
        [loadingView1 setFrame:CGRectMake((rect.size.width-200)/2, (rect.size.height-100-_topToolbar.frame.size.height-_bottomToolbar.frame.size.height)/2, 200, 100)];
        loadingView1.layer.masksToBounds=YES;
        loadingView1.layer.cornerRadius = 5;
        loadingView1.clipsToBounds = YES;
        [loadingView addSubview:loadingView1];
        [loadingView1 release];
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, (rect.size.height-20-_topToolbar.frame.size.height-_bottomToolbar.frame.size.height)/2, loadingView.frame.size.width, 20)];
        [lab setTag:1001];
        [lab setFont:WFONT_BOLD(16)];
        [lab setTextAlignment:NSTextAlignmentCenter];
        [lab setTextColor:[UIColor whiteColor]];
        [lab setBackgroundColor:[UIColor clearColor]];
        [loadingView addSubview:lab];
        [lab release];
        
        UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [activityView startAnimating];
        [activityView setFrame:CGRectMake((rect.size.width-30)/2, (rect.size.height-30-_topToolbar.frame.size.height-_bottomToolbar.frame.size.height)/2+25, 30, 30)];
        [loadingView addSubview:activityView];
        [activityView release];
    }
    [loadingView setFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
    
    for (UILabel *lab in [loadingView subviews]) {
        if ([lab isKindOfClass:[UILabel class]]) {
            [lab setText:msg];
            break;
        }
    }
    
    if (yesOrNo) {
        [loadingView setAlpha:1.];
    }
    else {
        [loadingView setAlpha:0.];
    }
    
    [self.view bringSubviewToFront:loadingView];
}

- (void)showInitLoading:(BOOL)yesOrNo {
    CGRect appFrame = [AppManager getAppFrame];
    
    UIView *initLoadingView = [self.view viewWithTag:200013];
    if (yesOrNo) {
        if (nil == initLoadingView) {
            initLoadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, appFrame.size.width, appFrame.size.height-44)];
            [initLoadingView setTag:200013];
            [initLoadingView setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
            [self.view addSubview:initLoadingView];
            [initLoadingView release];
            
            UIImageView *initLoadingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, (appFrame.size.height-44-156)/2., 320, 156)];
            [initLoadingImageView setImage:[UIImage imageNamed:@"parentViewInitLoading.png"]];
            [initLoadingView addSubview:initLoadingImageView];
            [initLoadingImageView release];
        }
    }
    else {
        [initLoadingView removeFromSuperview];
    }
}

- (void)btnTapping:(id)sender {
    
}

//任务栏点击事件
- (void)topStatusClick:(id)sender {
    
}

#pragma mark UIPopController delegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    
}

#pragma mark UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGRect appRect = [AppManager getAppFrame];
//    
//    if (scrollView.contentOffset.x<=0) {
//        [scrollView setContentOffset:CGPointZero];
//    }
//    else if (scrollView.contentOffset.x>=appRect.size.width) {
//        [self back:nil];
//    }
}

@end
