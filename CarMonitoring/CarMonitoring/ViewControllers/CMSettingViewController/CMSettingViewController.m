//
//  CMSettingViewController.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 60
#define TABLEVIEW_TAG   1001

#import "CMSettingViewController.h"
#import "CMMyInfoCellView.h"
#import "AppManager.h"

@interface CMSettingViewController () {
    UISwitch *switch1;
    UISwitch *switch2;
    UISwitch *switch3;
}

@end

@implementation CMSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect appFrame = [AppManager getAppFrame];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [bgView setImage:[UIImage imageNamed:@"pic_bg.png"]];
    [self.view addSubview:bgView];
    [bgView release];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"设置"];
    [self setTopToolToHomeBarWithHidden:NO];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [tableView setTag:TABLEVIEW_TAG];
    [tableView setRowHeight:ROW_HIG];
	[tableView setDataSource:self];
	[tableView setDelegate:self];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:tableView];
	[tableView release];
    [tableView setBackgroundColor:[UIColor clearColor]];
    
    switch1 = [[UISwitch alloc] initWithFrame:CGRectMake(260, 15, 80, 20)];
    switch2 = [[UISwitch alloc] initWithFrame:CGRectMake(260, 15, 80, 20)];
    switch3 = [[UISwitch alloc] initWithFrame:CGRectMake(260, 15, 80, 20)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back:(id)sender {
    [super back:sender];
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView removeFromSuperview];
    
    [switch1 release];
    [switch2 release];
    [switch3 release];
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 1;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        [cell.textLabel setTextColor:UIColorFromRGB(0x383838)];
        [cell.textLabel setFont:WFONT(16)];
        
        CMMyInfoCellView *cellView = [[CMMyInfoCellView alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [cellView setTag:1];
        [cell.contentView addSubview:cellView];
        [cellView release];
	}
    CMMyInfoCellView *cellView = (CMMyInfoCellView *)[cell.contentView viewWithTag:1];
    if (0 == indexPath.row%2) {
        [cellView.bgImageView setImage:[UIImage imageNamed:@"carlistbg1.png"]];
    }
    else {
        [cellView.bgImageView setImage:[UIImage imageNamed:@"carlistbg2.png"]];
    }
    
    [cellView.imgIconView setHidden:YES];
    [cellView.labTitle2 setText:@""];
    
    switch (indexPath.row) {
        case 0:
        {
            [cellView.labTitle1 setText:@"接收推送信息"];
            [cell.contentView addSubview:switch1];
        }
            break;
        case 1:
        {
            [cellView.labTitle1 setText:@"消息铃声"];
            [cell.contentView addSubview:switch2];
        }
            break;
        case 2:
        {
            [cellView.labTitle1 setText:@"消息震动"];
            [cell.contentView addSubview:switch3];
        }
            break;
            
        default:
            break;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
}

@end
