//
//  CMSettingViewController.h
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"

@interface CMSettingViewController : NVParentViewController <UITableViewDelegate,UITableViewDataSource>

@end
