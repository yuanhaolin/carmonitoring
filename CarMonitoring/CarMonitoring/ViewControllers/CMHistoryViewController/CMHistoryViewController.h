//
//  CMHistoryViewController.h
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"

@interface CMHistoryViewController : NVParentViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIActionSheetDelegate>

@property (nonatomic,assign) BOOL isMulti;

- (void)setItems:(NSArray *)items_;

@end
