//
//  CMHistoryViewController.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 61
#define TABLEVIEW_TAG   1001
#define BTN_SECTION_BEGIN_TAG   30001
#define ACTIONSHEET_WARN_TAG    3001

#import "CMHistoryViewController.h"
#import "AppManager.h"
#import "CDTVCarInfoSectionView.h"
#import "CDTVCatInfoTextCell1View.h"
#import "CDTVCatInfoTextCell2View.h"
#import "CMHistoryTopView.h"
#import "CMHistoryDescViewController.h"
#import "AppDelegate.h"
#import "CDTVNavigationController.h"
#import "CMUserInfoAlrmsManager.h"
#import "CDTVMapsViewController.h"

@interface CMHistoryViewController () {
    CMHistoryTopView *_hisTopView;
    
    NSMutableArray *_items;
    
    NSArray *_vehicleArray;
    
    NSMutableArray *selectCellIndexArray;
    
    int _alarmIndex;
    
    CDTVMapsViewController *mapsViewController;
}

@end

@interface CMHistoryViewController (Private)
- (void)changeView;

@end

@implementation CMHistoryViewController (Private)
- (void)changeView {
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

@end

@implementation CMHistoryViewController
@synthesize isMulti;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _items = [NSMutableArray new];

    CGRect appFrame = [AppManager getAppFrame];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [bgView setImage:[UIImage imageNamed:@"pic_bg.png"]];
    [self.view addSubview:bgView];
    [bgView release];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"历史信息"];
    [self setTopToolToHomeBarWithHidden:NO];
    
    _hisTopView = [[CMHistoryTopView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, 44)];
    [self.view addSubview:_hisTopView];
    [_hisTopView release];
    [_hisTopView.textField setDelegate:self];
    [_hisTopView.btnTap addTarget:self action:NSSelectorFromString(@"btnSearchTapping:") forControlEvents:UIControlEventTouchUpInside];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _hisTopView.frame.size.height+_hisTopView.frame.origin.y, 320, appFrame.size.height-_hisTopView.frame.size.height-_hisTopView.frame.origin.y)];
    [tableView setTag:TABLEVIEW_TAG];
    [tableView setRowHeight:ROW_HIG];
	[tableView setDataSource:self];
	[tableView setDelegate:self];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:tableView];
	[tableView release];
    [tableView setBackgroundColor:[UIColor clearColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldWillChange:) name:UITextFieldTextDidChangeNotification object:nil];
    
    selectCellIndexArray = [NSMutableArray new];
    [selectCellIndexArray addObject:[NSNumber numberWithInt:0]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_items release];
    [selectCellIndexArray release];
    [super dealloc];
}

- (void)setItemsTimerOut:(id)sender {
    for (VehicleInfo *vi in _vehicleArray) {
        [_items addObjectsFromArray:[[CMUserInfoAlrmsManager getInstance] getWarnList:vi.vehicleId]];
    }
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

- (void)setItems:(NSArray *)items_ {
    [_vehicleArray release];_vehicleArray=nil;
    _vehicleArray = [items_ retain];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setItemsTimerOut:") userInfo:nil repeats:NO];
}

#pragma mark UIButton
- (void)btnTapping:(id)sender {
    UIButton *btn = (UIButton *)sender;
    int index = btn.tag-BTN_SECTION_BEGIN_TAG;
    
    NSNumber *number = [NSNumber numberWithInt:index];
    if ([selectCellIndexArray containsObject:number]) {
        [selectCellIndexArray removeObject:number];
    }
    else {
        [selectCellIndexArray addObject:number];
    }
    
    [self changeView];
}

- (void)btnSearchTapping:(id)sender {
    [_hisTopView.textField resignFirstResponder];
    [_hisTopView.textField setText:@""];
    
    [self textFieldWillChange:nil];
}

- (void)back:(id)sender {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    
    [super back:sender];
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 2;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = 0;
    
    NSNumber *sectionNumber = [NSNumber numberWithInt:section];
    
    switch (section) {
        case 0:
        {
            if ([selectCellIndexArray containsObject:sectionNumber]) {
                count = [_items count];
            }
        }
            break;
        case 1:
        {
            if ([selectCellIndexArray containsObject:sectionNumber]) {
                count = 0;
            }
        }
            break;
        default:
            break;
    }
    return count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CDTVCarInfoSectionView *carInfoSectionView = [[CDTVCarInfoSectionView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [carInfoSectionView.btnTap setTag:BTN_SECTION_BEGIN_TAG+section];
    if (0 == section%2) {
        [carInfoSectionView.bgImageView setImage:[UIImage imageNamed:@"carlistbg1.png"]];
    }
    else {
        [carInfoSectionView.bgImageView setImage:[UIImage imageNamed:@"carlistbg2.png"]];
    }
    if (0 == section) {
        [carInfoSectionView.labTitle setText:@"报警信息"];
    }
    else if (1 == section) {
        [carInfoSectionView.labTitle setText:@"通知信息"];
    }
    [carInfoSectionView.btnTap addTarget:self action:NSSelectorFromString(@"btnTapping:") forControlEvents:UIControlEventTouchUpInside];
    
    return [carInfoSectionView autorelease];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        CDTVCatInfoTextCell1View *catInfoTextCell1View = [[CDTVCatInfoTextCell1View alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [catInfoTextCell1View setTag:1];
        [cell.contentView addSubview:catInfoTextCell1View];
        [catInfoTextCell1View release];
        
        CDTVCatInfoTextCell2View *catInfoTextCell2View = [[CDTVCatInfoTextCell2View alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [catInfoTextCell2View setTag:2];
        [cell.contentView addSubview:catInfoTextCell2View];
        [catInfoTextCell2View release];
	}
    CDTVCatInfoTextCell1View *catInfoTextCell1View = (CDTVCatInfoTextCell1View *)[cell.contentView viewWithTag:1];
    CDTVCatInfoTextCell2View *catInfoTextCell2View = (CDTVCatInfoTextCell2View *)[cell.contentView viewWithTag:2];
    
    [catInfoTextCell1View.viewNumer setHidden:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            [catInfoTextCell1View setHidden:YES];
            [catInfoTextCell2View setHidden:NO];
            
            NSArray *items = _items;
            Alarm *alarm = [items objectAtIndex:indexPath.row];
            
            VehicleInfo *vi = [[CMUserInfoAlrmsManager getInstance] objectVehicleInfoFromAlarm:alarm items_:_vehicleArray];
            VehicleReport *vr = [[CMUserInfoAlrmsManager getInstance] objectVehicleReportFromAlarm:alarm items_:_vehicleArray];
            
            NSMutableString *tags = [NSMutableString stringWithFormat:@"%@ ",vi.licencePlateNumber];
            
            for (NSString *tag in  alarm.tags) {
                [tags appendString:tag];
            }
            
            [catInfoTextCell2View.labTitle1 setText:tags];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];
            NSString *strDate = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:vr.location.time]];
            
            [catInfoTextCell2View.labTitle2 setText:strDate];
            
            [dateFormatter release];
        }
            break;
        case 1:
        {
            [catInfoTextCell1View setHidden:YES];
            [catInfoTextCell2View setHidden:NO];
            
            [catInfoTextCell2View.labTitle1 setText:@"川A 15234违章"];
            [catInfoTextCell2View.labTitle2 setText:@"2013-11-30 15:20"];
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            _alarmIndex = indexPath.row;
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"地图" otherButtonTitles:@"详情", nil];
            [actionSheet setTag:ACTIONSHEET_WARN_TAG];
            [actionSheet showInView:self.view];
            [actionSheet release];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_items removeAllObjects];
    [_items addObjectsFromArray:[[CMUserInfoAlrmsManager getInstance] searchCarWarnList:_hisTopView.textField.text items:_vehicleArray]];
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
    
    [_hisTopView.textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldWillChange:(id)sender {
    [_items removeAllObjects];
    [_items addObjectsFromArray:[[CMUserInfoAlrmsManager getInstance] searchCarWarnList:_hisTopView.textField.text items:_vehicleArray]];
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

- (void)actionSheetTimerOut:(id)sender {
//    NSArray *items = _items;
//    Alarm *alarm = [items objectAtIndex:_alarmIndex];
//    
//    VehicleInfo *vi = [[CMUserInfoAlrmsManager getInstance] objectVehicleInfoFromAlarm:alarm items_:_vehicleArray];
    
    [mapsViewController setTopToolTitle:@"地图"];
}

#pragma mark UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (ACTIONSHEET_WARN_TAG == actionSheet.tag) {
        switch (buttonIndex) {
            case 0:
            {
                NSArray *items = _items;
                Alarm *alarm = [items objectAtIndex:_alarmIndex];
                
                VehicleInfo *vi = [[CMUserInfoAlrmsManager getInstance] objectVehicleInfoFromAlarm:alarm items_:_vehicleArray];
                
                AppDelegate *dele = [AppManager getAppDelegate];
                mapsViewController = [[CDTVMapsViewController alloc] initWithNibName:@"CDTVMapsViewController" bundle:nil];
                [dele.nav pushViewController:mapsViewController animated:YES];
                [mapsViewController release];
                
                NSMutableArray *arrayLocations = [NSMutableArray array];
                
                PBArray *vrs = vi.processingAlarms;
                for (int i=0; i<[vrs count]; ++i) {
                    VehicleReport *vr = [vrs objectAtIndex:i];
                    [arrayLocations addObject:vr.location];
                }
                
                BasicLocation *bl = [arrayLocations objectAtIndex:0];
//                [mapsViewController setCenter:CLLocationCoordinate2DMake(bl.latitude, bl.longitude)];
                [mapsViewController setCenter:CLLocationCoordinate2DMake(bl.latitude, bl.longitude) title:alarm.message gpses:nil];
                
                [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"actionSheetTimerOut:") userInfo:nil repeats:NO];
            }
                break;
            case 1:
            {
                NSArray *items = _items;
                Alarm *alarm = [items objectAtIndex:_alarmIndex];
                
                VehicleInfo *vi = [[CMUserInfoAlrmsManager getInstance] objectVehicleInfoFromAlarm:alarm items_:_vehicleArray];
                
                AppDelegate *dele = [AppManager getAppDelegate];
                CMHistoryDescViewController *historeDescViewController = [[CMHistoryDescViewController alloc] init];
                [dele.nav pushViewController:historeDescViewController animated:YES];
                [historeDescViewController release];
                
                NSMutableArray *arrayAlarm = [NSMutableArray array];
                NSMutableArray *arrayLocations = [NSMutableArray array];
                
                PBArray *vrs = vi.processingAlarms;
                for (int i=0; i<[vrs count]; ++i) {
                    VehicleReport *vr = [vrs objectAtIndex:i];
                    [arrayAlarm addObject:vr.alarm];
                    [arrayLocations addObject:vr.location];
                }
                
                [historeDescViewController setItemAlarm:alarm vehicleInfo:vi vehicleReport:[vrs objectAtIndex:0]];
            }
                break;
            case 2:
            {
                
            }
                break;
            default:
                break;
        }
    }
}

@end
