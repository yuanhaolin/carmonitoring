//
//  CDTVMapsViewController.h
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"
#import "BMapKit.h"

@class MonitorVehicle;
@interface CDTVMapsViewController : NVParentViewController <BMKMapViewDelegate, BMKSearchDelegate,UIActionSheetDelegate> {
    IBOutlet UIView *commandView;
    IBOutlet BMKMapView* _mapView;
    IBOutlet UIButton *btnBaiduMap;
    
    IBOutlet UIActivityIndicatorView *activityIView;
}

- (void)setIsHis:(BOOL)yesOrNo;
//实时地图获取
- (void)setItemMonitorVehicles:(NSArray *)items_;

- (void)setCenter:(CLLocationCoordinate2D )location;

- (void)setCenter:(CLLocationCoordinate2D )location title:(NSString *)title;

- (void)setCenter:(CLLocationCoordinate2D )location_ title:(NSString *)title gpses:(NSArray *)gpses;



@end
