//
//  CDTVMapsViewController.m
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define BEGIN_LINE_TAG  300001

#define ALARM_TAG 10001
#define MAPNAV_TAG 10002

#define MYBUNDLE_NAME @ "mapapi.bundle"
#define MYBUNDLE_PATH [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: MYBUNDLE_NAME]
#define MYBUNDLE [NSBundle bundleWithPath: MYBUNDLE_PATH]

#import "CDTVMapsViewController.h"
#import "AppManager.h"
#import "CDTVMapsBottomView.h"
#import "CMWarnView.h"
#import "UIUitl.h"
#import "BMKNavigation.h"
#import "MCCGPSObject.h"
#import "CMSelectTime2View.h"
#import "CMServiceManager.h"
#import "Cm.pb.h"
#import "BMKMapView.h"
#import "CMMapMonitorVehicleManager.h"
#import "CMMapHistoryManager.h"
#import "CMGPSEntity.h"
#import "CLLocation+YCLocation.h"
#import "CMGetNavGPSObject.h"

typedef enum _MapsViewType {
    MAPHistory = 0, //历史
    MAPMonitor = 1, //实时
    MAPNAV = 2, //导航中
}MapsViewType;

@interface RouteAnnotation : BMKPointAnnotation
{
	int _type; ///<0:起点 1：终点 2：报警点
	int _degree;
    long long _id;
}

@property (nonatomic) int type;
@property (nonatomic) int degree;
@property (nonatomic,assign) long long id;
@end

@implementation RouteAnnotation
@synthesize type = _type;
@synthesize degree = _degree;
@synthesize id=_id;

@end

@interface CDTVMapsViewController () {
    BMKSearch* _search;
    
    CMWarnView *_warnView;
    
    //是否是历史轨迹
    MapsViewType _mapType;
    
    CMSelectTime2View *_selectTime2View;
    CDTVMapsBottomView *mapsBottomView;
    
    NSMutableArray *_timesList;
    
    long long _historyStartTime;
    long long _historyEndTime;
    
    long long _monitorStartTime;
    long long _monitorCurrentStartTime;
    long long _monitorEndTime;
    NSTimer *_monitorEndTimeOut;
    
    //实时地图item
    NSArray *_items;
    
    //当前正在下载gps坐标的车辆id
    int32_t _vehicle_id;
    
    NSArray *colors;
    
    //当前第几条Line
    int _lineIndex;
    
    //是否正在下载GPS数据
    BOOL _isLoadGPS;
    
    //设置当前坐标
    CLLocationCoordinate2D _center;
    NSString *_centerTitle;
    
    NSTimer *showMonitorVehicleGPSTimer;
    NSTimer *setIsHisTimerOutTimer;
    NSTimer *beginRequestHistoryTimer;
    NSTimer *timerOutTimer;
    NSTimer *setCenter2TimerOutTimer;
    NSTimer *setCenterTimerOutTimer;
    
    NSTimer *showHistoryTimer;
    
    NSTimer *_meNavTimer;
    
    BOOL _isFirst;
    
    NSArray *_mapCurrentLine;
    
    int _selectCarIndex;
    
    BOOL _isFirstHistory;
}

@end

@interface CDTVMapsViewController (Private)

//结束时间一直在变
- (void)timerOutMonitorTime ;

//开始下载实时地图需要的gps信息
- (void)getMonitorVehicleMapsPath:(VehicleReport *)beginVR endVR:(VehicleReport *)endVR;
//添加历史轨迹数据
- (void)showQueryHistoryMapsPath;

//显示实时地图数据
- (void)showMonitorVehicleGPS;

//开始请求历史轨迹
- (void)beginRequestHistory;

- (void)showHistoryLoading:(BOOL)yesOrNo msg:(NSString *)msg;

@end

@implementation CDTVMapsViewController (Private)
- (void)timerOutMonitorTime {
    NSLog(@"timerOutMonitorTime");
    
    _monitorEndTime++;
    
    //    if (!_isHistory) {
    if (MAPMonitor == _mapType) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:_monitorEndTime];
        NSString *endStrDate = [dateFormatter stringFromDate:endDate];
        
        [_selectTime2View setEndTime:_monitorEndTime];
        
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateNormal];
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateSelected];
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateHighlighted];
        
        [dateFormatter release];
    }
    
    NSLog(@"timerOutMonitorTime1");
}

- (void)getMonitorVehicleMapsPath:(VehicleReport *)beginVR endVR:(VehicleReport *)endVR {
    NSLog(@"getMonitorVehicleMapsPath");
    
    //    MCCGPSObject *myGPSObject = [MCCGPSObject getInstance];
    if (nil == beginVR || nil == endVR) {
        return;
    }
    
    BMKPlanNode* start = [[[BMKPlanNode alloc]init] autorelease];
	[start setPt:CLLocationCoordinate2DMake(beginVR.location.latitude, beginVR.location.longitude)];
	BMKPlanNode* end = [[[BMKPlanNode alloc]init] autorelease];
    [end setPt:CLLocationCoordinate2DMake(endVR.location.latitude, endVR.location.longitude)];
    
    NSLog(@"获取实时地图的线：start：%lf--%lf  end：%lf--%lf",start.pt.latitude,
          start.pt.longitude,end.pt.latitude,end.pt.longitude);
    
    _isLoadGPS = YES;
	BOOL flag = [_search drivingSearch:@"" startNode:start endCity:@"" endNode:end];
    
	if (flag) {
		NSLog(@"search success.");
	}
    else{
        NSLog(@"search failed!");
    }
}

- (void)showQueryHistoryMapsPath {
    if (MAPMonitor == _mapType || MAPNAV == _mapType) {
        return;
    }
    _lineIndex=0;
    
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
	[_mapView removeAnnotations:array];
	array = [NSArray arrayWithArray:_mapView.overlays];
	[_mapView removeOverlays:array];
    
    NSArray *gpsArray = [[CMMapHistoryManager getInstance] getGPS:_historyStartTime endTime:_historyEndTime];
    
    BOOL isData = NO;
    for (NSArray *array in gpsArray) {
        if ([array count]>0) {
            isData = YES;
        }
    }
    
    if (NO == isData && MAPHistory == _mapType) {
        [self alertMsg:@"无轨迹数据"];
        
        _isFirstHistory = NO;
    }
    
    NSMutableArray *arrayLine = [NSMutableArray array];
    
//    int split = 5;
//    
//    // 下面开始计算路线，并添加驾车提示点
//    int index = 0;
//    //            int size = [plan.routes count];
//    for (int i = 0; i < 1; i++) {
//        BMKRoute* route = [plan.routes objectAtIndex:i];
//        for (int j = 0; j < route.pointsCount; j++) {
//            int len = floor([route getPointsNum:j]/split);
//            index += (len+1);
//        }
//    }
//    
//    //            long long time = [[NSDate date] timeIntervalSince1970];
//    
//    BMKMapPoint* points = new BMKMapPoint[index];
//    index = 0;
//    for (int i = 0; i < 1; i++) {
//        BMKRoute* route = [plan.routes objectAtIndex:i];
//        for (int j = 0; j < route.pointsCount; j++) {
//            int len = floor([route getPointsNum:j]/split);
//            int oldLen = [route getPointsNum:j];
//            
//            BMKMapPoint* pointArrayTmp = (BMKMapPoint*)[route getPoints:j];
//            
//            BMKMapPoint *pointArray = new BMKMapPoint[len+1];
//            for (int m=0; m<len; ++m) {
//                pointArray[m] = pointArrayTmp[m*split];
//                //                        BMKMapPoint p = pointArray[m];
//                //                        NSLog(@"%@",BMKStringFromMapPoint(p));
//            }
//            pointArray[len] = pointArrayTmp[oldLen-1];
//            
//            memcpy(points + index, pointArray, (len+1) * sizeof(BMKMapPoint));
//            index += len+1;
//            
//            delete []pointArray;
//        }
//    }
    
    //添加报警点 所有的点
    NSMutableArray *warnsAnns = [NSMutableArray array];
    
//    int split = 1;
    
    for (NSArray *pointArray in gpsArray) {
//        int index = floor([pointArray count]/split);
        int index = [pointArray count];
        BMKMapPoint* points1 = new BMKMapPoint[index];
        
        for (int i=0; i<index; i++) {
            CMGPSEntity *gpsEntity = [pointArray objectAtIndex:i];
            float lat = gpsEntity.lat;
            float lon = gpsEntity.lon;
            
            if (0 == i) {
                [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(lat, lon)];
                
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item.coordinate = CLLocationCoordinate2DMake(lat, lon);
                item.title = gpsEntity.licencePlateNumber;
                item.type = 0;
                [warnsAnns addObject:item]; // 添加起点标注
                [item release];
            }
            if (i == index-1) {
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item.coordinate = CLLocationCoordinate2DMake(lat, lon);
                item.type = 1;
                item.title = gpsEntity.licencePlateNumber;
                [warnsAnns addObject:item]; // 终点标注
                [item release];
            }
            
            CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lat, lon);
            BMKMapPoint point = BMKMapPointForCoordinate(coor);
            points1[i] = point;
        }
//        if ([pointArray count]>0) {
//            CMGPSEntity *gpsEntity = [pointArray objectAtIndex:[pointArray count]-1];
//            float lat = gpsEntity.lat;
//            float lon = gpsEntity.lon;
//            
//            CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lat, lon);
//            BMKMapPoint point = BMKMapPointForCoordinate(coor);
//            points1[index+1] = point;
//        }
//        
        if (index>0) {
            // 通过points构建BMKPolyline
            BMKPolyline* polyLine1 = [BMKPolyline polylineWithPoints:points1 count:index];
            [arrayLine addObject:polyLine1];
        }
        
        delete [] points1;
    }
    [_mapView addOverlays:arrayLine]; // 添加路线overlay
    
    NSMutableArray *warns = [NSMutableArray array];
    for (VehicleInfo *vi in _items) {
        NSArray *ws = [[CMMapHistoryManager getInstance] getWarnList:vi.vehicleId];
        
        [warns addObjectsFromArray:ws];
    }
    
    for (int i=0; i<[warns count]; ++i) {
        VehicleReport *vr = (VehicleReport *)[warns objectAtIndex:i];
        if ([vr isKindOfClass:[VehicleReport class]] && 0 == vr.alarm.alarmId) {
            [warns removeObject:vr];
        }
    }
    
    for (VehicleReport *vr in warns) {
        if ([vr isKindOfClass:[CMGPSEntity class]]) {
            continue;
        }
        NSMutableString *tags = [NSMutableString string];
        for (NSString *s in vr.alarm.tags) {
            [tags appendFormat:@"%@ ",s];
        }
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:vr.location.latitude longitude:vr.location.longitude];
        CLLocation *newLocation = [location locationMarsFromEarth];
        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
        
        RouteAnnotation* item = [[RouteAnnotation alloc]init];
        item.coordinate = CLLocationCoordinate2DMake(baiduLocation.coordinate.latitude, baiduLocation.coordinate.longitude);
        item.title = vr.alarm.message;
        item.type = 2;
        item.id = vr.alarm.alarmId;
        [warnsAnns addObject:item];
        [item release];
    }
    
    [_mapView addAnnotations:warnsAnns];
    
//    [_mapView setZoomLevel:13];
}

- (NSString*)getMyBundlePath1:(NSString *)filename
{
	NSBundle * libBundle = MYBUNDLE ;
	if ( libBundle && filename ){
		NSString * s=[[libBundle resourcePath ] stringByAppendingPathComponent : filename];
		return s;
	}
	return nil ;
}

- (void)showMonitorVehicleGPS {
    _lineIndex = 0;
    
    showMonitorVehicleGPSTimer = nil;
    if (MAPHistory == _mapType || MAPNAV == _mapType) {
        return;
    }
    
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
	array = [NSArray arrayWithArray:_mapView.overlays];
    [_mapView removeOverlays:array];
    
    //所有的点
    NSMutableArray *warnsAnns = [NSMutableArray array];
    
    for (VehicleInfo *vi in _items) {
        NSArray *gpsArray = [[CMMapMonitorVehicleManager getInstance] getVehiclesByIDFromGPS:vi.vehicleId startTime:_monitorStartTime];
        
        int index = [gpsArray count];
        
        BMKMapPoint* points1 = new BMKMapPoint[index];
        for (int i=0; i<[gpsArray count]; ++i) {
            CMGPSEntity *gpsEntity = [gpsArray objectAtIndex:i];
            float lat = gpsEntity.lat;
            float lon = gpsEntity.lon;
            
            if (0 == i) {
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item.coordinate = CLLocationCoordinate2DMake(lat, lon);
                item.title = gpsEntity.licencePlateNumber;
                item.type = 0;
                [warnsAnns addObject:item]; // 添加起点标注
                [item release];
                
                if (!_isFirst) {
                    [_mapView setZoomLevel:17];
                    
                    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"warnViewCenterTimerOut:") userInfo:[NSString stringWithFormat:@"%lf,%lf",lat,lon] repeats:NO];
                }
                
                _isFirst = YES;
            }
            else if (i == index-1) {
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item.coordinate = CLLocationCoordinate2DMake(lat, lon);
                item.type = 1;
                item.title = gpsEntity.licencePlateNumber;
                [warnsAnns addObject:item]; // 终点标注
                [item release];
                
                [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(lat, lon)];
            }
            
            CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lat, lon);
            BMKMapPoint point = BMKMapPointForCoordinate(coor);
            points1[i] = point;
        }
        // 通过points构建BMKPolyline
        BMKPolyline* polyLine1 = [BMKPolyline polylineWithPoints:points1 count:index];
        [_mapView addOverlay:polyLine1]; // 添加路线overlay
        
        delete [] points1;
    }
    
    //报警点
    NSMutableArray *warns = [NSMutableArray array];
    for (VehicleInfo *vi in _items) {
        NSArray *ws = [[CMMapMonitorVehicleManager getInstance] getWarnList:vi.vehicleId];
        
        [warns addObjectsFromArray:ws];
    }
    
    for (int i=0; i<[warns count]; ++i) {
        VehicleReport *vr = (VehicleReport *)[warns objectAtIndex:i];
        if ([vr isKindOfClass:[CMGPSEntity class]]) {
            continue;
        }
        if (0 == vr.alarm.alarmId) {
            [warns removeObject:vr];
        }
    }
    
    for (VehicleReport *vr in warns) {
        if ([vr isKindOfClass:[CMGPSEntity class]]) {
            continue;
        }
        if (_monitorStartTime>vr.location.time) {
            continue;
        }
        
        NSMutableString *tags = [NSMutableString string];
        for (NSString *s in vr.alarm.tags) {
            [tags appendFormat:@"%@ ",s];
        }
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:vr.location.latitude longitude:vr.location.longitude];
        CLLocation *newLocation = [location locationMarsFromEarth];
        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
        
        RouteAnnotation* item = [[RouteAnnotation alloc]init];
        item.coordinate = CLLocationCoordinate2DMake(baiduLocation.coordinate.latitude, baiduLocation.coordinate.longitude);
        item.title = vr.alarm.message;
        item.type = 2;
        item.id = vr.alarm.alarmId;
        [warnsAnns addObject:item];
        [item release];
    }
    
    [_mapView addAnnotations:warnsAnns];
}

- (void)beginRequestHistory {
    for (int i=0; i<[_items count]; ++i) {
        VehicleInfo *mv = (VehicleInfo *)[_items objectAtIndex:i];
        
        QueryHistory_Builder *qhb = [QueryHistory_Builder new];
        [qhb setType:QueryHistory_QueryTypeLocation];
        [qhb setStartTime:_historyStartTime];
        [qhb setEndTime:_historyEndTime];
        [qhb setVehicleId:mv.vehicleId];
        [qhb setSimpleVersion:YES];
        //开始历史轨迹
        [[CMServiceManager getInstance] getQueryHistory:qhb];
        [qhb release];
    }
    
    beginRequestHistoryTimer =nil;
    
    [self showHistoryLoading:YES msg:@""];
}

- (void)showHistoryLoadingTimer:(id)sender {
    [self showHistoryLoading:NO msg:@""];
}

- (void)showHistoryLoading:(BOOL)yesOrNo msg:(NSString *)msg {
    [activityIView setHidden:!yesOrNo];
    
    [showHistoryTimer invalidate];showHistoryTimer=nil;
    if (YES == yesOrNo) {
        showHistoryTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:NSSelectorFromString(@"") userInfo:nil repeats:NO];
    }
}

@end

@implementation CDTVMapsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolToHomeBarWithHidden:NO];
    
    CGRect appFrame = [AppManager getAppFrame];
    
    _search = [[BMKSearch alloc]init];
    [_mapView setFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [_mapView setShowsUserLocation:YES];
    
    mapsBottomView = [[CDTVMapsBottomView alloc] initWithFrame:CGRectMake(0, appFrame.size.height-44-44, 320, 44)];
    [mapsBottomView setDelegate:self];
    [self.view addSubview:mapsBottomView];
    [mapsBottomView release];
    
    _selectTime2View = [[CMSelectTime2View alloc] initWithFrame:CGRectMake(0, mapsBottomView.frame.size.height+mapsBottomView.frame.origin.y, 320, 44)];
    [_selectTime2View setDelegate:self];
    [self.view addSubview:_selectTime2View];
    [_selectTime2View release];
    
    _warnView = [[CMWarnView alloc] initWithFrame:CGRectMake(0, appFrame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [_warnView setDelegate:self];
    [self.view addSubview:_warnView];
    [_warnView release];
    
    [mapsBottomView setItmes:_timesList];
    
    //测试
//    NSString *strCurrentDate = @"2013-10-14";
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSDate *currentDate = [dateFormatter dateFromString:strCurrentDate];
//    [dateFormatter release];
//    _historyEndTime = [currentDate timeIntervalSince1970];
    _historyEndTime = [[NSDate date] timeIntervalSince1970];
    
    
    _historyStartTime = _historyEndTime-60*60*2;
    
    [mapsBottomView setStartTime:_historyStartTime];
    [mapsBottomView setEndTime:_historyEndTime];
    
    [_selectTime2View setStartTime:_historyStartTime];
    [_selectTime2View setEndTime:_historyEndTime];
    
    [_mapView setZoomLevel:16];
    
    colors = [[NSArray alloc] initWithObjects:UIColorFromRGB(0x59a0f7),
              UIColorFromRGB(0x3357a0),
              UIColorFromRGB(0x874fb6),
              UIColorFromRGB(0x1aad90),
              UIColorFromRGB(0x15c6d6),
              nil];
    
    [_monitorEndTimeOut invalidate];_monitorEndTimeOut=nil;
    _monitorEndTimeOut = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"timerOutMonitorTime") userInfo:nil repeats:YES];
    
    [activityIView startAnimating];
    
    BOOL isHistory = NO;
    if (MAPHistory == _mapType) {
        isHistory = YES;
    }
    if (isHistory) {
        [self setTopToolTitle:@"历史轨迹"];
    }
    else {
        [self setTopToolTitle:@"实时地图"];
    }
    
//    if (!CLLocationCoordinate2DIsValid(_center)) {
//        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"btnToMeTapping:") userInfo:nil repeats:NO];
//    }
}

- (void)viewDidAppearTimer:(id)sender {
    for (int i=0; i<[_items count]; ++i) {
        VehicleInfo *mv = (VehicleInfo *)[_items objectAtIndex:i];
        //开始实时地图
        [[CMServiceManager getInstance] getMonitorVehicle:mv.vehicleId];
        
        if (0 == i) {
            CLLocation *location = [[CLLocation alloc] initWithLatitude:mv.lastReport.location.latitude longitude:mv.lastReport.location.longitude];
            CLLocation *newLocation = [location locationMarsFromEarth];
            CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
            
            CLLocationCoordinate2D center = [baiduLocation coordinate];
            
            [_mapView setCenterCoordinate:center];
            
            break;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [[MCCGPSObject getInstance] pauseGPS];
    
    [[CMServiceManager getInstance] setMyDelegate:self];
    
    [_mapView viewWillAppear];
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
    _search.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
    
    if (0 == _center.longitude) {
        if (MAPMonitor == _mapType) {
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"viewDidAppearTimer:") userInfo:nil repeats:NO];
        }
        else {
            //        [_mapView setCenterCoordinate:_mapView.userLocation.coordinate];
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"btnToMeTapping:") userInfo:nil repeats:NO];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[MCCGPSObject getInstance] startGPS];
    
    [_mapView viewWillDisappear];
    _mapView.delegate = nil; // 不用时，置nil
    _search.delegate = nil; // 不用时，置nil
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_timesList release];
    
    [super dealloc];
    if (_search != nil) {
        [_search release];
        _search = nil;
    }
    if (_mapView) {
        [_mapView release];
        _mapView = nil;
    }
}

- (void)back:(id)sender {
    NSLog(@"back");
    
    [_selectTime2View clearDate];
    
    [showMonitorVehicleGPSTimer invalidate];showMonitorVehicleGPSTimer=nil;
    [setIsHisTimerOutTimer invalidate];setIsHisTimerOutTimer=nil;
    [beginRequestHistoryTimer invalidate];beginRequestHistoryTimer=nil;
    [timerOutTimer invalidate];timerOutTimer=nil;
    [setCenter2TimerOutTimer invalidate];setCenter2TimerOutTimer=nil;
    [setCenterTimerOutTimer invalidate];setCenterTimerOutTimer=nil;
    
    [showHistoryTimer invalidate];showHistoryTimer=nil;
    
    [_meNavTimer invalidate];_meNavTimer=nil;
    
    [_monitorEndTimeOut invalidate];_monitorEndTimeOut=nil;
    
    [_selectTime2View removeFromSuperview];_selectTime2View=nil;
    [mapsBottomView removeFromSuperview];mapsBottomView=nil;
    
    [[CMServiceManager getInstance] clearData];
    
    [_meNavTimer invalidate];_meNavTimer=nil;
    [setIsHisTimerOutTimer invalidate];setIsHisTimerOutTimer=nil;
    
    [colors release];colors=nil;
    
    [_warnView removeFromSuperview];
    
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
	array = [NSArray arrayWithArray:_mapView.overlays];
    [_mapView removeOverlays:array];
    
    [_selectTime2View clearDate];
    
    if (0. == _center.longitude) {
        [[CMMapHistoryManager getInstance] clearAll];
        [[CMMapMonitorVehicleManager getInstance] clearAll];
    }
    
    for (int i=0; i<[_items count]; ++i) {
        MonitorVehicle *mv = (MonitorVehicle *)[_items objectAtIndex:i];
        [[CMServiceManager getInstance] cancelMonitorVehicle:mv.vehicleId];
    }
    
    [_items release];_items=nil;
    
//    [_mapView removeFromSuperview];_mapView=nil;
    
    [super back:sender];
}

- (void)setIsHisTimerOut:(id)sender {
    NSLog(@"setIsHisTimerOut");
    
    NSArray *array = [NSArray arrayWithArray:_mapView.overlays];
	[_mapView removeOverlays:array];
    
    array = [NSArray arrayWithArray:_mapView.annotations];
	[_mapView removeAnnotations:array];
    
    BOOL isHistory = NO;
    if (MAPHistory == _mapType) {
        isHistory = YES;
    }
    [mapsBottomView setIsHis:isHistory];
    
    //去掉历史数据
    //    [[CMMapHistoryManager getInstance] clearAll];
    
    if (isHistory) {
        [self setTopToolTitle:@"历史轨迹"];
        
        [btnBaiduMap setHidden:YES];
        
        if (!_isFirstHistory) {
            [self showQueryHistoryMapsPath];
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:_historyEndTime];
        NSString *endStrDate = [dateFormatter stringFromDate:endDate];
        
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateNormal];
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateHighlighted];
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateSelected];
        
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:_historyStartTime];
        NSString *startStrDate = [dateFormatter stringFromDate:startDate];
        [mapsBottomView.btnTimeLeft setTitle:startStrDate forState:UIControlStateNormal];
        [mapsBottomView.btnTimeLeft setTitle:startStrDate forState:UIControlStateHighlighted];
        [mapsBottomView.btnTimeLeft setTitle:startStrDate forState:UIControlStateSelected];
        
        [dateFormatter release];
        
        [_selectTime2View setStartTime:_historyStartTime];
        [_selectTime2View setEndTime:_historyEndTime];
    }
    else {
        [self setTopToolTitle:@"实时地图"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:_monitorEndTime];
        NSString *endStrDate = [dateFormatter stringFromDate:endDate];
        
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateNormal];
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateHighlighted];
        [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateSelected];
        
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:_monitorCurrentStartTime];
        NSString *startStrDate = [dateFormatter stringFromDate:startDate];
        [mapsBottomView.btnTimeLeft setTitle:startStrDate forState:UIControlStateNormal];
        [mapsBottomView.btnTimeLeft setTitle:startStrDate forState:UIControlStateHighlighted];
        [mapsBottomView.btnTimeLeft setTitle:startStrDate forState:UIControlStateSelected];
        
        [dateFormatter release];
        
        [showMonitorVehicleGPSTimer invalidate];showMonitorVehicleGPSTimer=nil;
        showMonitorVehicleGPSTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"showMonitorVehicleGPS") userInfo:nil repeats:NO];
        
        [btnBaiduMap setHidden:NO];
        
        [_selectTime2View setStartTime:_monitorStartTime];
        [_selectTime2View setMonitorStartTime:_monitorStartTime];
        [_selectTime2View setEndTime:_monitorEndTime];
    }
    
    [_selectTime2View setIsHis:isHistory];
    
    setIsHisTimerOutTimer = nil;
}

- (void)setIsHis:(BOOL)yesOrNo {
    NSLog(@"setIsHis");
    if (yesOrNo) {
        _mapType = MAPHistory;
    }
    else {
        _mapType = MAPMonitor;
    }
    
    [setIsHisTimerOutTimer invalidate];setIsHisTimerOutTimer=nil;
    setIsHisTimerOutTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:NSSelectorFromString(@"setIsHisTimerOut:") userInfo:nil repeats:NO];
}

- (void)timerOut:(id)sender {
    NSLog(@"timerOut");
    timerOutTimer = nil;
    for (int i=0; i<[_items count]; ++i) {
        VehicleInfo *mv = (VehicleInfo *)[_items objectAtIndex:i];
        //开始实时地图
        [[CMServiceManager getInstance] getMonitorVehicle:mv.vehicleId];
        
        if (0 == i) {
            CLLocation *location = [[CLLocation alloc] initWithLatitude:mv.lastReport.location.latitude longitude:mv.lastReport.location.longitude];
            CLLocation *newLocation = [location locationMarsFromEarth];
            CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
            
            CLLocationCoordinate2D center = [baiduLocation coordinate];
            
            [_mapView setCenterCoordinate:center];
        }
    }
    
    [beginRequestHistoryTimer invalidate];beginRequestHistoryTimer=nil;
    beginRequestHistoryTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"beginRequestHistory") userInfo:nil repeats:NO];
}

//实时地图获取
- (void)setItemMonitorVehicles:(NSArray *)items_ {
    NSLog(@"setItemMonitorVehicles");
    [_items release];_items=nil;
    _items = [items_ retain];
    
    [[CMMapHistoryManager getInstance] clearAll];
    [[CMMapMonitorVehicleManager getInstance] clearAll];
    
    _monitorStartTime = [[NSDate date] timeIntervalSince1970];
    _monitorCurrentStartTime = _monitorStartTime;
    _monitorEndTime = _monitorStartTime;
    
    [[CMMapMonitorVehicleManager getInstance] setCarItem:_items];
    
    _isFirstHistory = YES;
    
    [timerOutTimer invalidate];timerOutTimer=nil;
    timerOutTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"timerOut:") userInfo:nil repeats:NO];
}

- (void)setCenterTimerOut:(id)sender {
    NSLog(@"setCenterTimerOut");
    setCenterTimerOutTimer = nil;
    
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
	array = [NSArray arrayWithArray:_mapView.overlays];
    [_mapView removeOverlays:array];
    
    [_mapView setCenterCoordinate:_center];
    
    //画线
    int index = [_mapCurrentLine count];
    BMKMapPoint* points1 = new BMKMapPoint[index];
    
    for (int i=0; i<index; i++) {
        CMGPSEntity *gpsEntity = [_mapCurrentLine objectAtIndex:i];
        float lat = gpsEntity.lat;
        float lon = gpsEntity.lon;
        
        if (0 == i) {
            [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(lat, lon)];
            
            RouteAnnotation* item = [[RouteAnnotation alloc]init];
            item.coordinate = CLLocationCoordinate2DMake(lat, lon);
            item.title = gpsEntity.licencePlateNumber;
            item.type = 0;
            [_mapView addAnnotation:item]; // 添加起点标注
            [item release];
            
            if ([_centerTitle length]>0) {
                [self setTopToolTitle:@"地图"];
            }
        }
        if (i == index-1) {
            RouteAnnotation* item = [[RouteAnnotation alloc]init];
            item.coordinate = CLLocationCoordinate2DMake(lat, lon);
            item.type = 1;
            item.title = gpsEntity.licencePlateNumber;
            [_mapView addAnnotation:item]; // 终点标注
            [item release];
        }
        
        CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lat, lon);
        BMKMapPoint point = BMKMapPointForCoordinate(coor);
        points1[i] = point;
    }
    // 通过points构建BMKPolyline
    BMKPolyline* polyLine1 = [BMKPolyline polylineWithPoints:points1 count:index];
    [_mapView addOverlay:polyLine1];
    
    delete [] points1;
    
    RouteAnnotation* item = [[RouteAnnotation alloc]init];
    item.coordinate = _center;
    item.title = [_centerTitle length]>0?_centerTitle:@"";
    item.type = 2;
    [_mapView addAnnotation:item]; // 添加起点标注
    [item release];
}

- (void)setCenter2TimerOut {
    NSLog(@"setCenter2TimerOut");
    setCenter2TimerOutTimer = nil;
    [commandView setHidden:YES];
    [_selectTime2View setHidden:YES];
    [mapsBottomView setHidden:YES];
}

- (void)setCenter:(CLLocationCoordinate2D )location_ title:(NSString *)title {
    NSLog(@"setCenter");
    CLLocation *location = [[CLLocation alloc] initWithLatitude:location_.latitude longitude:location_.longitude];
    CLLocation *newLocation = [location locationMarsFromEarth];
    CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
    
    _center = [baiduLocation coordinate];
    
    [_centerTitle release];_centerTitle=nil;
    _centerTitle = [title retain];
    
    [location release];
    
    [setCenter2TimerOutTimer invalidate];setCenter2TimerOutTimer=nil;
    setCenter2TimerOutTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setCenter2TimerOut") userInfo:nil repeats:NO];
    
    [setCenterTimerOutTimer invalidate];setCenterTimerOutTimer=nil;
    setCenterTimerOutTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"setCenterTimerOut:") userInfo:nil repeats:NO];
}

- (void)setCenter:(CLLocationCoordinate2D )location_ {
    CLLocation *location = [[CLLocation alloc] initWithLatitude:location_.latitude longitude:location_.longitude];
    CLLocation *newLocation = [location locationMarsFromEarth];
    CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
    
    _center = [baiduLocation coordinate];
    
    [location release];
    
    [setCenter2TimerOutTimer invalidate];setCenter2TimerOutTimer=nil;
    setCenter2TimerOutTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setCenter2TimerOut") userInfo:nil repeats:NO];
    
    [setCenterTimerOutTimer invalidate];setCenterTimerOutTimer=nil;
    setCenterTimerOutTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"setCenterTimerOut:") userInfo:nil repeats:NO];
}

- (void)setCenter:(CLLocationCoordinate2D )location_ title:(NSString *)title gpses:(NSArray *)gpses {
    NSLog(@"setCenter");
    CLLocation *location = [[CLLocation alloc] initWithLatitude:location_.latitude longitude:location_.longitude];
    CLLocation *newLocation = [location locationMarsFromEarth];
    CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
    
    _center = [baiduLocation coordinate];
    
    [_centerTitle release];_centerTitle=nil;
    _centerTitle = [title retain];
    
    [location release];
    
    [_mapCurrentLine release];_mapCurrentLine=nil;
    _mapCurrentLine = [gpses retain];
    
    [setCenter2TimerOutTimer invalidate];setCenter2TimerOutTimer=nil;
    setCenter2TimerOutTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setCenter2TimerOut") userInfo:nil repeats:NO];
    
    [setCenterTimerOutTimer invalidate];setCenterTimerOutTimer=nil;
    setCenterTimerOutTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"setCenterTimerOut:") userInfo:nil repeats:NO];
}

- (void)selectTime2ViewChange:(id)selectTime2View startTime:(long long)startTime endTime:(long long)endTime {
    NSLog(@"selectTime2ViewChange");
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:startTime];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endTime];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *starStrDate = [dateFormatter stringFromDate:startDate];
    NSString *endStrDate = [dateFormatter stringFromDate:endDate];
    
    [mapsBottomView.btnTimeLeft setTitle:starStrDate forState:UIControlStateNormal];
    [mapsBottomView.btnTimeLeft setTitle:starStrDate forState:UIControlStateSelected];
    [mapsBottomView.btnTimeLeft setTitle:starStrDate forState:UIControlStateHighlighted];
    
    [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateNormal];
    [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateSelected];
    [mapsBottomView.btnTimeRight setTitle:starStrDate forState:UIControlStateHighlighted];
    
    [dateFormatter release];
    
    [mapsBottomView setStartTime:startTime];
    [mapsBottomView setEndTime:endTime];
    
    if (MAPMonitor == _mapType) {
        _monitorStartTime = startTime;
    }
}

- (void)selectTime2ViewEnd:(id)selectTime2View startTime:(long long)startTime endTime:(long long)endTime {
    NSLog(@"selectTime2ViewEnd");
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:startTime];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endTime];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *starStrDate = [dateFormatter stringFromDate:startDate];
    NSString *endStrDate = [dateFormatter stringFromDate:endDate];
    
    [mapsBottomView.btnTimeLeft setTitle:starStrDate forState:UIControlStateNormal];
    [mapsBottomView.btnTimeLeft setTitle:starStrDate forState:UIControlStateSelected];
    [mapsBottomView.btnTimeLeft setTitle:starStrDate forState:UIControlStateHighlighted];
    
    [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateNormal];
    [mapsBottomView.btnTimeRight setTitle:endStrDate forState:UIControlStateSelected];
    [mapsBottomView.btnTimeRight setTitle:starStrDate forState:UIControlStateHighlighted];
    
    [dateFormatter release];
    
    [mapsBottomView setStartTime:startTime];
    [mapsBottomView setEndTime:endTime];
    
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
	array = [NSArray arrayWithArray:_mapView.overlays];
    [_mapView removeOverlays:array];
    
    if (MAPHistory == _mapType) {
        //历史轨迹中
        [[CMMapHistoryManager getInstance] clearAll];
        _historyStartTime = startTime;
        _historyEndTime = endTime;
        
        _lineIndex = 0;
        [self beginRequestHistory];
    }
    else {
        //实时地图
        _monitorStartTime = startTime;
        [self showMonitorVehicleGPS];
    }
    
    _lineIndex = 0;
}

- (void)selectTime2View:(id)selectTime2View leftValue:(int)leftValue rightValue:(int)rightValue {
    NSLog(@"selectTime2View");
    [mapsBottomView.btnTimeLeft setTitle:[_timesList objectAtIndex:leftValue] forState:UIControlStateNormal];
    [mapsBottomView.btnTimeLeft setTitle:[_timesList objectAtIndex:leftValue] forState:UIControlStateSelected];
    [mapsBottomView.btnTimeLeft setTitle:[_timesList objectAtIndex:leftValue] forState:UIControlStateHighlighted];
    
    [mapsBottomView.btnTimeRight setTitle:[_timesList objectAtIndex:rightValue] forState:UIControlStateNormal];
    [mapsBottomView.btnTimeRight setTitle:[_timesList objectAtIndex:rightValue] forState:UIControlStateSelected];
     [mapsBottomView.btnTimeRight setTitle:[_timesList objectAtIndex:rightValue] forState:UIControlStateHighlighted];
}

- (void)warnViewCenterTimerOut:(id)sender {
    NSLog(@"warnViewCenterTimerOut");
    NSTimer *timer = (NSTimer *)sender;
    NSString *value = [timer userInfo];
    NSArray *array = [value componentsSeparatedByString:@","];
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake([[array objectAtIndex:0] doubleValue], [[array objectAtIndex:1] doubleValue]);
    
    [_mapView setCenterCoordinate:center animated:YES];
}

- (void)warnView:(id)warnView center:(CLLocationCoordinate2D)center vehicleReport:(VehicleReport *)vehicleReport {
    NSLog(@"warnView");
    if ([vehicleReport isKindOfClass:[VehicleReport class]]) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:center.latitude longitude:center.longitude];
        CLLocation *newLocation = [location locationMarsFromEarth];
        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"warnViewCenterTimerOut:") userInfo:[NSString stringWithFormat:@"%lf,%lf",baiduLocation.coordinate.latitude,baiduLocation.coordinate.longitude] repeats:NO];
        
        [_mapView setZoomLevel:17];
    }
    else {
        [_mapView setZoomLevel:17];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"warnViewCenterTimerOut:") userInfo:[NSString stringWithFormat:@"%lf,%lf",center.latitude,center.longitude] repeats:NO];
    }
    
//    if (nil != vehicleReport) {
//        NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
//        for (RouteAnnotation *ann in array) {
//            if (ann.id == vehicleReport.alarm.alarmId) {
//                BMKAnnotationView *annView = [_mapView viewForAnnotation:ann];
//                [annView setSelected:YES];
//            }
//            else {
//                BMKAnnotationView *annView = [_mapView viewForAnnotation:ann];
//                [annView setSelected:NO];
//            }
//        }
//    }
//    else {
//        NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
//        for (RouteAnnotation *ann in array) {
//            BMKAnnotationView *annView = [_mapView viewForAnnotation:ann];
//            [annView setSelected:NO];
//        }
//    }
    
//    NSMutableArray *warns = [NSMutableArray array];
//    for (VehicleInfo *vi in _items) {
//        NSArray *ws = nil;
//        if (MAPHistory == _mapType) {
//            ws = [[CMMapHistoryManager getInstance] getWarnList:vi.vehicleId];
//        }
//        else {
//            ws = [[CMMapMonitorVehicleManager getInstance] getWarnList:vi.vehicleId];
//        }
//        
//        [warns addObjectsFromArray:ws];
//    }
//    
//    for (int i=0; i<[warns count]; ++i) {
//        VehicleReport *vr = (VehicleReport *)[warns objectAtIndex:i];
//        if ([vr isKindOfClass:[VehicleReport class]] && 0 == vr.alarm.alarmId) {
//            [warns removeObject:vr];
//        }
//    }
//    
//    for (VehicleReport *vr in warns) {
//        if ([vr isKindOfClass:[CMGPSEntity class]]) {
//            continue;
//        }
//        if (vehicleReport.alarm.alarmId == vr.alarm.alarmId) {
//            
//            return
//        }
//    }

}

#pragma mark CDTVMapsBottomView delegate
- (void)mapsBottomView:(id)mapsBottomView_ startTime:(long long)startTime endTime:(long long)endTime {
    NSLog(@"mapsBottomView");
    if (startTime>=endTime) {
        startTime = endTime;
    }
    if (endTime>=[[NSDate date] timeIntervalSince1970]) {
        endTime = [[NSDate date] timeIntervalSince1970];
    }
    
    if (MAPHistory == _mapType) {
        //历史轨迹中
        [[CMMapHistoryManager getInstance] clearAll];
        _historyStartTime = startTime;
        _historyEndTime = endTime;
        
        [_selectTime2View setStartTime:_historyStartTime];
        [_selectTime2View setEndTime:_historyEndTime];
        
        [mapsBottomView setStartTime:_historyStartTime];
        [mapsBottomView setEndTime:_historyEndTime];
        
        [self beginRequestHistory];
    }
    
    _lineIndex = 0;
}

- (BMKAnnotationView*)getRouteAnnotationView:(BMKMapView *)mapview viewForAnnotation:(RouteAnnotation*)routeAnnotation
{
    NSLog(@"getRouteAnnotationView");
	BMKAnnotationView* view = nil;
    
    switch (routeAnnotation.type) {
        case 0:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"start_node"];
            if (view == nil) {
                
                view = [[[BMKAnnotationView alloc] initWithAnnotation:routeAnnotation reuseIdentifier:@"start_node"] autorelease];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_start.png"]];
                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 1:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"end_node"];
            if (view == nil) {
                
                view = [[[BMKAnnotationView alloc] initWithAnnotation:routeAnnotation reuseIdentifier:@"end_node"] autorelease];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_end.png"]];
                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 2:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"end_node"];
            if (view == nil) {
                
                view = [[[BMKAnnotationView alloc] initWithAnnotation:routeAnnotation reuseIdentifier:@"node"] autorelease];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/pin_red.png"]];
                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        default:
            break;
    }
    
    if (0 != _center.longitude) {
        [view setSelected:YES];
    }
    
	return view;
}

- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view {
    NSLog(@"mapView");
    [_mapView setCenterCoordinate:view.annotation.coordinate animated:YES];
}

- (BMKAnnotationView *)mapView:(BMKMapView *)view viewForAnnotation:(id <BMKAnnotation>)annotation
{
    NSLog(@"mapView");
	if ([annotation isKindOfClass:[RouteAnnotation class]]) {
		return [self getRouteAnnotationView:view viewForAnnotation:(RouteAnnotation*)annotation];
	}
	return nil;
}

- (void)mapStatusDidChanged:(BMKMapView *)mapView {
    
}

- (BMKOverlayView*)mapView:(BMKMapView *)map viewForOverlay:(id<BMKOverlay>)overlay
{
    NSLog(@"mapView");
    [self showHistoryLoading:NO msg:@""];
	if ([overlay isKindOfClass:[BMKPolyline class]]) {
        BMKPolylineView* polylineView = [[[BMKPolylineView alloc] initWithOverlay:overlay] autorelease];
        [polylineView setTag:_lineIndex];
        //        if (!_isHistory) {
        polylineView.fillColor = [colors objectAtIndex:_lineIndex];
        polylineView.strokeColor = [colors objectAtIndex:_lineIndex];
        //        }
        polylineView.lineWidth = 4.;
        
        _lineIndex++;
        if (5 == _lineIndex) {
            _lineIndex=0;
        }
        
        return polylineView;
    }
	return nil;
}

- (void)onGetDrivingRouteResult:(BMKPlanResult*)result errorCode:(int)error
{
    NSLog(@"onGetDrivingRouteResult");
    if (result != nil) {
        // error 值的意义请参考BMKErrorCode
        if (error == BMKErrorOk) {
            //            [_mapView setCenterCoordinate:result.startNode.pt];
            
            NSMutableArray *lineGPSArray = [NSMutableArray array];
            
            BMKRoutePlan* plan = (BMKRoutePlan*)[result.plans objectAtIndex:0];
            
            // 下面开始计算路线，并添加驾车提示点
            int index = 0;
            //            int size = [plan.routes count];
            for (int i = 0; i < 1; i++) {
                BMKRoute* route = [plan.routes objectAtIndex:i];
                for (int j = 0; j < route.pointsCount; j++) {
                    int len = [route getPointsNum:j];
                    index += len;
                }
            }
            
            long long time = [[NSDate date] timeIntervalSince1970];
            
            BMKMapPoint* points = new BMKMapPoint[index];
            index = 0;
            for (int i = 0; i < 1; i++) {
                BMKRoute* route = [plan.routes objectAtIndex:i];
                for (int j = 0; j < route.pointsCount; j++) {
                    int len = [route getPointsNum:j];
                    BMKMapPoint* pointArray = (BMKMapPoint*)[route getPoints:j];
                    memcpy(points + index, pointArray, len * sizeof(BMKMapPoint));
                    index += len;
                }
            }
            
            for (int i=0; i<index; ++i) {
                BMKMapPoint mapPoint = points[i];
                CLLocationCoordinate2D point = BMKCoordinateForMapPoint(mapPoint);
                CMGPSEntity *gpsEntity = [CMGPSEntity new];
                [gpsEntity setLon:point.longitude];
                [gpsEntity setLat:point.latitude];
                [gpsEntity setTime:time];
                
                [lineGPSArray addObject:gpsEntity];
                [gpsEntity release];
            }
            
            delete []points;
            
            _vehicle_id = 0;
            _isLoadGPS = NO;
        }
    }
}

- (IBAction)btnToMeTapping:(id)sender {
    NSLog(@"btnToMeTapping");
    [mapsBottomView restoreView:YES];
    
    [_mapView setCenterCoordinate:_mapView.userLocation.location.coordinate animated:YES];
}

- (IBAction)btnWarnTapping:(id)sender {
    _lineIndex = 0;
    
    NSLog(@"btnWarnTapping");
    
    [mapsBottomView restoreView:YES];
    
    if (1 == [_items count]) {
        VehicleInfo *vi = (VehicleInfo *)[_items lastObject];
        NSMutableArray *warns = nil;
        
        if (MAPHistory == _mapType) {
            warns = [[CMMapHistoryManager getInstance] getWarnList:vi.vehicleId];
            
            [_warnView setMonitorItems:warns vehicle:vi];
            
            NSArray *gpses = [[CMMapHistoryManager getInstance] getGPS:_historyStartTime endTime:_historyEndTime vehicle_id:vi.vehicleId];
            [_warnView setCurrentPolylinesGps:gpses];
        }
        else {
            warns = [[CMMapMonitorVehicleManager getInstance] getWarnList:vi.vehicleId];
            [_warnView setMonitorItems:warns vehicle:vi];
            
            NSArray *gpses = [[CMMapMonitorVehicleManager getInstance] getVehiclesByIDFromGPS:vi.vehicleId startTime:_monitorStartTime];
            [_warnView setCurrentPolylinesGps:gpses];
        }
        
        [_warnView setIsMinimize:NO];
        [UIUitl toFrameAnimation:_warnView toFrame:CGRectMake(0, _topToolbar.frame.size.height, _warnView.frame.size.width, _warnView.frame.size.height)];
    }
    else {
        UIActionSheet *aSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
        [aSheet setTag:ALARM_TAG];
        for (VehicleInfo *vi in _items) {
            VehicleInfo_Builder *vib = [vi toBuilder];
            [aSheet addButtonWithTitle:vib.licencePlateNumber];
        }
        [aSheet addButtonWithTitle:@"取消"];
        [aSheet showInView:self.view];
        [aSheet release];
    }
}

- (IBAction)btnHisTapping:(id)sender {
    NSLog(@"btnHisTapping");
    [mapsBottomView restoreView:YES];
    
    _lineIndex = 0;
    _isFirst = NO;
    
    [_meNavTimer invalidate];_meNavTimer=nil;
    
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
	array = [NSArray arrayWithArray:_mapView.overlays];
    [_mapView removeOverlays:array];
    
    BOOL isHistory = NO;
    if (MAPHistory == _mapType) {
        _mapType = MAPMonitor;
        
        [[CMMapMonitorVehicleManager getInstance] clearAll];
        
        [_mapView setZoomLevel:13];
        
        for (VehicleInfo *vi in _items) {
            NSArray *gpsArray = [[CMMapMonitorVehicleManager getInstance] getVehiclesByIDFromGPS:vi.vehicleId startTime:_monitorStartTime];
            
            for (int i=0; i<[gpsArray count]; ++i) {
                CMGPSEntity *gpsEntity = [gpsArray objectAtIndex:i];
                float lat = gpsEntity.lat;
                float lon = gpsEntity.lon;
                
                if (0 == i) {
                    [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(lat, lon)];
                    break;
                }
            }
        }
    }
    else {
        _mapType = MAPHistory;
        isHistory = YES;
        
        [_mapView setZoomLevel:17];
    }
    
    [self setIsHis:isHistory];
}

- (IBAction)btnNavTapping:(id)sender {
    NSLog(@"btnNavTapping");
    _lineIndex = 0;
    
    [_meNavTimer invalidate];_meNavTimer=nil;
    
    if (1 == [_items count]) {
        VehicleInfo *vi = (VehicleInfo *)[_items objectAtIndex:0];
        CMGPSEntity *gpsEntity = [[CMMapMonitorVehicleManager getInstance] getLastGPS:vi.vehicleId];
        
        NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
        [_mapView removeAnnotations:array];
        array = [NSArray arrayWithArray:_mapView.overlays];
        [_mapView removeOverlays:array];
        
        _mapType = MAPNAV;
        
        //我的位置
        CLLocationCoordinate2D myCood2D = [[_mapView userLocation] coordinate];
        
//        CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:myCood2D.latitude longitude:myCood2D.longitude];
//        CLLocation *newStartLocation = [startLocation locationMarsFromEarth];
//        CLLocation *baiduStartLocation = [newStartLocation locationBaiduFromMars];
        
        //目标地址
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(gpsEntity.lat, gpsEntity.lon);
//        
//        CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
//        CLLocation *newEndLocation = [endLocation locationMarsFromEarth];
//        CLLocation *baiduEndLocation = [newEndLocation locationBaiduFromMars];
        
        [[CMGetNavGPSObject getInstance] getNavToMe:location meLocation:myCood2D];
        [[CMGetNavGPSObject getInstance] setDelegate:self];
        
        RouteAnnotation* item = [[RouteAnnotation alloc]init];
        item.coordinate = location;
        item.title = @"终点";
        item.type = 1;
        [_mapView addAnnotation:item]; // 添加起点标注
        [item release];
        
        [self toMeNavTimerOut:nil];
        
        [_meNavTimer invalidate];_meNavTimer=nil;
        _meNavTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:NSSelectorFromString(@"toMeNavTimerOut:") userInfo:nil repeats:YES];
        
        [self showHistoryLoading:YES msg:@""];
        
        [self setTopToolTitle:@"导航"];
        
        _selectCarIndex = 0;
    }
    else {
        UIActionSheet *aSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
        [aSheet setTag:MAPNAV_TAG];
        for (VehicleInfo *vi in _items) {
            VehicleInfo_Builder *vib = [vi toBuilder];
            [aSheet addButtonWithTitle:vib.licencePlateNumber];
        }
        [aSheet addButtonWithTitle:@"取消"];
        [aSheet showInView:self.view];
        [aSheet release];
    }
    
    /*
     BOOL yes = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]];
     
     //指定导航类型
     if (yes) {
     //初始化调启导航时的参数管理类
     NaviPara* para = [[NaviPara alloc]init];
     
     para.naviType = NAVI_TYPE_NATIVE;
     
     //初始化终点节点
     BMKPlanNode* end = [[[BMKPlanNode alloc]init] autorelease];
     //指定终点经纬度
     CLLocationCoordinate2D coor2;
     coor2.latitude = 39.915101;
     coor2.longitude = 116.403981;
     end.pt = coor2;
     //指定终点名称
     end.name = @"abc";
     //指定终点
     para.endPoint = end;
     
     //指定返回自定义scheme
     para.appScheme = @"baidumapcarmonitoring://com.brant.SmartTrip";
     
     //调启百度地图客户端导航
     [BMKNavigation openBaiduMapNavigation:para];
     [para release];
     }
     else {
     //初始化调启导航时的参数管理类
     NaviPara* para = [[NaviPara alloc]init];
     //指定导航类型
     para.naviType = NAVI_TYPE_WEB;
     
     //初始化起点节点
     BMKPlanNode* start = [[[BMKPlanNode alloc]init] autorelease];
     //指定起点经纬度
     CLLocationCoordinate2D coor1;
     coor1.latitude = 39.915101;
     coor1.longitude = 116.403981;
     start.pt = coor1;
     //指定起点名称
     start.name = @"起点";
     //指定起点
     para.startPoint = start;
     
     MCCGPSObject *gpsObjc = [MCCGPSObject getInstance];
     
     //初始化终点节点
     BMKPlanNode* end = [[[BMKPlanNode alloc]init] autorelease];
     CLLocationCoordinate2D coor2;
     coor2.latitude = [gpsObjc getLocationCoordinate2D].latitude;
     coor2.longitude = [gpsObjc getLocationCoordinate2D].latitude;
     end.pt = coor2;
     para.endPoint = end;
     //指定终点名称
     end.name = @"终点";
     //指定调启导航的app名称
     para.appName = [NSString stringWithFormat:@"%@", @"CarMonitoring"];
     //调启web导航
     [BMKNavigation openBaiduMapNavigation:para];
     [para release];
     }
     */
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    NSLog(@"actionSheet");
    if (ALARM_TAG == actionSheet.tag) {
        if (buttonIndex<[_items count]) {
            VehicleInfo *vi = (VehicleInfo *)[_items objectAtIndex:buttonIndex];
            NSArray *warns = nil;
            
            if (MAPHistory == _mapType) {
                warns = [[CMMapHistoryManager getInstance] getWarnList:vi.vehicleId];
            }
            else {
                warns = [[CMMapMonitorVehicleManager getInstance] getWarnList:vi.vehicleId];
            }
            
            [UIUitl toFrameAnimation:_warnView toFrame:CGRectMake(0, _topToolbar.frame.size.height, _warnView.frame.size.width, _warnView.frame.size.height)];
            [_warnView setMonitorItems:warns vehicle:vi];
            
            [_warnView setIsMinimize:NO];
        }
    }
    else if (MAPNAV_TAG == actionSheet.tag) {
        if (buttonIndex<[_items count]) {
            _selectCarIndex = buttonIndex;
            
            VehicleInfo *vi = (VehicleInfo *)[_items objectAtIndex:_selectCarIndex];
            CMGPSEntity *gpsEntity = [[CMMapMonitorVehicleManager getInstance] getLastGPS:vi.vehicleId];
            
            NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
            [_mapView removeAnnotations:array];
            array = [NSArray arrayWithArray:_mapView.overlays];
            [_mapView removeOverlays:array];
            
            _mapType = MAPNAV;
            
            //我的位置
            CLLocationCoordinate2D myCood2D = [[_mapView userLocation] coordinate];
            
            //        CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:myCood2D.latitude longitude:myCood2D.longitude];
            //        CLLocation *newStartLocation = [startLocation locationMarsFromEarth];
            //        CLLocation *baiduStartLocation = [newStartLocation locationBaiduFromMars];
            
            //目标地址
            CLLocationCoordinate2D location = CLLocationCoordinate2DMake(gpsEntity.lat, gpsEntity.lon);
            //
            //        CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
            //        CLLocation *newEndLocation = [endLocation locationMarsFromEarth];
            //        CLLocation *baiduEndLocation = [newEndLocation locationBaiduFromMars];
            
            [[CMGetNavGPSObject getInstance] getNavToMe:location meLocation:myCood2D];
            [[CMGetNavGPSObject getInstance] setDelegate:self];
            
            RouteAnnotation* item = [[RouteAnnotation alloc]init];
            item.coordinate = location;
            item.title = @"终点";
            item.type = 1;
            [_mapView addAnnotation:item]; // 添加起点标注
            [item release];
            
            [self toMeNavTimerOut:nil];
            
            [_meNavTimer invalidate];_meNavTimer=nil;
            _meNavTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:NSSelectorFromString(@"toMeNavTimerOut:") userInfo:nil repeats:YES];
            
            [self showHistoryLoading:YES msg:@""];
            
            [self setTopToolTitle:@"导航"];
        }
    }
}

- (void)toMeNavTimerOut:(id)sender {
    NSLog(@"toMeNavTimerOut");
    VehicleInfo *vi = (VehicleInfo *)[_items objectAtIndex:_selectCarIndex];
    CMGPSEntity *gpsEntity = [[CMMapMonitorVehicleManager getInstance] getLastGPS:vi.vehicleId];
    
    //目标地址
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(gpsEntity.lat, gpsEntity.lon);
    CLLocationCoordinate2D myCood2D = [[_mapView userLocation] coordinate];
    
    [[CMGetNavGPSObject getInstance] getNavToMe:location meLocation:myCood2D];
    [[CMGetNavGPSObject getInstance] setDelegate:self];
    
    [[CMGetNavGPSObject getInstance] refreshData];
}

#pragma mark CMGetNavGPSObject
- (void)getNavGPSObject:(id)getNavGPSObject polyline:(BMKPolyline *)polyline lastLocation:(CLLocationCoordinate2D)lastLocation {
    NSLog(@"getNavGPSObject");
    _lineIndex = 0;
    
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
//    if (_mapType != MAPNAV) {
        [_mapView removeAnnotations:array];
//    }
	array = [NSArray arrayWithArray:_mapView.overlays];
	[_mapView removeOverlays:array];
    
    [_mapView addOverlay:polyline];
    
    RouteAnnotation* item = [[RouteAnnotation alloc]init];
    item.coordinate = lastLocation;
    item.title = @"终点";
    item.type = 1;
    [_mapView addAnnotation:item]; // 添加起点标注
    [item release];
    
    [self showHistoryLoading:NO msg:@""];
}

- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    NSLog(@"service");
    if ([typename_ isEqualToString:@"SimpleHistory"]) {
        SimpleHistory *sh = (SimpleHistory *)entity;
        
        _isFirstHistory = NO;
        
        [[CMMapHistoryManager getInstance] addItem:sh];
        [self showQueryHistoryMapsPath];
        
        
//        if (0 == [sh.locationBuckets count] && MAPNAV == _mapType) {
//            [self alertMsg:@"无轨迹数据"];
//        }
        
        [self showHistoryLoading:NO msg:@""];
    }
    else if ([typename_ isEqualToString:@"VehicleReport"]) {
        VehicleReport *vr = (VehicleReport *)entity;
        
        if (nil != entity) {
            [[CMMapMonitorVehicleManager getInstance] addItem:vr];
        }
        
        //显示已经加载出来的gps位置
        [self showMonitorVehicleGPS];
        
        _isFirst = YES;
        NSLog(@"下载了实时地图数据： %@",[vr debugDescription]);
    }
}

- (void)serviceFaild:(id)service tag:(int)tag entity:(id)entity userInfo:(NSDictionary *)userInfo {
    
}

@end
