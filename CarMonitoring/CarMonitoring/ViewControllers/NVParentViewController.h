//
//  CDParentViewController.h
//  CDCoupon
//
//  Created by Symbio on 3/24/12.
//  Copyright 2012 symbio. All rights reserved.
//
#define TOP_HIG 44

#import <UIKit/UIKit.h>
#import "Service.h"
#import "CDTVTopBarView.h"
#import "CDTVBottomBarView.h"

@interface NVParentViewController : UIViewController <UITextFieldDelegate,UIPopoverControllerDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate> {
	CDTVTopBarView *_topToolbar;
    CDTVBottomBarView *_bottomToolbar;
	
	Service *_service;
	
	id _delegate;
    
    CGSize _keySize;
}
@property (nonatomic,retain) Service *service;
@property (nonatomic,retain) id delegate;
@property (nonatomic,assign) CDTVTopBarView *topToolbar;
@property (nonatomic,assign) CDTVBottomBarView *bottomToolbar;

- (void)setTopToolbarWithHidden:(BOOL)yesOrNo;
- (void)setTopToolTitle:(NSString *)title;
- (void)setTopToolRightTitle:(NSString *)title;
- (void)setTopToolBackBarWithHidden:(BOOL)yesOrNo;
- (void)setTopToolToHomeBarWithHidden:(BOOL)yesOrNo ;

- (void)setBottomToolbarWithHidden:(BOOL)yesOrNo;
- (void)setBottomToolBackBarWithHidden:(BOOL)yesOrNo;
- (void)setBottomToolToHomeBarWithHidden:(BOOL)yesOrNo;

- (void)alertMsg:(NSString *)msg;
- (void)back:(id)sender;

- (void)clearData;

- (void)showLoading:(BOOL)yesOrNo msg:(NSString *)msg;

//初始化Loading界面
- (void)showInitLoading:(BOOL)yesOrNo;

//右键事件
- (void)rightTapping:(id)sender;

//任务栏点击事件
- (void)topStatusClick:(id)sender;

@end
