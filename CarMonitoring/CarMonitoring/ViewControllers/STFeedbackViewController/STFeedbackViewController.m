//
//  STFeedbackViewController.m
//  SmartTrip
//
//  Created by Work on 11/15/13.
//  Copyright (c) 2013 PP. All rights reserved.
//

#import "STFeedbackViewController.h"
#import "AppManager.h"
#import "AppDelegate.h"

@interface STFeedbackViewController () {
    UIControl *overlay;
}

@end

@implementation STFeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTopToolTitle:@"意见反馈"];
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolToHomeBarWithHidden:NO];

    [self.view setBackgroundColor:UIColorFromRGB(0xe6e6e6)];
    
    [textField becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 监控键盘
    // 键盘高度变化通知，ios5.0新增的
#ifdef __IPHONE_5_0
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    }
#endif
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // 监控键盘
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    // 键盘高度变化通知，ios5.0新增的
#ifdef __IPHONE_5_0
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    }
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btnBackTapping:(id)sender {
    [self back:nil];
}

- (void)btnSendTapping:(id)sender {
    
}

- (void)back:(id)sender {
    [super back:sender];
}

#pragma mark Keyword
-(void)keyboardWillShow:(id)notification
{
    overlay = [[[UIControl alloc] initWithFrame:textField.frame] autorelease];
//    [overlay setBackgroundColor:[UIColor redColor]];
    overlay.tag = 98;
    [self.view addSubview:overlay];
    //    [zjTextView sendSubviewToBack:overlay];
    [overlay addTarget:self action:@selector(overlayTouch:) forControlEvents:UIControlEventTouchUpInside];
    //    NSLog(@"scroll view contentOffect %@",[NSValue valueWithCGPoint:[_scrollView contentOffset]]);
}

-(void)keyboardDidShow:(id)notification
{
    //    if ([_scrollView firstResponder] == _nameField ||
    //        [_scrollView firstResponder] == _mobileField ||
    //        [_scrollView firstResponder] == _emailField ||
    //        [_scrollView firstResponder] == _emailDomainField) {
    //        [_scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    //    }
    //    NSLog(@"scroll view contentOffect %@",[NSValue valueWithCGPoint:[_scrollView contentOffset]]);
}

-(void)overlayTouch:(id)sender
{
    [overlay removeFromSuperview];
    
    [self btnBGTapping:nil];
}

- (IBAction)btnBGTapping:(id)sender {
    [textField resignFirstResponder];
    [overlay removeFromSuperview];overlay=nil;
}

@end
