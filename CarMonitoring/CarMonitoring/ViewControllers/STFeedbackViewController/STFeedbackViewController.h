//
//  STFeedbackViewController.h
//  SmartTrip
//
//  Created by Work on 11/15/13.
//  Copyright (c) 2013 PP. All rights reserved.
//

#import "NVParentViewController.h"

@interface STFeedbackViewController : NVParentViewController {
    IBOutlet UITextView *textField;
}

@end
