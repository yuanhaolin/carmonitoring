//
//  CMCarModelViewController.h
//  CarMonitoring
//
//  Created by Work on 3/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"

@interface CMCarModelViewController : NVParentViewController <UITableViewDataSource,UITableViewDelegate>

- (void)setDicItems:(NSDictionary *)items_;

@end
