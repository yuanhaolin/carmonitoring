//
//  CMCarModelViewController.m
//  CarMonitoring
//
//  Created by Work on 3/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 44

#import "CMCarModelViewController.h"
#import "AppManager.h"
#import "AppDelegate.h"
#import "CDTVNavigationController.h"
#import "CDTVCarInfoModifyViewController.h"
#import "ChineseString.h"

@interface CMCarModelViewController () {
    UITableView *mainTableView;
    NSDictionary *_items;
    
    NSMutableArray *_results;
}

@end

@implementation CMCarModelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect appFrame = [AppManager getAppFrame];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [bgView setImage:[UIImage imageNamed:@"pic_bg.png"]];
    [self.view addSubview:bgView];
    [bgView release];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"车型选择"];
    
    mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [mainTableView setRowHeight:ROW_HIG];
	[mainTableView setDataSource:self];
	[mainTableView setDelegate:self];
    [mainTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:mainTableView];
	[mainTableView release];
    [mainTableView setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_results release];
    [super dealloc];
}

- (void)setDicItemsTimerOut:(id)sender {
    [mainTableView reloadData];
}

- (void)setDicItems:(NSDictionary *)items_ {
    [_items release];_items=nil;
    _items = [items_ retain];
    
    if ([_items isKindOfClass:[NSDictionary class]]) {
        NSMutableArray *result=[NSMutableArray arrayWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"abc"]];

        if (nil == [_items objectForKey:[result objectAtIndex:0]]) {
            NSMutableArray *stringsToSort = [NSMutableArray arrayWithArray:[_items allKeys]];
            
            //Step2:获取字符串中文字的拼音首字母并与字符串共同存放
            NSMutableArray *chineseStringsArray=[NSMutableArray array];
            for(int i=0;i<[stringsToSort count];i++){
                ChineseString *chineseString=[[ChineseString alloc]init];
                
                chineseString.string=[NSString stringWithString:[stringsToSort objectAtIndex:i]];
                
                if(chineseString.string==nil){
                    chineseString.string=@"";
                }
                
                if(![chineseString.string isEqualToString:@""]){
                    NSString *pinYinResult=[NSString string];
                    for(int j=0;j<chineseString.string.length;j++){
                        NSString *singlePinyinLetter=[[NSString stringWithFormat:@"%c",pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
                        
                        pinYinResult=[pinYinResult stringByAppendingString:singlePinyinLetter];
                        
                        if ([pinYinResult rangeOfString:@"#" options:NSCaseInsensitiveSearch].length>0) {
                            pinYinResult = chineseString.string;
                        }
                    }
                    chineseString.pinYin=pinYinResult;
                }else{
                    chineseString.pinYin=@"";
                }
                [chineseStringsArray addObject:chineseString];
                
                [chineseString release];
            }
            
            NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
            [chineseStringsArray sortUsingDescriptors:sortDescriptors];
            
            NSMutableArray *resultTmp=[NSMutableArray array];
            for(int i=0;i<[chineseStringsArray count];i++){
                NSString *key = ((ChineseString*)[chineseStringsArray objectAtIndex:i]).string;
                [resultTmp addObject:key];
            }
            
            if (nil == _results) {
                _results = [NSMutableArray new];
            }
            [_results addObjectsFromArray:resultTmp];
        }
        else {
            if (nil == _results) {
                _results = [NSMutableArray new];
            }
            [_results addObjectsFromArray:result];
        }
    }
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setDicItemsTimerOut:") userInfo:nil repeats:NO];
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 1;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int num = 0;
    
    if ([_items isKindOfClass:[NSArray class]]) {
        if ([(NSArray *)_items count]>1) {
            if ([[(NSArray *)_items objectAtIndex:1] isKindOfClass:[NSArray class]]) {
                num = [(NSArray *)_items count]-1;
            }
            else {
                num = [(NSArray *)_items count];
            }
        }
        else {
            num = [(NSArray *)_items count];
        }
    }
    
    return num;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        [cell.textLabel setTextColor:UIColorFromRGB(0xffffff)];
        [cell.textLabel setFont:WFONT(15)];
        
        UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [bgView setTag:21];
        [cell.contentView addSubview:bgView];
        [bgView release];
        
        //        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, ROW_HIG-0.5, 300, 0.5)];
        //        [lineView setBackgroundColor:UIColorFromRGB(0xd9d9d9)];
        //        [cell.contentView addSubview:lineView];
        //        [lineView release];
	}
    UIImageView *bgView = (UIImageView *)[cell.contentView viewWithTag:21];
    
    if (0 == indexPath.row%2) {
        [bgView setImage:[UIImage imageNamed:@"carlistbg1.png"]];
    }
    else {
        [bgView setImage:[UIImage imageNamed:@"carlistbg2.png"]];
    }
    
    NSString *name = @"";
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    NSArray *arrTmp = [(NSArray *)_items objectAtIndex:indexPath.row];
    if ([arrTmp isKindOfClass:[NSArray class]]) {
        if ([[arrTmp objectAtIndex:0] isKindOfClass:[NSString class]]) {
            name =[(NSArray *)arrTmp objectAtIndex:0];
        }
    }
    else {
        name = [(NSArray *)_items objectAtIndex:indexPath.row];
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    [cell.textLabel setText:name];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([(NSArray *)_items count]>1 && [[(NSArray *)_items objectAtIndex:1] isKindOfClass:[NSArray class]]) {
        AppDelegate *dele = [AppManager getAppDelegate];
        
        CMCarModelViewController *carModelViewController = [[CMCarModelViewController alloc] initWithNibName:@"CMCarModelViewController" bundle:nil];
        [dele.nav pushViewController:carModelViewController animated:YES];
        [carModelViewController release];
        
        NSArray *dics = [(NSArray *)_items objectAtIndex:indexPath.row];
        
        [AppManager addCarModeString:[dics objectAtIndex:0]];
        
        NSMutableArray *arrTmp = [NSMutableArray arrayWithArray:dics];
        [arrTmp removeObjectAtIndex:0];
        
        [carModelViewController setDicItems:arrTmp];
    }
    else {
        NSString *name = [(NSArray *)_items objectAtIndex:indexPath.row];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"carEnter" object:name];
        
        
        AppDelegate *dele = [AppManager getAppDelegate];
        NSArray *viewControllers = [dele.nav viewControllers];
        
        int index = 0;
        for (int i=[viewControllers count]-1; i>=0; i--) {
            NVParentViewController *pViewController = [viewControllers objectAtIndex:i];
            if ([pViewController isKindOfClass:[CMCarModelViewController class]]) {
                index++;
                [pViewController clearData];
                [dele.nav popViewControllerAnimated:NO];
            }
            else {
                break;
            }
        }
    }
}

- (void)back:(id)sender {
    [super back:sender];
    
    [AppManager removeLastCarModeString];
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
}

@end
