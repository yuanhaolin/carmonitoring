//
//  CDTVCarInfoDescViewController.m
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 61
#define TABLEVIEW_TAG   1001
#define BTN_SECTION_BEGIN_TAG   30001
#define ACTIONSHEET_WARN_TAG    2001
#define ACTIONSHEET_CLWZ_TAG      2002
#define ACTIONSHEET_BY_TAG      2003

#import "CDTVCarInfoDescViewController.h"
#import "AppManager.h"
#import "CDTVCarInfoSectionView.h"
#import "CDTVCatInfoTextCell1View.h"
#import "CDTVCatInfoTextCell2View.h"
#import "CDTVCarInfoModifyViewController.h"
#import "AppDelegate.h"
#import "CDTVNavigationController.h"
#import "CDTVMapsViewController.h"
#import "CMHistoryDescViewController.h"
#import "CMUserInfoAlrmsManager.h"
#import "CLLocation+YCLocation.h"
#import "CMMyUserInfoManager.h"

@interface CDTVCarInfoDescViewController () {
    NSMutableArray *selectCellIndexArray;
    VehicleInfo *_item;
    
    int _alarmIndex;
    
    BMKSearch* _baiduSearch;
    NSString *_addressString;
}

@end

@interface CDTVCarInfoDescViewController (Private)
- (void)changeView;

@end

@implementation CDTVCarInfoDescViewController (Private)
- (void)changeView {
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

@end

@implementation CDTVCarInfoDescViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    selectCellIndexArray = [NSMutableArray new];
    [selectCellIndexArray addObject:[NSNumber numberWithInt:0]];
    [selectCellIndexArray addObject:[NSNumber numberWithInt:2]];

    CGRect appFrame = [AppManager getAppFrame];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [bgView setImage:[UIImage imageNamed:@"pic_bg.png"]];
    [self.view addSubview:bgView];
    [bgView release];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"车辆详细信息"];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [tableView setTag:TABLEVIEW_TAG];
    [tableView setRowHeight:ROW_HIG];
	[tableView setDataSource:self];
	[tableView setDelegate:self];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:tableView];
	[tableView release];
    [tableView setBackgroundColor:[UIColor clearColor]];
    
    _baiduSearch = [[BMKSearch alloc]init];
    [_baiduSearch setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_baiduSearch release];
    [selectCellIndexArray release];
    [_item release];
    [_addressString release];
    
    [super dealloc];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    PBArray *array = [[[CMMyUserInfoManager getInstance] getUserInfo] vehicles];
    for (VehicleInfo *vi in array) {
        if (vi.vehicleId == _item.vehicleId) {
            [_item release];_item=nil;
            _item = [vi retain];
        }
    }
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

- (void)setItemTimerOut:(id)sender {
    CLLocation *location = [[CLLocation alloc] initWithLatitude:_item.lastReport.location.latitude longitude:_item.lastReport.location.longitude];
    CLLocation *newLocation = [location locationMarsFromEarth];
    CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
    
    CLLocationCoordinate2D pt = [baiduLocation coordinate];
	BOOL flag = [_baiduSearch reverseGeocode:pt];
	if (flag) {
		NSLog(@"ReverseGeocode search success.");
        
	}
    else{
        NSLog(@"ReverseGeocode search failed!");
    }
}

- (void)setItem:(VehicleInfo *)item_ {
    [_item release];_item=nil;
    _item = [item_ retain];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setItemTimerOut:") userInfo:nil repeats:NO];
}

- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error
{
    [_addressString release];_addressString=nil;
    _addressString = [result.strAddr retain];
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

#pragma mark UIButton
- (void)btnTapping:(id)sender {
    UIButton *btn = (UIButton *)sender;
    int index = btn.tag-BTN_SECTION_BEGIN_TAG;
    
    NSNumber *number = [NSNumber numberWithInt:index];
    if ([selectCellIndexArray containsObject:number]) {
        [selectCellIndexArray removeObject:number];
    }
    else {
        [selectCellIndexArray addObject:number];
    }
    
    [self changeView];
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 4;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = 0;
    
    NSNumber *sectionNumber = [NSNumber numberWithInt:section];
    switch (section) {
        case 0:
        {
            if ([selectCellIndexArray containsObject:sectionNumber]) {
                count = 3;
            }
        }
            break;
        case 1:
        {
            if ([selectCellIndexArray containsObject:sectionNumber]) {
                count = 4;
            }
        }
            break;
        case 2:
        {
            if ([selectCellIndexArray containsObject:sectionNumber]) {
                NSMutableArray *array = [NSMutableArray array];
                
                PBArray *vrs = _item.processingAlarms;
                for (int i=0; i<[vrs count]; ++i) {
                    VehicleReport *vr = [vrs objectAtIndex:i];
                    [array addObject:vr.alarm];
                }
                
                count = [array count];
            }
        }
            break;
        case 3:
        {
            if ([selectCellIndexArray containsObject:sectionNumber]) {
                count = 2;
            }
        }
            break;
        default:
            break;
    }
    return count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CDTVCarInfoSectionView *carInfoSectionView = [[CDTVCarInfoSectionView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    [carInfoSectionView.btnTap setTag:BTN_SECTION_BEGIN_TAG+section];
    if (0 == section%2) {
        [carInfoSectionView.bgImageView setImage:[UIImage imageNamed:@"carlistbg1.png"]];
    }
    else {
        [carInfoSectionView.bgImageView setImage:[UIImage imageNamed:@"carlistbg2.png"]];
    }
    if (0 == section) {
        [carInfoSectionView.labTitle setText:@"基本信息"];
    }
    else if (1 == section) {
        [carInfoSectionView.labTitle setText:@"车辆状态"];
    }
    else if (2 == section) {
        [carInfoSectionView.labTitle setText:@"报警信息"];
    }
    else if (3 == section) {
        [carInfoSectionView.labTitle setText:@"通知信息"];
    }
    [carInfoSectionView.btnTap addTarget:self action:NSSelectorFromString(@"btnTapping:") forControlEvents:UIControlEventTouchUpInside];
    
    return [carInfoSectionView autorelease];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        CDTVCatInfoTextCell1View *catInfoTextCell1View = [[CDTVCatInfoTextCell1View alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [catInfoTextCell1View setTag:1];
        [cell.contentView addSubview:catInfoTextCell1View];
        [catInfoTextCell1View release];
        
        CDTVCatInfoTextCell2View *catInfoTextCell2View = [[CDTVCatInfoTextCell2View alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [catInfoTextCell2View setTag:2];
        [cell.contentView addSubview:catInfoTextCell2View];
        [catInfoTextCell2View release];
	}
    CDTVCatInfoTextCell1View *catInfoTextCell1View = (CDTVCatInfoTextCell1View *)[cell.contentView viewWithTag:1];
    CDTVCatInfoTextCell2View *catInfoTextCell2View = (CDTVCatInfoTextCell2View *)[cell.contentView viewWithTag:2];
    
    [catInfoTextCell1View.viewNumer setHidden:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            [catInfoTextCell1View setHidden:NO];
            [catInfoTextCell2View setHidden:YES];
            
            switch (indexPath.row) {
                case 0:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"车牌号"];
                    [catInfoTextCell1View.labTitle2 setText:_item.licencePlateNumber];
                }
                    break;
                case 1:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"车架号"];
                    [catInfoTextCell1View.labTitle2 setText:_item.vin];
                }
                    break;
                case 2:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"车型"];
                    [catInfoTextCell1View.labTitle2 setText:_item.model];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            [catInfoTextCell1View setHidden:NO];
            [catInfoTextCell2View setHidden:YES];
            
            VehicleReport *vr = _item.lastReport;
            VehicleReport_Builder *vrb = [vr toBuilder];
            
            [catInfoTextCell1View.viewRig setHidden:YES];
            
            switch (indexPath.row) {
                case 0:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"车辆行驶里程"];
                    [catInfoTextCell1View.labTitle2 setText:[NSString stringWithFormat:@"%d公里",vrb.addtionalInfo.mileage]];
                    
                    [catInfoTextCell1View.viewRig setHidden:NO];
                }
                    break;
                case 1:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"最后位置"];
                    [catInfoTextCell1View.labTitle2 setText:[_addressString length]>0?_addressString:@""];
                }
                    break;
                case 2:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"待缴纳罚款"];
                    [catInfoTextCell1View.labTitle2 setText:@""];
                    [catInfoTextCell1View.viewNumer setHidden:NO];
                    [catInfoTextCell1View.labNumber setText:@"0"];
                }
                    break;
                case 3:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"车辆保养"];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            NSMutableArray *arrayAlarm = [NSMutableArray array];
            NSMutableArray *arrayLocations = [NSMutableArray array];
            
            PBArray *vrs = _item.processingAlarms;
            for (int i=0; i<[vrs count]; ++i) {
                VehicleReport *vr = [vrs objectAtIndex:i];
                [arrayAlarm addObject:vr.alarm];
                [arrayLocations addObject:vr.location];
            }
            
            [catInfoTextCell1View setHidden:YES];
            [catInfoTextCell2View setHidden:NO];
            [catInfoTextCell2View.labTitle1 setTextColor:UIColorFromRGB(0x707070)];
            
            Alarm *alarm = [arrayAlarm objectAtIndex:indexPath.row];
            
            NSMutableString *tags = [NSMutableString string];
            for (NSString *s in alarm.tags) {
                [tags appendFormat:@"%@ ",s];
            }
            
            [catInfoTextCell2View.labTitle1 setText:tags];
            
            if ([arrayLocations count]>indexPath.row) {
                BasicLocation *location = [arrayLocations objectAtIndex:indexPath.row];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];
                
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:location.time];
                NSString *dateStr = [dateFormatter stringFromDate:date];
                
                [catInfoTextCell2View.labTitle2 setText:dateStr];
                
                [dateFormatter release];
            }
        }
            break;
        case 3:
        {
            [catInfoTextCell1View setHidden:YES];
            [catInfoTextCell2View setHidden:NO];
            
            switch (indexPath.row) {
                case 0:
                {
                    [catInfoTextCell2View.labTitle1 setText:@"车辆违章"];
                }
                    break;
                case 1:
                {
                    [catInfoTextCell2View.labTitle1 setText:@"车辆需要保养"];
                }
                    break;
            }
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            AppDelegate *dele = [AppManager getAppDelegate];
            CDTVCarInfoModifyViewController *carInfoModifyViewController = [[CDTVCarInfoModifyViewController alloc] initWithNibName:@"CDTVCarInfoModifyViewController" bundle:nil];
            [dele.nav pushViewController:carInfoModifyViewController animated:YES];
            [carInfoModifyViewController release];
            [carInfoModifyViewController setItem:_item];
        }
            break;
        case 1:
        {
            if (1 == indexPath.row) {
                AppDelegate *dele = [AppManager getAppDelegate];
                CDTVMapsViewController *mapsViewController = [[CDTVMapsViewController alloc] initWithNibName:@"CDTVMapsViewController" bundle:nil];
                [dele.nav pushViewController:mapsViewController animated:YES];
                [mapsViewController release];
                
                [mapsViewController setCenter:CLLocationCoordinate2DMake(_item.lastReport.location.latitude, _item.lastReport.location.longitude) title:_item.lastReport.alarm.message];
            }
            else if (2 == indexPath.row) {
                NSURL *url=[NSURL URLWithString:@"http://www.cdjg.gov.cn/index.aspx"];  [[UIApplication sharedApplication]openURL:url];
            }
            else if (3 == indexPath.row) {
                NSURL *url=[NSURL URLWithString:@"tel://4007-028-666"];
                [[UIApplication sharedApplication] openURL:url];
            }
        }
            break;
        case 2:
        {
            _alarmIndex = indexPath.row;
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"地图" otherButtonTitles:@"详情", nil];
            [actionSheet setTag:ACTIONSHEET_WARN_TAG];
            [actionSheet showInView:self.view];
            [actionSheet release];
        }
            break;
        case 3:
        {
            if (0 == indexPath.row) {
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"缴费" otherButtonTitles:@"详情", nil];
                [actionSheet setTag:ACTIONSHEET_CLWZ_TAG];
                [actionSheet showInView:self.view];
                [actionSheet release];
            }
            else {
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"联系4S店" otherButtonTitles:@"详情", nil];
                [actionSheet setTag:ACTIONSHEET_BY_TAG];
                [actionSheet showInView:self.view];
                [actionSheet release];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
}

#pragma mark UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (ACTIONSHEET_WARN_TAG == actionSheet.tag) {
        switch (buttonIndex) {
            case 0:
            {
                AppDelegate *dele = [AppManager getAppDelegate];
                CDTVMapsViewController *mapsViewController = [[CDTVMapsViewController alloc] initWithNibName:@"CDTVMapsViewController" bundle:nil];
                [dele.nav pushViewController:mapsViewController animated:YES];
                [mapsViewController release];
                
                NSMutableArray *arrayLocations = [NSMutableArray array];
                
                PBArray *vrs = _item.processingAlarms;
                for (int i=0; i<[vrs count]; ++i) {
                    VehicleReport *vr = [vrs objectAtIndex:i];
                    [arrayLocations addObject:vr.location];
                }
                
                BasicLocation *bl = [arrayLocations objectAtIndex:_alarmIndex];
                [mapsViewController setCenter:CLLocationCoordinate2DMake(bl.latitude, bl.longitude)];
            }
                break;
            case 1:
            {
                AppDelegate *dele = [AppManager getAppDelegate];
                CMHistoryDescViewController *historeDescViewController = [[CMHistoryDescViewController alloc] init];
                [dele.nav pushViewController:historeDescViewController animated:YES];
                [historeDescViewController release];
                
                NSMutableArray *arrayAlarm = [NSMutableArray array];
                NSMutableArray *arrayLocations = [NSMutableArray array];
                
                PBArray *vrs = _item.processingAlarms;
                for (int i=0; i<[vrs count]; ++i) {
                    VehicleReport *vr = [vrs objectAtIndex:i];
                    [arrayAlarm addObject:vr.alarm];
                    [arrayLocations addObject:vr.location];
                }
                
                Alarm *alarm = [arrayAlarm objectAtIndex:_alarmIndex];
                
                [historeDescViewController setItemAlarm:alarm vehicleInfo:_item vehicleReport:[vrs objectAtIndex:_alarmIndex]];
            }
                break;
            case 2:
            {
                
            }
                break;
            default:
                break;
        }
    }
    else if (ACTIONSHEET_BY_TAG == actionSheet.tag) {
        switch (buttonIndex) {
            case 0:
            {
                NSURL *url=[NSURL URLWithString:@"tel://4007-028-666"];
                [[UIApplication sharedApplication] openURL:url];
            }
                break;
            case 1:
            {
                AppDelegate *dele = [AppManager getAppDelegate];
                CMHistoryDescViewController *historeDescViewController = [[CMHistoryDescViewController alloc] init];
                [dele.nav pushViewController:historeDescViewController animated:YES];
                [historeDescViewController release];
            }
                break;
            case 2:
            {
                
            }
                break;
            default:
                break;
        }
    }
    else if (ACTIONSHEET_CLWZ_TAG == actionSheet.tag) {
        switch (buttonIndex) {
            case 0:
            {
                NSURL *url=[NSURL URLWithString:@"http://www.cdjg.gov.cn/index.aspx"];  [[UIApplication sharedApplication]openURL:url];
            }
                break;
            case 1:
            {
                AppDelegate *dele = [AppManager getAppDelegate];
                CMHistoryDescViewController *historeDescViewController = [[CMHistoryDescViewController alloc] init];
                [dele.nav pushViewController:historeDescViewController animated:YES];
                [historeDescViewController release];
            }
                break;
            case 2:
            {
                
            }
                break;
            default:
                break;
        }
    }
}

@end
