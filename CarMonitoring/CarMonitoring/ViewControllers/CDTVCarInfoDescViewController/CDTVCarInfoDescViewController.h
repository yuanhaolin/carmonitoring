//
//  CDTVCarInfoDescViewController.h
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"
#import "BMapKit.h"

@class VehicleInfo;
@interface CDTVCarInfoDescViewController : NVParentViewController <UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,BMKSearchDelegate>

- (void)setItem:(VehicleInfo *)item_;

@end
