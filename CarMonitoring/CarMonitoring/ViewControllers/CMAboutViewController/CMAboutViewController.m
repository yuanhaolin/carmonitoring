//
//  CMAboutViewController.m
//  CarMonitoring
//
//  Created by Work on 3/7/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMAboutViewController.h"

@interface CMAboutViewController ()

@end

@implementation CMAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"关于"];
    [self setTopToolToHomeBarWithHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
