//
//  CMHelpViewController.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 60
#define TABLEVIEW_TAG   1001

#import "CMHelpViewController.h"
#import "AppManager.h"
#import "AppDelegate.h"
#import "CMMyInfoCellView.h"
#import "CMSettingViewController.h"
#import "CDTVNavigationController.h"
#import "STFeedbackViewController.h"
#import "CDTVWebViewController.h"
#import "CMAboutViewController.h"

@interface CMHelpViewController ()

@end

@implementation CMHelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect appFrame = [AppManager getAppFrame];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [bgView setImage:[UIImage imageNamed:@"pic_bg.png"]];
    [self.view addSubview:bgView];
    [bgView release];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"帮助"];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [tableView setTag:TABLEVIEW_TAG];
    [tableView setRowHeight:ROW_HIG];
	[tableView setDataSource:self];
	[tableView setDelegate:self];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:tableView];
	[tableView release];
    [tableView setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 1;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        [cell.textLabel setTextColor:UIColorFromRGB(0x383838)];
        [cell.textLabel setFont:WFONT(16)];
        
        CMMyInfoCellView *cellView = [[CMMyInfoCellView alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [cellView setTag:1];
        [cell.contentView addSubview:cellView];
        [cellView release];
	}
    CMMyInfoCellView *cellView = (CMMyInfoCellView *)[cell.contentView viewWithTag:1];
    if (0 == indexPath.row%2) {
        [cellView.bgImageView setImage:[UIImage imageNamed:@"carlistbg1.png"]];
    }
    else {
        [cellView.bgImageView setImage:[UIImage imageNamed:@"carlistbg2.png"]];
    }
    
    [cellView.imgIconView setHidden:NO];
    [cellView.labTitle2 setText:@""];
    
    switch (indexPath.row) {
        case 0:
        {
            [cellView.labTitle1 setText:@"设置"];
        }
            break;
        case 1:
        {
            [cellView.labTitle1 setText:@"常见疑问解答"];
        }
            break;
        case 2:
        {
            [cellView.labTitle1 setText:@"版本更新"];
        }
            break;
        case 3:
        {
            [cellView.labTitle1 setText:@"意见反馈"];
        }
            break;
        case 4:
        {
            [cellView.labTitle1 setText:@"客服电话"];
            [cellView.labTitle2 setText:@"4007-028-666"];
        }
            break;
        case 5:
        {
            [cellView.labTitle1 setText:@"关于我们"];
        }
            break;
            
        default:
            break;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
        {
            AppDelegate *dele = [AppManager getAppDelegate];
            
            CMSettingViewController *settingViewController = [[CMSettingViewController alloc] init];
            [dele.nav pushViewController:settingViewController animated:YES];
            [settingViewController release];
        }
            break;
        case 1:
        {
            AppDelegate *dele = [AppManager getAppDelegate];
            
            CDTVWebViewController *webViewController = [[CDTVWebViewController alloc] init];
            [dele.nav pushViewController:webViewController animated:YES];
            [webViewController release];
            [webViewController setUrl:@"http://www.gnssgis.com/app_qa.html" title:@"常见疑问解答"];
        }
            break;
        case 2:
        {
            [self alertMsg:@"您使用的是最新版本"];
        }
            break;
        case 3:
        {
            AppDelegate *dele = [AppManager getAppDelegate];
            
            STFeedbackViewController *feedbackViewController = [[STFeedbackViewController alloc] initWithNibName:@"STFeedbackViewController" bundle:nil];
            [dele.nav pushViewController:feedbackViewController animated:YES];
            [feedbackViewController release];
        }
            break;
        case 4:
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"立即拨打" otherButtonTitles: nil];
            [actionSheet showInView:self.view];
            [actionSheet release];
        }
            break;
        case 5:
        {
            AppDelegate *dele = [AppManager getAppDelegate];
            
            CMAboutViewController *aboutViewController = [[CMAboutViewController alloc] initWithNibName:@"CMAboutViewController" bundle:nil];
            [dele.nav pushViewController:aboutViewController animated:YES];
            [aboutViewController release];
        }
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
}

#pragma mark UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (0 == buttonIndex) {
        NSURL *url=[NSURL URLWithString:@"tel://4007-028-666"];
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
