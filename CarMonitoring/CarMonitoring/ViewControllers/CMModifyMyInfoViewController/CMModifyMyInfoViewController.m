//
//  CMModifyMyInfoViewController.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMModifyMyInfoViewController.h"
#import "CMMyUserInfoManager.h"
#import "Cm.pb.h"
#import "CMServiceManager.h"

@interface CMModifyMyInfoViewController ()

@end

@implementation CMModifyMyInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"修改资料"];

    [textLXR setFont:WFONT(15)];
    [textLXR setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [textName setFont:WFONT(15)];
    [textName setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [textPhone setFont:WFONT(15)];
    [textPhone setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    [textLXRPhone setFont:WFONT(15)];
    [textLXRPhone setTextColor:UIColorFromRGB(0x6b6b6b)];
    
    CMMyUserInfoManager *myUserInfoManager = [CMMyUserInfoManager getInstance];
    UserInfo_Builder *myUserInfo = [myUserInfoManager getUserInfo];
    UserInfo_Person *ownerInfoPerson = myUserInfo.owner;
    UserInfo_Person *contactInfoPerson = [myUserInfo.contact objectAtIndex:0];
    
    [textName setText:ownerInfoPerson.name];
    [textPhone setText:ownerInfoPerson.cellphone];
    
    [textLXR setText:contactInfoPerson.name];
    [textLXRPhone setText:contactInfoPerson.cellphone];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[CMServiceManager getInstance] setMyDelegate:nil];
}

- (IBAction)btnBGTapping:(id)sender {
    [textName resignFirstResponder];
    [textPhone resignFirstResponder];
    [textLXRPhone resignFirstResponder];
    [textLXR resignFirstResponder];
}

- (IBAction)btnTapping:(id)sender {
    [textName resignFirstResponder];
    [textPhone resignFirstResponder];
    [textLXRPhone resignFirstResponder];
    [textLXR resignFirstResponder];
    
    UserInfo_Builder *myUserInfo = [[CMMyUserInfoManager getInstance] getUserInfo];
    
    UserInfo_Person_Builder *ownerInfoPerson = [myUserInfo.owner toBuilder];
    [ownerInfoPerson setCellphone:textPhone.text];
    [ownerInfoPerson setName:textName.text];
    
    [myUserInfo setOwner:[ownerInfoPerson buildPartial]];
    
    UserInfo_Person_Builder *contactInfoPerson = [[myUserInfo.contact objectAtIndex:0] toBuilder];
    [contactInfoPerson setCellphone:textLXRPhone.text];
    [contactInfoPerson setName:textLXR.text];
    
    NSArray *contacts = [NSArray arrayWithObjects:[contactInfoPerson buildPartial], nil];
    [myUserInfo setContactArray:contacts];
    
    NSArray *array = [NSArray array];
    [myUserInfo setVehiclesArray:array];
    
    [[CMServiceManager getInstance] sendModifyMyUserInfo:myUserInfo];
    [[CMServiceManager getInstance] setMyDelegate:self];
    
    [self showLoading:YES msg:@"提交中..."];
}

- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    
    [self showLoading:NO msg:@"提交中..."];
    
    if ([typename_ isEqualToString:@"RequestResponse"]) {
        if (nil != entity) {
            [self alertMsg:@"修改成功!"];
            [self back:nil];
        }
        else {
            [self alertMsg:@"修改失败!"];
        }
    }
}

- (void)serviceFaild:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    
    [self alertMsg:@"修改失败!"];
    [self showLoading:NO msg:@"提交中..."];
}

@end
