//
//  CMModifyMyInfoViewController.h
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"

@interface CMModifyMyInfoViewController : NVParentViewController {
    IBOutlet UITextField *textName;
    IBOutlet UITextField *textPhone;
    IBOutlet UITextField *textLXR;
    IBOutlet UITextField *textLXRPhone;
}

@end
