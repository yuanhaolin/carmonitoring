//
//  CMHomeViewController.m
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMHomeViewController.h"
#import "CMCarInfoListViewController.h"
#import "AppDelegate.h"
#import "AppManager.h"
#import "CDTVNavigationController.h"
#import "CMMyInfoViewController.h"
#import "CMHistoryViewController.h"
#import "CMHelpViewController.h"
#import "CMMyUserInfoManager.h"
#import "Cm.pb.h"
#import "CMServiceManager.h"

@interface CMHomeViewController ()

@end

@implementation CMHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CMMyUserInfoManager *myUserInfoManager = [CMMyUserInfoManager getInstance];
    UserInfo_Builder *myUserInfo = [myUserInfoManager getUserInfo];
    UserInfo_Person *myUserInfoPerson = myUserInfo.owner;
    [labName setText:myUserInfoPerson.name];
    
    [labName setTextColor:UIColorFromRGB(0x2e72c7)];
    [labName setFont:WFONT(15)];
    
    [labLogining setFont:WFONT(9)];
    [labLogining setTextColor:UIColorFromRGB(0x7e7e7e)];
    
    CGSize sizeLabel = [labName.text sizeWithFont:labName.font constrainedToSize:CGSizeMake(10000, labName.frame.size.height) lineBreakMode:NSLineBreakByCharWrapping];
    [labName setFrame:CGRectMake(labName.frame.origin.x, labName.frame.origin.y, sizeLabel.width, labName.frame.size.height)];
    [labLogining setFrame:CGRectMake(labName.frame.origin.x+labName.frame.size.width+3, labLogining.frame.origin.y, labLogining.frame.size.width, labLogining.frame.size.height)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnLogoutTapping:(id)sender {
    [[CMServiceManager getInstance] logout];
}

- (IBAction)btnCarInfoTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    CMCarInfoListViewController *carInfoListViewController = [[CMCarInfoListViewController alloc] init];
    [dele.nav pushViewController:carInfoListViewController animated:YES];
    [carInfoListViewController release];
}

- (IBAction)btnUserInfoTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    CMMyInfoViewController *myInfoViewController = [[CMMyInfoViewController alloc] initWithNibName:@"CMMyInfoViewController" bundle:nil];
    [dele.nav pushViewController:myInfoViewController animated:YES];
    [myInfoViewController release];
}

- (IBAction)btnHistoryInfoTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    UserInfo_Builder *uib = [[CMMyUserInfoManager getInstance] getUserInfo];
    
    NSMutableArray *carList = [NSMutableArray array];
    for (VehicleInfo *vi  in uib.vehicles) {
        [carList addObject:vi];
    }
    
    CMHistoryViewController *historyViewController = [[CMHistoryViewController alloc] init];
    [dele.nav pushViewController:historyViewController animated:YES];
    [historyViewController release];
    [historyViewController setItems:carList];
}

- (IBAction)btnHelpInfoTapping:(id)sender {
    AppDelegate *dele = [AppManager getAppDelegate];
    
    CMHelpViewController *helpViewController = [[CMHelpViewController alloc] init];
    [dele.nav pushViewController:helpViewController animated:YES];
    [helpViewController release];
}

- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    if ([typename_ isEqualToString:@"RequestResponse"]) {
        if (nil != entity) {
            [[CMServiceManager getInstance] disconn];
            [self back:nil];
        }
        else {
            
        }
    }
}

- (void)serviceFaild:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    
    [self alertMsg:@"修改失败!"];
    [self showLoading:NO msg:@"提交中..."];
}

@end
