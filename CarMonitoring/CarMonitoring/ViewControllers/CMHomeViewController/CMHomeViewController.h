//
//  CMHomeViewController.h
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"

@interface CMHomeViewController : NVParentViewController {
    IBOutlet UILabel *labName;
    IBOutlet UILabel *labLogining;
}

@end
