//
//  CMHistoryDescViewController.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 61
#define TABLEVIEW_TAG   1001

#import "CMHistoryDescViewController.h"
#import "AppManager.h"
#import "CDTVCatInfoTextCell1View.h"
#import "CDTVCatInfoTextCell2View.h"
#import "Cm.pb.h"
#import "CMServiceManager.h"

@interface CMHistoryDescViewController () {
    Alarm *_alarm;
    VehicleInfo *_vInfo;
    VehicleReport *_vReport;
    
    BMKSearch *_baiduSearch;
    
    NSString *_address;
}

@end

@implementation CMHistoryDescViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    CGRect appFrame = [AppManager getAppFrame];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [bgView setImage:[UIImage imageNamed:@"pic_bg.png"]];
    [self.view addSubview:bgView];
    [bgView release];
    
    [self setTopToolbarWithHidden:NO];
    [self setTopToolBackBarWithHidden:NO];
    [self setTopToolTitle:@"详情"];
    [self setTopToolToHomeBarWithHidden:NO];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _topToolbar.frame.size.height, 320, appFrame.size.height-_topToolbar.frame.size.height)];
    [tableView setTag:TABLEVIEW_TAG];
    [tableView setRowHeight:ROW_HIG];
	[tableView setDataSource:self];
	[tableView setDelegate:self];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:tableView];
	[tableView release];
    [tableView setBackgroundColor:[UIColor clearColor]];
    
    _baiduSearch = [[BMKSearch alloc]init];
    [_baiduSearch setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_alarm release];
    [_vReport release];
    [_vInfo release];
    [_address release];
    [super dealloc];
}

- (void)back:(id)sender {
    [_baiduSearch setDelegate:nil];
    [_baiduSearch release];
    
    [super back:sender];
}

- (void)setItemAlarmTimeOut:(id)sender {
    CLLocationCoordinate2D pt = (CLLocationCoordinate2D){_vReport.location.latitude, _vReport.location.longitude};
	BOOL flag = [_baiduSearch reverseGeocode:pt];
	if (flag) {
		NSLog(@"ReverseGeocode search success.");
        
	}
    else{
        NSLog(@"ReverseGeocode search failed!");
    }
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
    
    [[CMServiceManager getInstance] getAlarm:_vInfo.vehicleId alarm_id:_alarm.alarmId];
}

- (void)setItemAlarm:(Alarm *)alarm vehicleInfo:(VehicleInfo *)vehicleInfo vehicleReport:(VehicleReport *)vehicleReport {
    [_alarm release];_alarm=nil;
    _alarm = [alarm retain];
    
    [_vInfo release];_vInfo=nil;
    _vInfo = [vehicleInfo retain];
    
    [_vReport release];_vReport=nil;
    _vReport = [vehicleReport retain];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"setItemAlarmTimeOut:") userInfo:nil repeats:NO];
}

- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error
{
    [_address release];_address=nil;
    _address = [result.strAddr retain];
    
    UITableView *tableView = (UITableView *)[self.view viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 1;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = 5;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        CDTVCatInfoTextCell1View *catInfoTextCell1View = [[CDTVCatInfoTextCell1View alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [catInfoTextCell1View setTag:1];
        [cell.contentView addSubview:catInfoTextCell1View];
        [catInfoTextCell1View release];
        
        CDTVCatInfoTextCell2View *catInfoTextCell2View = [[CDTVCatInfoTextCell2View alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [catInfoTextCell2View setTag:2];
        [cell.contentView addSubview:catInfoTextCell2View];
        [catInfoTextCell2View release];
	}
    CDTVCatInfoTextCell1View *catInfoTextCell1View = (CDTVCatInfoTextCell1View *)[cell.contentView viewWithTag:1];
    CDTVCatInfoTextCell2View *catInfoTextCell2View = (CDTVCatInfoTextCell2View *)[cell.contentView viewWithTag:2];
    
    [catInfoTextCell1View.viewNumer setHidden:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            [catInfoTextCell1View setHidden:NO];
            [catInfoTextCell1View.viewRig setHidden:NO];
            [catInfoTextCell2View setHidden:YES];
            
            [catInfoTextCell1View.labTitle1 setFrame:CGRectMake(catInfoTextCell1View.labTitle1.frame.origin.x, catInfoTextCell1View.labTitle1.frame.origin.y, 80, catInfoTextCell1View.labTitle1.frame.size.height)];
            [catInfoTextCell2View.labTitle1 setFrame:CGRectMake(catInfoTextCell1View.labTitle1.frame.origin.x+catInfoTextCell1View.labTitle1.frame.size.width, catInfoTextCell2View.labTitle1.frame.origin.y, self.view.frame.size.width-(catInfoTextCell1View.labTitle1.frame.origin.x+catInfoTextCell1View.labTitle1.frame.size.width)-40, catInfoTextCell2View.labTitle1.frame.size.height)];
            
            switch (indexPath.row) {
                case 0:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"违法车辆"];
                    
                    [catInfoTextCell1View.labTitle2 setText:_vInfo.licencePlateNumber];
                }
                    break;
                case 1:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"违法车型"];
                    
                    [catInfoTextCell1View.labTitle2 setText:_vInfo.model];
                }
                    break;
                case 2:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"违法时间"];
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];
                    NSString *strDate = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:_vReport.location.time]];
                    
                    [catInfoTextCell1View.labTitle2 setText:strDate];
                    
                    [dateFormatter release];
                }
                    break;
                case 3:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"违法地点"];
                    
                    [catInfoTextCell1View.labTitle2 setText:_address];
                }
                    break;
                case 4:
                {
                    [catInfoTextCell1View.labTitle1 setText:@"违法情况"];
                    
                    NSMutableString *tags = [NSMutableString string];
                    
                    for (NSString *tag in _alarm.tags) {
                        [tags appendString:tag];
                    }
                    
                    [catInfoTextCell1View.labTitle2 setText:tags];
                }
                    break;
            }
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
}

@end
