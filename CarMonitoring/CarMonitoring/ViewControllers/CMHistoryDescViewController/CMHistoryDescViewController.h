//
//  CMHistoryDescViewController.h
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"
#import "BMapKit.h"

@class VehicleInfo;
@class Alarm;
@class VehicleReport;
@interface CMHistoryDescViewController : NVParentViewController <UITableViewDelegate,UITableViewDataSource,BMKSearchDelegate>

- (void)setItemAlarm:(Alarm *)alarm vehicleInfo:(VehicleInfo *)vehicleInfo vehicleReport:(VehicleReport *)vehicleReport;

@end
