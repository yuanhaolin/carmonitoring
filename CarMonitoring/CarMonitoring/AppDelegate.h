//
//  AppDelegate.h
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"

@class CDTVNavigationController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,BMKGeneralDelegate,BMKMapViewDelegate,BMKSearchDelegate> {
    CDTVNavigationController *_nav;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) CDTVNavigationController *nav;

@end
