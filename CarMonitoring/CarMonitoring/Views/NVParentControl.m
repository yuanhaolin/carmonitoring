//
//  NVParentControl.m
//  NoTableVideo
//
//  Created by Work on 4/30/12.
//  Copyright (c) 2012 symbio. All rights reserved.
//

#import "NVParentControl.h"
#import "AppDelegate.h"
#import "AppManager.h"

@implementation NVParentControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _service = [Service new];
		[_service setDelegate:self];
        
        [self setBackgroundColor:UIColorFromRGB(0xe6e6e6)];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)removeFromSuperview {
	[_service.downQueue cancelAllOperations];
	[_service.downQueue reset];
    [_service release];_service=nil;
	
	[super removeFromSuperview];
}

- (void)dealloc
{
    [_service release];
    [super dealloc];
}

- (void)alertMsg:(NSString *)msg {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

//#pragma mark Service delegate
//-(void)service:(id)service tag:(int)tag entity:(id)entity {
//	
//}

@end
