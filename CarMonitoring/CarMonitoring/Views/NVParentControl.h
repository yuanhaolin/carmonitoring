//
//  NVParentControl.h
//  NoTableVideo
//
//  Created by Work on 4/30/12.
//  Copyright (c) 2012 symbio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@interface NVParentControl : UIView {
    Service *_service;
}
- (void)alertMsg:(NSString *)msg;

@end
