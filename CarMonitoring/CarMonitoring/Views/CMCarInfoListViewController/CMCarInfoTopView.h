//
//  CMCarInfoTopView.h
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMCarInfoTopView : UIView

@property (nonatomic,retain) IBOutlet UIButton *btnSSDT;
@property (nonatomic,retain) IBOutlet UIButton *btnLSGJ;
@property (nonatomic,retain) IBOutlet UIButton *btnLSJB;
@property (nonatomic,retain) IBOutlet UIButton *btnSXXX;

- (void)setIsDan:(BOOL)yesOrNo;

@end
