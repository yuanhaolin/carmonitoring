//
//  CMCarInfoTopView.m
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMCarInfoTopView.h"

@interface CMCarInfoTopView () {
    UIView *bgView;
}

@end

@implementation CMCarInfoTopView
@synthesize btnLSGJ;
@synthesize btnLSJB;
@synthesize btnSSDT;
@synthesize btnSXXX;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark attr
- (void)setIsDan:(BOOL)yesOrNo {
    [bgView removeFromSuperview];
    if (yesOrNo) {
        bgView = [[[NSBundle mainBundle] loadNibNamed:@"CMCarInfoTopViewDuo" owner:self options:nil] lastObject];
        [self addSubview:bgView];
    }
    else {
        bgView = [[[NSBundle mainBundle] loadNibNamed:@"CMCarInfoTopViewDan" owner:self options:nil] lastObject];
        [self addSubview:bgView];
    }
}

@end
