//
//  CMCarInfoListCellView.m
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMCarInfoListCellView.h"
#import "CMUserInfoAlrmsManager.h"
#import "CMHistoryViewController.h"
#import "AppManager.h"
#import "AppDelegate.h"
#import "CDTVNavigationController.h"

@interface CMCarInfoListCellView () {
    BMKSearch *_baiduSearch;
    VehicleInfo *_vInfo;
}

@end

@implementation CMCarInfoListCellView
@synthesize bgImageView;
@synthesize delegate=_delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CMCarInfoListCellView" owner:self options:nil] lastObject];
        [self addSubview:view];
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        [labTitle setFont:WFONT_BOLD(15)];
        [labTitle setTextColor:[UIColor whiteColor]];
        
        [labNumber setFont:WFONT(15)];
        [labNumber setTextColor:[UIColor whiteColor]];
        [labNumber setLineBreakMode:NSLineBreakByWordWrapping];
        
        [labZXState setFont:WFONT(10)];
        [labZXState setTextColor:UIColorFromRGB(0x8a4ebc)];
        
        [labAddress setFont:WFONT(12)];
        [labAddress setTextColor:UIColorFromRGB(0x707070)];
    }
    return self;
}

- (void)setSelected:(BOOL)yesOrNo {
    if (yesOrNo) {
        [selectedImageView setImage:[UIImage imageNamed:@"ico_tick_selected.png"] forState:UIControlStateNormal];
    }
    else {
        [selectedImageView setImage:[UIImage imageNamed:@"ico_tick.png"] forState:UIControlStateNormal];
    }
}

- (void)dealloc
{
    [super dealloc];
    [_vInfo release];
}

- (void)refreshAddress:(NSString *)address {
    [labAddress setText:address];
}

- (void)setItem:(VehicleInfo *)vInfo {
    [_vInfo release];_vInfo=nil;
    _vInfo = [vInfo retain];
    
    [labTitle setText:vInfo.licencePlateNumber];
//    [labNumber setText:vInfo.licencePlateNumber];
    if (vInfo.isOnline) {
        [labZXState setText:@"(在线)"];
        [labZXState setTextColor:UIColorFromRGB(0x8a4ebc)];
        [iconImageView setImage:[UIImage imageNamed:@"ico_car_blue.png"]];
    }
    else {
        [labZXState setText:@"(离线)"];
        [labZXState setTextColor:UIColorFromRGB(0x807979)];
        [iconImageView setImage:[UIImage imageNamed:@"ico_car_gery.png"]];
    }
    CGSize titleSize = [labTitle.text sizeWithFont:labTitle.font constrainedToSize:CGSizeMake(1000, labTitle.frame.size.height) lineBreakMode:NSLineBreakByClipping];
    [labTitle setFrame:CGRectMake(labTitle.frame.origin.x, labTitle.frame.origin.y, titleSize.width, labTitle.frame.size.height)];
    
    [labZXState setFrame:CGRectMake(labTitle.frame.size.width+4+labTitle.frame.origin.x, labZXState.frame.origin.y, labZXState.frame.size.width, labZXState.frame.size.height)];
    
    NSArray *alarms = [[CMUserInfoAlrmsManager getInstance] getWarnList:vInfo.vehicleId];
    int alarmCount = [alarms count];
    if (0 == alarmCount) {
        [viewNumberBG setHidden:YES];
    }
    else {
        [viewNumberBG setHidden:NO];
        [labNumber setText:[NSString stringWithFormat:@"%d",alarmCount]];
    }
    [labAddress setText:@""];
}

- (void)removeFromSuperview {
    [_baiduSearch setDelegate:nil];
    
    [super removeFromSuperview];
}

- (IBAction)btnCarTapping:(id)sender {
    NSArray *alarms = [[CMUserInfoAlrmsManager getInstance] getWarnList:_vInfo.vehicleId];
    if ([alarms count]>0) {
        AppDelegate *dele = [AppManager getAppDelegate];
        
        CMHistoryViewController *historeViewController = [[CMHistoryViewController alloc] init];
        [dele.nav pushViewController:historeViewController animated:YES];
        [historeViewController release];
        [historeViewController setItems:[NSArray arrayWithObjects:_vInfo, nil]];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
