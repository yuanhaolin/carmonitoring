//
//  CMCarInfoListCellView.h
//  CarMonitoring
//
//  Created by Work on 1/1/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cm.pb.h"
#import "BMapKit.h"

@protocol CMCarInfoListCellViewDelegate <NSObject>
- (void)loadEndCarInfoListCellView:(id)carInfoListCellView;

@end

@class VehicleInfo;
@interface CMCarInfoListCellView : UIView <BMKSearchDelegate> {
    IBOutlet UIImageView *iconImageView;
    IBOutlet UILabel *labTitle;
    IBOutlet UILabel *labZXState;
    IBOutlet UILabel *labAddress;
    IBOutlet UIButton *selectedImageView;
    IBOutlet UILabel *labNumber;
    IBOutlet UIView *viewNumberBG;
    
    id _delegate;
}
@property (nonatomic,assign) IBOutlet UIImageView *bgImageView;
@property (nonatomic,retain) id delegate;

- (void)setSelected:(BOOL)yesOrNo;
- (void)setItem:(VehicleInfo *)vInfo;

- (void)refreshAddress:(NSString *)address;

@end
