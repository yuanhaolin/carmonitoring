//
//  CDTVCarInfoSectionView.m
//  CarMonitoring
//
//  Created by Work on 1/3/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CDTVCarInfoSectionView.h"

@implementation CDTVCarInfoSectionView
@synthesize bgImageView;
@synthesize labTitle;
@synthesize btnTap;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CDTVCarInfoSectionView" owner:self options:nil] lastObject];
        [self addSubview:view];
        
        [self.labTitle setTextColor:[UIColor whiteColor]];
        [self.labTitle setFont:WFONT(15)];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
