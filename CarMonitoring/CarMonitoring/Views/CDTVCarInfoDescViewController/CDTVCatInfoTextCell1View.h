//
//  CDTVCatInfoTextCell1View.h
//  CarMonitoring
//
//  Created by Work on 1/3/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDTVCatInfoTextCell1View : UIView {
    
}
@property (nonatomic,assign) IBOutlet UILabel *labTitle1;
@property (nonatomic,assign) IBOutlet UILabel *labTitle2;
@property (nonatomic,assign) IBOutlet UILabel *labNumber;
@property (nonatomic,assign) IBOutlet UIView *viewNumer;
@property (nonatomic,assign) IBOutlet UIView *viewRig;

@end
