//
//  CDTVCatInfoTextCell2View.m
//  CarMonitoring
//
//  Created by Work on 1/3/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CDTVCatInfoTextCell2View.h"

@implementation CDTVCatInfoTextCell2View
@synthesize labTitle1;
@synthesize labTitle2;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CDTVCatInfoTextCell2View" owner:self options:nil] lastObject];
        [self addSubview:view];
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        [self.labTitle1 setFont:WFONT(15)];
        [self.labTitle1 setTextColor:UIColorFromRGB(0x707070)];
        
        [self.labTitle2 setFont:WFONT(12)];
        [self.labTitle2 setTextColor:UIColorFromRGB(0xb3b3b3)];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
