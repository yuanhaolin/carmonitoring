//
//  CDTVCatInfoTextCell1View.m
//  CarMonitoring
//
//  Created by Work on 1/3/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CDTVCatInfoTextCell1View.h"

@implementation CDTVCatInfoTextCell1View
@synthesize labTitle1;
@synthesize labTitle2;
@synthesize labNumber;
@synthesize viewNumer;
@synthesize viewRig;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CDTVCatInfoTextCell1View" owner:self options:nil] lastObject];
        [self addSubview:view];
        
        [self setBackgroundColor:[UIColor whiteColor]];
        
        [self.labTitle1 setFont:WFONT(15)];
        [self.labTitle1 setTextColor:UIColorFromRGB(0x707070)];
        
        [self.labTitle2 setFont:WFONT(15)];
        [self.labTitle2 setTextColor:UIColorFromRGB(0x3c3c3c)];
        
        [self.labNumber setFont:WFONT(15)];
        [self.labNumber setTextColor:[UIColor whiteColor]];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
