//
//  CDTVCarInfoSectionView.h
//  CarMonitoring
//
//  Created by Work on 1/3/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDTVCarInfoSectionView : UIView

@property (nonatomic,assign) IBOutlet UIImageView *bgImageView;
@property (nonatomic,assign) IBOutlet UILabel *labTitle;
@property (nonatomic,assign) IBOutlet UIButton *btnTap;

@end
