//
//  CDTVCatInfoTextCell2View.h
//  CarMonitoring
//
//  Created by Work on 1/3/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDTVCatInfoTextCell2View : UIView

@property (nonatomic,assign) IBOutlet UILabel *labTitle1;
@property (nonatomic,assign) IBOutlet UILabel *labTitle2;
@end
