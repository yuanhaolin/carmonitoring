//
//  CDTVTopBarView.h
//  CDTV2
//
//  Created by Symbio on 3/25/13.
//  Copyright (c) 2013 Symbio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BYImageView;
@interface CDTVTopBarView : UIView {
    
}
@property (nonatomic,assign) IBOutlet UIView *topLeftView;
@property (nonatomic,assign) IBOutlet UIView *topRightView;
@property (nonatomic,assign) IBOutlet UIButton *btnLeft;
@property (nonatomic,assign) IBOutlet UIButton *btnRight;
@property (nonatomic,assign) IBOutlet UIButton *btnLeftBG;
@property (nonatomic,assign) IBOutlet UIButton *btnRightBG;
@property (nonatomic,assign) IBOutlet BYImageView *imageBGView;

- (void)setLeftButton:(UIImage *)icon target:(id)target action:(SEL)action;
- (void)setRightButton:(UIImage *)icon target:(id)target action:(SEL)action;

- (void)setRightButtonTitle:(NSString *)title target:(id)target action:(SEL)action;

@end
