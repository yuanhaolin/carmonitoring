//
//  CDTVBottomBarView.m
//  SmartTrip
//
//  Created by Work on 11/13/13.
//  Copyright (c) 2013 PP. All rights reserved.
//

#import "CDTVBottomBarView.h"

@implementation CDTVBottomBarView
@synthesize topLeftView;
@synthesize topRightView;
@synthesize btnLeft;
@synthesize btnRight;
@synthesize btnLeftBG;
@synthesize btnRightBG;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setClipsToBounds:NO];
        [self setClearsContextBeforeDrawing:NO];
        
        [self setBackgroundColor:UIColorFromRGB(0x626262)];
        
        UIView *topView = [[[NSBundle mainBundle] loadNibNamed:@"CDTVBottomBarView" owner:self options:nil] objectAtIndex:0];
        [self addSubview:topView];
        [topView setClipsToBounds:NO];
        [topView setClearsContextBeforeDrawing:NO];
        
        [self.btnLeft setShowsTouchWhenHighlighted:YES];
        [self.btnLeftBG setShowsTouchWhenHighlighted:YES];
        
        [self.btnRight setShowsTouchWhenHighlighted:YES];
        [self.btnRightBG setShowsTouchWhenHighlighted:YES];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setLeftButton:(UIImage *)icon target:(id)target action:(SEL)action {
    [self.topLeftView setHidden:NO];
    [self.btnLeft setBackgroundImage:icon forState:UIControlStateNormal];
    
    NSArray *actions = [self.btnLeft actionsForTarget:target forControlEvent:UIControlEventTouchUpInside];
    for (NSString *strAction in actions) {
        [self.btnLeft removeTarget:target action:NSSelectorFromString(strAction) forControlEvents:UIControlEventTouchUpInside];
    }
    NSArray *actionsBG = [self.btnLeftBG actionsForTarget:target forControlEvent:UIControlEventTouchUpInside];
    for (NSString *strAction in actionsBG) {
        [self.btnLeftBG removeTarget:target action:NSSelectorFromString(strAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (nil == icon) {
        [self.topLeftView setHidden:YES];
    }
    
    [self.btnLeft addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.btnLeftBG addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

- (void)setRightButton:(UIImage *)icon target:(id)target action:(SEL)action {
    [self.topRightView setHidden:NO];
    [self.btnRight setBackgroundImage:icon forState:UIControlStateNormal];
    
    NSArray *actions = [self.btnRight actionsForTarget:target forControlEvent:UIControlEventTouchUpInside];
    for (NSString *strAction in actions) {
        [self.btnRight removeTarget:target action:NSSelectorFromString(strAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    NSArray *actionsBG = [self.btnRightBG actionsForTarget:target forControlEvent:UIControlEventTouchUpInside];
    for (NSString *strAction in actionsBG) {
        [self.btnRightBG removeTarget:target action:NSSelectorFromString(strAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    //    UILabel *labTitle = (UILabel *)[self viewWithTag:1];
    if (nil == icon) {
        [self.topRightView setHidden:YES];
        
        //        [labTitle setFrame:CGRectMake(73, 0, self.frame.size.width-73, 44)];
    }
    else {
        //        [labTitle setFrame:CGRectMake(73, 0, self.frame.size.width-73*2, 44)];
    }
    
    [self.btnRight addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.btnRightBG addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

- (void)setRightButtonTitle:(NSString *)title target:(id)target action:(SEL)action {
    [self.topRightView setHidden:NO];
    [self.btnRight setTitle:title forState:UIControlStateNormal];
    [self.btnRight.titleLabel setFont:WFONT(14)];
    [self.btnRight setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    NSArray *actions = [self.btnRight actionsForTarget:target forControlEvent:UIControlEventTouchUpInside];
    for (NSString *strAction in actions) {
        [self.btnRight removeTarget:target action:NSSelectorFromString(strAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    NSArray *actionsBG = [self.btnRightBG actionsForTarget:target forControlEvent:UIControlEventTouchUpInside];
    for (NSString *strAction in actionsBG) {
        [self.btnRightBG removeTarget:target action:NSSelectorFromString(strAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self.btnRight addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.btnRightBG addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}

@end
