//
//  CDTVBottomBarView.h
//  SmartTrip
//
//  Created by Work on 11/13/13.
//  Copyright (c) 2013 PP. All rights reserved.
//

#import "NVParentControl.h"

@interface CDTVBottomBarView : NVParentControl

@property (nonatomic,assign) IBOutlet UIView *topLeftView;
@property (nonatomic,assign) IBOutlet UIView *topRightView;
@property (nonatomic,assign) IBOutlet UIButton *btnLeft;
@property (nonatomic,assign) IBOutlet UIButton *btnRight;
@property (nonatomic,assign) IBOutlet UIButton *btnLeftBG;
@property (nonatomic,assign) IBOutlet UIButton *btnRightBG;

- (void)setLeftButton:(UIImage *)icon target:(id)target action:(SEL)action;
- (void)setRightButton:(UIImage *)icon target:(id)target action:(SEL)action;

- (void)setRightButtonTitle:(NSString *)title target:(id)target action:(SEL)action;
@end
