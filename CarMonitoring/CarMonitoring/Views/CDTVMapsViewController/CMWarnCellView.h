//
//  CMWarnCellView.h
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"

@class VehicleReport;
@interface CMWarnCellView : UIView <BMKSearchDelegate> {
    IBOutlet UILabel *labTime;
    IBOutlet UILabel *labTitle1;
    IBOutlet UILabel *labTitle2;
    IBOutlet UIImageView *docImageView;
    IBOutlet UIImageView *lineImageView;
}
@property (nonatomic,assign) BOOL isHis;

- (void)setIsFirst:(BOOL)yesOrNo;
- (void)setIsEnd:(BOOL)yesOrNo;
- (void)setIsMid:(BOOL)yesOrNo;

- (void)refreshAddress:(NSString *)address index:(int)index_;

- (float)setItem:(VehicleReport *)item_ index:(int)index ;

@end
