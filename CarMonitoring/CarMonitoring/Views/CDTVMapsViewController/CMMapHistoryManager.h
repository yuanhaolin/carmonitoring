//
//  CMMapHistoryManager.h
//  CarMonitoring
//
//  Created by Work on 1/17/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SimpleHistory;
@interface CMMapHistoryManager : NSObject

+ (CMMapHistoryManager *)getInstance;
- (void)addItem:(id)item_;

- (void)clearAll;

- (NSMutableArray *)getWarnList:(int32_t)vehicle_id;
- (NSArray *)getGPS:(long long)startTime endTime:(long long)endTime;
- (NSArray *)getGPS:(long long)startTime endTime:(long long)endTime vehicle_id:(int32_t)vehicle_id;

@end
