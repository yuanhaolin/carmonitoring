//
//  CMSelectTime2View.h
//  CarMonitoring
//
//  Created by Work on 1/10/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CMSelectTime2ViewDelegate
- (void)selectTime2ViewChange:(id)selectTime2View startTime:(long long)startTime endTime:(long long)endTime;
- (void)selectTime2ViewEnd:(id)selectTime2View startTime:(long long)startTime endTime:(long long)endTime;

@end

@class NMRangeSlider;
@interface CMSelectTime2View : UIView {
    id _delegate;
    
    IBOutlet NMRangeSlider *_slider;
}
@property (nonatomic,retain) id delegate;

- (void)clearDate;

- (void)setStartTime:(long long)timer_;
- (void)setEndTime:(long long)timer_;
- (void)setMonitorStartTime:(long long)timer_;

//是否是历史
- (void)setIsHis:(BOOL)yesOrNo;

@end
