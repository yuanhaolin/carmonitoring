//
//  CMSelectTime2View.m
//  CarMonitoring
//
//  Created by Work on 1/10/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMSelectTime2View.h"
#import "NMRangeSlider.h"

@interface CMSelectTime2View () {
    int _count;
    
    //进来时的大小
    long long _startTime;
    long long _endTime;
    
    //当前大小
    long long _startCurrentTime;
    long long _endCurrentTime;
    long long _monitorStartTime;
    
    long long _currentTime;
    
    int _timerIndex;
    NSTimer *_timer;
    
    NSTimer *_timerValue;
    
    BOOL _isHis;
    
    UIView *bgUpperView;
}

@end

@implementation CMSelectTime2View
@synthesize delegate=_delegate;

- (void)initTimerOut:(id)sender {
    [_slider setMinimumValue:0];
    [_slider setMaximumValue:2];
    
//    [_slider setLowerValue:0.05 upperValue:90. animated:NO];
    _slider.lowerValue = 0.1;
    _slider.upperValue = 1.9;
//    [_slider setLowerValue:15 animated:YES];
//    [_slider setUpperValue:95 animated:YES];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
        [bgView setBackgroundColor:[UIColor blackColor]];
        [bgView setAlpha:0.8];
        [self addSubview:bgView];
        [bgView release];
        
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CMSelectTime2View" owner:self options:nil] lastObject];
        [self addSubview:view];
        [view setBackgroundColor:[UIColor clearColor]];
        
        [_slider setBackgroundColor:[UIColor clearColor]];
//        _slider = [[NMRangeSlider alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [_slider setLowerHandleImageNormal:[UIImage imageNamed:@"ico_slide_green.png"]];
        [_slider setLowerHandleImageHighlighted:[UIImage imageNamed:@"ico_slide_green.png"]];
        [_slider setUpperHandleImageNormal:[UIImage imageNamed:@"ico_slide_blue.png"]];
        [_slider setUpperHandleImageHighlighted:[UIImage imageNamed:@"ico_slide_blue.png"]];
        [_slider setTrackBackgroundImage:[UIImage imageNamed:@"pic_slidebar.png"]];
        [_slider setTrackImage:[UIImage imageNamed:@"pic_slidebar.png"]];
        [_slider addTarget:self action:NSSelectorFromString(@"sliderBeginValue:") forControlEvents:UIControlEventTouchDown];
        [_slider addTarget:self action:NSSelectorFromString(@"sliderChangeValue:") forControlEvents:UIControlEventValueChanged];
        [_slider addTarget:self action:NSSelectorFromString(@"sliderEndValue:") forControlEvents:UIControlEventTouchUpInside];
        
        bgUpperView = [[UIView alloc] initWithFrame:CGRectMake(260, 0, 60, frame.size.height)];
        [bgUpperView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:bgUpperView];
        [bgUpperView release];
//        [self addSubview:_slider];
//        [_slider release];
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"initTimerOut:") userInfo:nil repeats:NO];
        
        NSDate *currentDate = [NSDate date];
        _currentTime = [currentDate timeIntervalSince1970];
        
        _timerValue = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"timerValue:") userInfo:nil repeats:YES];
    }
    return self;
}

- (void)clearDate {
    [_timerValue invalidate];_timerValue=nil;
    [self setDelegate:nil];
    [_slider release];
}

- (void)dealloc
{
    [super dealloc];
}

- (void)setStartTime:(long long)timer_ {
    _startTime = timer_;
    _startCurrentTime = _startTime;
    _currentTime = _startTime;
}

- (void)setEndTime:(long long)timer_ {
    _endTime = timer_;
    _endCurrentTime = _endTime;
}

- (void)setMonitorStartTime:(long long)timer_ {
    _monitorStartTime = timer_;
}

- (void)timerOutLower:(NSTimer *)timerOut {
    _timerIndex++;
}

- (void)sliderBeginValue:(id)sender {
    _timerIndex = 0;
    [_timer invalidate];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:NSSelectorFromString(@"timerOutLower:") userInfo:nil repeats:YES];
}

- (void)timerValue:(id)sender {
    if (_slider.upperValue>1.9) {
        _endCurrentTime+=(_timerIndex*2000);
        _endTime = _endCurrentTime;
        
        long long currentEndTime = [[NSDate date] timeIntervalSince1970];
        if (_endCurrentTime > currentEndTime) {
            _endCurrentTime = currentEndTime;
            _endTime = _endCurrentTime;
        }
        else {
            if ([_delegate respondsToSelector:@selector(selectTime2ViewChange:startTime:endTime:)]) {
                [_delegate selectTime2ViewChange:self startTime:_startCurrentTime endTime:_endCurrentTime];
            }
        }
    }
    if (_slider.lowerValue<0.1) {
        if (_isHis) {
            _startTime-=(_timerIndex*2000);
        }
        else {
            _startTime-=(_timerIndex*30);
        }
        _startCurrentTime = _startTime;
        if (!_isHis) {
            _startCurrentTime=_startCurrentTime<=_monitorStartTime?_monitorStartTime:_startCurrentTime;
        }
        
        if ([_delegate respondsToSelector:@selector(selectTime2ViewChange:startTime:endTime:)]) {
            [_delegate selectTime2ViewChange:self startTime:_startCurrentTime endTime:_endCurrentTime];
        }
    }
}

- (void)sliderChangeValue:(id)sender {
    if (_slider.lowerValue<0.1) {
        //向前
        
    }
    else if (_slider.upperValue>1.9) {
        //向后
        
    }
    else {
        if (!_isHis) {
            [bgUpperView setHidden:NO];
        }
        else {
            [bgUpperView setHidden:YES];
        }
        
        float lowerScale = (_slider.lowerValue>0.1?_slider.lowerValue:0.1);
        lowerScale -=0.1;
        lowerScale = lowerScale/1.8;
        float upperScale = (_slider.upperValue<1.9?_slider.upperValue:1.9);
        upperScale-=0.1;
        upperScale = upperScale/1.8;
        
        _startCurrentTime = (_endTime-_startTime)*lowerScale+_startTime;
        _endCurrentTime = _startTime+(_endTime-_startTime)*upperScale;
        
        NSLog(@"lowerScale：%lf----upperScale：%lf",lowerScale,upperScale);
        
        //中间选择
        if ([_delegate respondsToSelector:@selector(selectTime2ViewChange:startTime:endTime:)]) {
            [_delegate selectTime2ViewChange:self startTime:_startCurrentTime endTime:_endCurrentTime];
        }
    }
    
    if (!_isHis) {
        [bgUpperView setHidden:NO];
    }
    else {
        [bgUpperView setHidden:YES];
    }
}

- (void)sliderEndValue:(id)sender {
    NSLog(@"sliderEndValue");
    
    _timerIndex = 0;
    [_timer invalidate];_timer=nil;
    
//    if (_slider.lowerValue<=5) {
        _slider.lowerValue = 0.1;
//    }
//    if (_slider.upperValue>=95) {
        _slider.upperValue = 1.9;
//    }
    
    _startTime = _startCurrentTime;
    _endTime = _endCurrentTime;
    
    if ([_delegate respondsToSelector:@selector(selectTime2ViewEnd:startTime:endTime:)]) {
        [_delegate selectTime2ViewEnd:self startTime:_startTime endTime:_endTime];
    }
}

//是否是历史
- (void)setIsHis:(BOOL)yesOrNo {
    if (yesOrNo) {
        [_slider setUpperHandleImageNormal:[UIImage imageNamed:@"ico_slide_blue.png"]];
        [_slider setUpperHandleImageHighlighted:[UIImage imageNamed:@"ico_slide_blue.png"]];
        [bgUpperView setHidden:YES];
    }
    else {
        [_slider setUpperHandleImageNormal:[UIImage imageNamed:@"ico_slide_grey.png"]];
        [_slider setUpperHandleImageHighlighted:[UIImage imageNamed:@"ico_slide_grey.png"]];
        
        [_slider setUpperValue:1.9 animated:YES];
        
        [bgUpperView setHidden:NO];
    }
    
    _isHis = yesOrNo;
    
    [_slider refreshView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
