//
//  CMUserInfoAlrmsManager.m
//  CarMonitoring
//
//  Created by Work on 1/18/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMUserInfoAlrmsManager.h"
#import "Cm.pb.h"
#import "CMMyUserInfoManager.h"

static CMUserInfoAlrmsManager *_mUserInfoAlrmsManager;
@implementation CMUserInfoAlrmsManager
@synthesize userInfo;

+ (id)getInstance {
    @synchronized (self) {
        if (nil == _mUserInfoAlrmsManager) {
            _mUserInfoAlrmsManager = [[CMUserInfoAlrmsManager alloc] init];
        }
    }
    
    return _mUserInfoAlrmsManager;
}

- (NSArray *)getWarnList:(int32_t)vehicle_id {
    NSMutableArray *array = [NSMutableArray array];
    
    for (VehicleInfo *vi in [[CMMyUserInfoManager getInstance] getUserInfo].vehicles) {
        if (vi.vehicleId == vehicle_id) {
            
            PBArray *vrs = vi.processingAlarms;
            for (int i=0; i<[vrs count]; ++i) {
                VehicleReport *vr = [vrs objectAtIndex:i];
                [array addObject:vr.alarm];
            }
        }
    }
    
    return array;
}

- (NSArray *)getWarnAllList {
    NSMutableArray *array = [NSMutableArray array];
    
    for (VehicleInfo *vi in [[CMMyUserInfoManager getInstance] getUserInfo].vehicles) {
        PBArray *vrs = vi.processingAlarms;
        for (int i=0; i<[vrs count]; ++i) {
            VehicleReport *vr = [vrs objectAtIndex:i];
            [array addObject:vr.alarm];
        }
    }
    
    return array;
}

- (NSArray *)searchCarWarnList:(NSString *)searchQ_ items:(NSArray *)items_ {
    NSMutableArray *alarmSearchResults = [NSMutableArray array];
    
    for (VehicleInfo *vi in items_) {
        //获取每辆车
        PBArray *alrams = vi.processingAlarms;//获取每辆车的警告
        for (VehicleReport *vr in alrams) {
            Alarm *a = vr.alarm;
            if (0 == [searchQ_ length]) {
                [alarmSearchResults addObject:a];
            }
            else {
                NSMutableString *tagsString = [NSMutableString string];
                for (NSString *tag in a.tags) {
                    [tagsString appendString:tag];
                }
                
                if (0 != [tagsString rangeOfString:searchQ_].length) {
                    [alarmSearchResults addObject:a];
                }
                else if (0 != [vi.licencePlateNumber rangeOfString:searchQ_].length) {
                    [alarmSearchResults addObject:a];
                }
                else if (0 != [a.message rangeOfString:searchQ_].length) {
                    [alarmSearchResults addObject:a];
                }
            }
        }
    }
    
    return alarmSearchResults;
}

- (VehicleInfo *)objectVehicleInfoFromAlarm:(Alarm *)alarm items_:(NSArray *)items_ {
    for (VehicleInfo *vi in items_) {
        //获取每辆车
        PBArray *alrams = vi.processingAlarms;//获取每辆车的警告
        for (VehicleReport *vr in alrams) {
            Alarm *a = vr.alarm;
            if (a.alarmId == alarm.alarmId) {
                return vi;
            }
        }
    }
    
    return nil;
}

- (VehicleReport *)objectVehicleReportFromAlarm:(Alarm *)alarm items_:(NSArray *)items_ {
    for (VehicleInfo *vi in items_) {
        //获取每辆车
        PBArray *alrams = vi.processingAlarms;//获取每辆车的警告
        for (VehicleReport *vr in alrams) {
            Alarm *a = vr.alarm;
            if (a.alarmId == alarm.alarmId) {
                return vr;
            }
        }
    }
    
    return nil;
}

@end
