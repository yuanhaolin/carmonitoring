//
//  CDTVMapsBottomView.m
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CDTVMapsBottomView.h"
#import "UIUitl.h"
#import <QuartzCore/QuartzCore.h>

@interface CDTVMapsBottomView () {
    UIPickerView *typePickerYear;
    UIPickerView *typePickerMonth;
    UIPickerView *typePickerDay;
    
    UIPickerView *typePickerHour;
    UIPickerView *typePickerMinute;
    
    NSMutableArray *yearSections;
    NSMutableArray *monthSections;
    NSMutableArray *daySections;
    NSMutableArray *hourSections;
    NSMutableArray *minuteSections;
    
    UIView *bgView;
    
    //左边还是右边的时间选择器
    int _timeIndex;
    
    CGRect _oldRect;
    
    long long _startTime;
    long long _endTime;
    
    UIView *bg2View ;
}

@end

@interface CDTVMapsBottomView (Private1)
- (void)changeView;
- (void)changeSwitchView;

@end

@implementation CDTVMapsBottomView (Private1)
- (void)changeView {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:_startTime];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:_endTime];
    
    NSString *strStateTime = [dateFormatter stringFromDate:startDate];
    NSString *strEndTime = [dateFormatter stringFromDate:endDate];
    
    [self.btnTimeLeft setTitle:strStateTime forState:UIControlStateNormal];
    [self.btnTimeLeft setTitle:strStateTime forState:UIControlStateHighlighted];
    [self.btnTimeLeft setTitle:strStateTime forState:UIControlStateSelected];
    
    [self.btnTimeRight setTitle:strEndTime forState:UIControlStateNormal];
    [self.btnTimeRight setTitle:strEndTime forState:UIControlStateHighlighted];
    [self.btnTimeRight setTitle:strEndTime forState:UIControlStateSelected];
    
    [dateFormatter release];
}

- (void)changeSwitchView {
    if (_timeIndex != -1) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:_startTime];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:_endTime];
        
        NSDateComponents *componentsStart = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:startDate];
        NSDateComponents *componentsEnd = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:endDate];
        
        [self.btnTimeRight setSelected:NO];
        [self.btnTimeLeft setSelected:NO];
        
        int startYear = 0;
        int startMonth = 0;
        int startDay = 0;
        
        int startHour = 0;
        int startMin = 0;
        if (0 == _timeIndex) {
            startYear = [componentsStart year];
            startMonth = [componentsStart month];
            startDay = [componentsStart day];
            
            startHour = [componentsStart hour];
            startMin = [componentsStart minute];
            
            [self.btnTimeLeft setSelected:YES];
        }
        else {
            startYear = [componentsEnd year];
            startMonth = [componentsEnd month];
            startDay = [componentsEnd day];
            
            startHour = [componentsEnd hour];
            startMin = [componentsEnd minute];
            
            [self.btnTimeRight setSelected:YES];
        }
        
        [typePickerYear selectRow:startYear-2004 inComponent:0 animated:NO];
        [typePickerMonth selectRow:startMonth-1 inComponent:0 animated:NO];
        [typePickerDay selectRow:startDay-1 inComponent:0 animated:NO];
        
        [typePickerHour selectRow:startHour inComponent:0 animated:NO];
        [typePickerMinute selectRow:startMin inComponent:0 animated:NO];
        
        [dateFormatter release];
    }
    else {
        [self.btnTimeRight setSelected:NO];
        [self.btnTimeLeft setSelected:NO];
    }
}

@end

@interface CDTVMapsBottomView (Private)


@end

@implementation CDTVMapsBottomView (Private)


@end

@implementation CDTVMapsBottomView
@synthesize btnTimeLeft;
@synthesize btnTimeRight;
@synthesize delegate=_delegate;
@synthesize isHis;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    _oldRect = frame;
    if (self) {
        bgView = [[[NSBundle mainBundle] loadNibNamed:@"CDTVMapsBottomView" owner:self options:nil] lastObject];
        [self addSubview:bgView];
        [bgView setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        [self.btnTimeLeft.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.btnTimeRight.titleLabel setTextAlignment:NSTextAlignmentCenter];
        
        [self.btnTimeLeft setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.btnTimeLeft.titleLabel setFont:WFONT(13)];
        [self.btnTimeLeft setBackgroundColor:UIColorFromRGB(0x787878)];
        
        [self.btnTimeRight setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.btnTimeRight.titleLabel setFont:WFONT(13)];
        [self.btnTimeRight setBackgroundColor:UIColorFromRGB(0x787878)];
        
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy"];
        NSString *currentYear = [dateFormatter stringFromDate:currentDate];
        [dateFormatter release];
        
        bg2View = [[UIView alloc] initWithFrame:CGRectMake(0, bgView.frame.size.height, 320, 212)];
        [bg2View setBackgroundColor:[UIColor clearColor]];
        [self addSubview:bg2View];
        [bg2View release];
        
        yearSections = [NSMutableArray new];
        for (int i=2004; i<=[currentYear intValue]; ++i) {
            [yearSections addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        if (typePickerYear == nil) {
            typePickerYear = [[UIPickerView alloc] initWithFrame:CGRectMake(0, bgView.frame.size.height, 80, 212)];
            typePickerYear.delegate = self;
            typePickerYear.dataSource=self;
            [bg2View addSubview:typePickerYear];
            [typePickerYear release];
        }
        
        monthSections = [NSMutableArray new];
        for (int i=1; i<=12; ++i) {
            [monthSections addObject:[NSString stringWithFormat:@"%.2d",i]];
        }
        
        if (typePickerMonth == nil) {
            typePickerMonth = [[UIPickerView alloc] initWithFrame:CGRectMake(80, bgView.frame.size.height, 60, 212)];
            typePickerMonth.delegate = self;
            typePickerMonth.dataSource=self;
            [bg2View addSubview:typePickerMonth];
            [typePickerMonth release];
        }
        
        daySections = [NSMutableArray new];
        for (int i=1; i<=31; ++i) {
            [daySections addObject:[NSString stringWithFormat:@"%.2d",i]];
        }
        if (typePickerDay == nil) {
            typePickerDay = [[UIPickerView alloc] initWithFrame:CGRectMake(140, bgView.frame.size.height, 60, 212)];
            typePickerDay.delegate = self;
            typePickerDay.dataSource=self;
            [bg2View addSubview:typePickerDay];
            [typePickerDay release];
        }
        
        hourSections = [NSMutableArray new];
        for (int i=0; i<24; ++i) {
            [hourSections addObject:[NSString stringWithFormat:@"%.2d",i]];
        }
        if (typePickerHour == nil) {
            typePickerHour = [[UIPickerView alloc] initWithFrame:CGRectMake(200, bgView.frame.size.height, 60, 212)];
            typePickerHour.delegate = self;
            typePickerHour.dataSource=self;
            [bg2View addSubview:typePickerHour];
            [typePickerHour release];
        }
        
        minuteSections = [NSMutableArray new];
        for (int i=0; i<60; ++i) {
            if (0 == i) {
                
            }
            [minuteSections addObject:[NSString stringWithFormat:@"%.2d",i]];
        }
        if (typePickerMinute == nil) {
            typePickerMinute = [[UIPickerView alloc] initWithFrame:CGRectMake(260, bgView.frame.size.height, 60, 212)];
            typePickerMinute.delegate = self;
            typePickerMinute.dataSource=self;
            [bg2View addSubview:typePickerMinute];
            [typePickerMinute release];
        }
        
//        [typePickerYear ];
//        [typePickerMonth update];
//        [typePickerDay update];
//        [typePickerHour update];
//        [typePickerMinute update];
        
        _timeIndex = -1;
        
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:NSSelectorFromString(@"btnTapTapping:")];
//        [tapGesture setNumberOfTapsRequired:1];
//        [tapGesture setNumberOfTouchesRequired:1];
//        [bg2View addGestureRecognizer:tapGesture];
//        [tapGesture release];
    }
    
    return self;
}

- (void)restoreView:(BOOL)yesOrNo {
    if (yesOrNo) {
        [UIUitl toFrameAnimation:self toFrame:_oldRect];
        _timeIndex = -1;
        
        [bg2View setBackgroundColor:[UIColor clearColor]];
        //        [self.btnTimeLeft setSelected:NO];
        //        [self.btnTimeRight setSelected:NO];
    }
    else {
        CGRect newFrame = _oldRect;
        newFrame.origin.y=newFrame.origin.y-212;
        newFrame.size.height = newFrame.size.height+212;
        [UIUitl toFrameAnimation:self toFrame:newFrame];
        
        [bg2View setBackgroundColor:[UIColor whiteColor]];
    }
    
    [self.btnTimeLeft setSelected:NO];
    [self.btnTimeRight setSelected:NO];
}

- (void)setItmes:(NSArray *)items_ {
//    [sectionName addObjectsFromArray:items_];
    
//    [typePicker update];
    
//    [self.btnTimeLeft setTitle:[sectionName objectAtIndex:0] forState:UIControlStateNormal];
//    [self.btnTimeLeft setTitle:[sectionName objectAtIndex:0] forState:UIControlStateSelected];
//    [self.btnTimeRight setTitle:[sectionName objectAtIndex:[sectionName count]-1] forState:UIControlStateNormal];
//    [self.btnTimeRight setTitle:[sectionName objectAtIndex:[sectionName count]-1] forState:UIControlStateSelected];
}

- (void)setStartTime:(long long)timer1970 {
    _startTime = timer1970;
    
    [self changeView];
    
    if (self.isHis) {
        [self changeSwitchView];
    }
}

- (void)setEndTime:(long long)timer1970 {
    _endTime = timer1970;
    
    [self changeView];
    
    if (self.isHis) {
        [self changeSwitchView];
    }
}

- (void)dealloc
{
    [yearSections release];
    [monthSections release];
    [daySections release];
    [hourSections release];
    [minuteSections release];
    
    [super dealloc];
}

- (IBAction)btnLeftTapping:(id)sender {
    [self.btnTimeRight setSelected:NO];
    [self.btnTimeLeft setSelected:NO];
    
    if (!self.isHis) {
        return;
    }
    
    if (_timeIndex == 0) {
        [self restoreView:YES];
        
        if ([_delegate respondsToSelector:@selector(mapsBottomView:startTime:endTime:)]) {
            [_delegate mapsBottomView:self startTime:_startTime endTime:_endTime];
        }
        
        return;
    }
    else {
        [self restoreView:NO];
    }
    _timeIndex = 0;
    
    [self changeSwitchView];
}

- (IBAction)btnRightTapping:(id)sender {
    [self.btnTimeRight setSelected:NO];
    [self.btnTimeLeft setSelected:NO];
    
    if (!self.isHis) {
        return;
    }
    
    if (_timeIndex == 1) {
        [self restoreView:YES];
        
        if ([_delegate respondsToSelector:@selector(mapsBottomView:startTime:endTime:)]) {
            [_delegate mapsBottomView:self startTime:_startTime endTime:_endTime];
        }
        
        return;
    }
    else {
        [self restoreView:NO];
    }
    _timeIndex = 1;
    
    [self changeSwitchView];
}

- (void)btnTapTapping:(id)sender {
    [self restoreView:YES];
    
    if ([_delegate respondsToSelector:@selector(mapsBottomView:startTime:endTime:)]) {
        [_delegate mapsBottomView:self startTime:_startTime endTime:_endTime];
    }
}

#pragma mark MyPickerViewDelegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:_startTime];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:_endTime];
    
    NSDateComponents *componentsStart = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:startDate];
    NSDateComponents *componentsEnd = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:endDate];
    
    int startYear = 0;
    int startMonth = 0;
    int startDay = 0;
    
    int startHour = 0;
    int startMin = 0;
    if (0 == _timeIndex) {
        startYear = [componentsStart year];
        startMonth = [componentsStart month];
        startDay = [componentsStart day];
        
        startHour = [componentsStart hour];
        startMin = [componentsStart minute];
    }
    else {
        startYear = [componentsEnd year];
        startMonth = [componentsEnd month];
        startDay = [componentsEnd day];
        
        startHour = [componentsEnd hour];
        startMin = [componentsEnd minute];
    }
    
    if (typePickerYear == pickerView) {
        startYear = [[yearSections objectAtIndex:row] intValue];
    }
    if (typePickerMonth == pickerView) {
        startMonth = [[monthSections objectAtIndex:row] intValue];
    }
    if (typePickerDay == pickerView) {
        startDay = [[daySections objectAtIndex:row] intValue];
    }
    if (typePickerHour == pickerView) {
        startHour = [[hourSections objectAtIndex:row] intValue];
    }
    if (typePickerMinute == pickerView) {
        startMin = [[minuteSections objectAtIndex:row] intValue];
    }
    
    NSString *startDateStr = [NSString stringWithFormat:@"%d-%.2d-%.2d %.2d:%.2d",
                              startYear,startMonth,startDay,startHour,startMin];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    if (0 == _timeIndex) {
        [self.btnTimeLeft setTitle:startDateStr forState:UIControlStateNormal];
        [self.btnTimeLeft setTitle:startDateStr forState:UIControlStateHighlighted];
        [self.btnTimeLeft setTitle:startDateStr forState:UIControlStateSelected];
        
        NSDate *startDateTmp = [dateFormatter dateFromString:startDateStr];
        
        _startTime = [startDateTmp timeIntervalSince1970];
    }
    else {
        [self.btnTimeRight setTitle:startDateStr forState:UIControlStateNormal];
        [self.btnTimeRight setTitle:startDateStr forState:UIControlStateHighlighted];
        [self.btnTimeRight setTitle:startDateStr forState:UIControlStateSelected];
        
        NSDate *endDateTmp = [dateFormatter dateFromString:startDateStr];
        
        _endTime = [endDateTmp timeIntervalSince1970];
    }
    
    [dateFormatter release];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    int count = 0;
    if (typePickerYear == pickerView) {
        count = [yearSections count];
    }
    else if (typePickerMonth == pickerView) {
        count = [monthSections count];
    }
    else if (typePickerDay == pickerView) {
        count = [daySections count];
    }
    else if (typePickerHour == pickerView) {
        count = [hourSections count];
    }
    else if (typePickerMinute == pickerView) {
        count = [minuteSections count];
    }
    
    return count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *string = nil;
    if (typePickerYear == pickerView) {
        string = [yearSections objectAtIndex:row];
    }
    else if (typePickerMonth == pickerView) {
        string = [monthSections objectAtIndex:row];
    }
    else if (typePickerDay == pickerView) {
        string = [daySections objectAtIndex:row];
    }
    else if (typePickerHour == pickerView) {
        string = [hourSections objectAtIndex:row];
    }
    else if (typePickerMinute == pickerView) {
        string = [minuteSections objectAtIndex:row];
    }
    
    return string;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
