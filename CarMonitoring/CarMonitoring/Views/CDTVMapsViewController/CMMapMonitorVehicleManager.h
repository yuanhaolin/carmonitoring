//
//  CMMapMonitorVehicleManager.h
//  CarMonitoring
//
//  Created by Work on 1/17/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CMGPSEntity;
@interface CMMapMonitorVehicleManager : NSObject

+ (CMMapMonitorVehicleManager *)getInstance;

//添加一个实时地图数据
- (void)addItem:(id)item_;

//设置车的数据
- (void)setCarItem:(NSArray *)items_;

- (void)clearAll;

//获取指定车的实时地图数据
- (NSArray *)getVehiclesByID:(int32_t)vehicle_id;

//根据车返回实时地图中的gps数组
- (NSArray *)getVehiclesByIDFromGPS:(int32_t)vehicle_id startTime:(long long)startTime;

//获取报警内容
- (NSMutableArray *)getWarnList:(int32_t)vehicle_id;

//获取最新一个gps位置
- (CMGPSEntity *)getLastGPS:(int32_t)vehicle_id;

@end
