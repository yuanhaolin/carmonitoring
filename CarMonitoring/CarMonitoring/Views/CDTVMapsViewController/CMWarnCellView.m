//
//  CMWarnCellView.m
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMWarnCellView.h"
#import "Cm.pb.h"
#import "CMGPSEntity.h"

@interface CMWarnCellView () {
    BMKSearch *_baiduSearch;
    VehicleReport *_vReport;
    int _index;
    
    BOOL _isFirst;
    BOOL _isMidden;
    BOOL _isEnd;
}

@end

@implementation CMWarnCellView
@synthesize isHis;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CMWarnCellView" owner:self options:nil] lastObject];
        [self addSubview:view];
        
        [labTime setFont:WFONT(12)];
        [labTime setTextColor:UIColorFromRGB(0x949494)];
        
        [labTitle1 setFont:WFONT(15)];
        [labTitle1 setTextColor:UIColorFromRGB(0x5a93d4)];
        
        [labTitle2 setFont:WFONT(15)];
        [labTitle2 setTextColor:UIColorFromRGB(0x6f6f6f)];
        
        [lineImageView setFrame:CGRectMake(lineImageView.frame.origin.x+0.5, docImageView.frame.origin.y+2, lineImageView.frame.size.width, frame.size.height-docImageView.frame.origin.y-2)];
        
//        _baiduSearch = [[BMKSearch alloc]init];
//        [_baiduSearch setDelegate:self];
    }
    return self;
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
    [_baiduSearch setDelegate:nil];
    [_baiduSearch release];_baiduSearch=nil;
    
    [_vReport release];_vReport=nil;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)setIsFirst:(BOOL)yesOrNo {
    _isFirst = yesOrNo;
    _isEnd = NO;
    _isMidden = NO;
    
    if (!self.isHis) {
        [labTitle1 setTextColor:UIColorFromRGB(0x5a93d4)];
    }
    else {
         [labTitle1 setTextColor:UIColorFromRGB(0xff5357)];
    }
    
    [lineImageView setFrame:CGRectMake(lineImageView.frame.origin.x, docImageView.frame.origin.y+2, lineImageView.frame.size.width, self.frame.size.height-docImageView.frame.origin.y-2)];
}

- (void)setIsEnd:(BOOL)yesOrNo {
    _isEnd = yesOrNo;
    _isFirst = NO;
    _isMidden = NO;
    
    if (!self.isHis) {
        [labTitle1 setTextColor:UIColorFromRGB(0x5a93d4)];
    }
    else {
        [labTitle1 setTextColor:UIColorFromRGB(0xff5357)];
    }
    
    [lineImageView setFrame:CGRectMake(lineImageView.frame.origin.x, 0, lineImageView.frame.size.width, docImageView.frame.origin.y+2)];
}

- (void)setIsMid:(BOOL)yesOrNo {
    _isMidden = yesOrNo;
    _isFirst = NO;
    _isEnd = NO;
    
    [labTitle1 setTextColor:UIColorFromRGB(0xff5357)];
    
    [lineImageView setFrame:CGRectMake(lineImageView.frame.origin.x, 0, lineImageView.frame.size.width, self.frame.size.height)];
}

- (float)setItem:(VehicleReport *)item_ index:(int)index {
    [labTitle2 setText:@""];
    [labTitle1 setText:@""];
    [labTime setText:@""];
    
    if ([item_ isKindOfClass:[VehicleReport class]]) {
        Alarm *alarm = item_.alarm;
        _index = index;
        
        NSMutableString *tags = [NSMutableString string];
        for (NSString *s in alarm.tags) {
            [tags appendFormat:@"%@ ",s];
        }
        if (self.isHis) {
            [labTitle1 setText:[NSString stringWithFormat:@"报警%d：",index+1]];
        }
        else {
            if (_isFirst) {
                [labTitle1 setText:[NSString stringWithFormat:@"开始："]];
            }
            if (_isEnd) {
                [labTitle1 setText:[NSString stringWithFormat:@"结束："]];
            }
            if (_isMidden) {
                [labTitle1 setText:[NSString stringWithFormat:@"报警%d：",index]];
            }
        }
        BasicLocation *location = item_.location;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy\nMM/dd\nhh:mm"];
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:location.time];
        NSString *dateStr = [dateFormatter stringFromDate:date];
        
        [labTime setText:dateStr];
        
        [dateFormatter release];
        
        //方向
        NSString *directionString = nil;
        if (location.direction>=350 || location.direction<=10) {
            directionString = @"北方";
        }
        else if (location.direction>10 && location.direction<=80) {
            directionString = @"东北方";
        }
        else if (location.direction>80 && location.direction<=100) {
            directionString = @"东方";
        }
        else if (location.direction>100 && location.direction<=170) {
            directionString = @"东南方";
        }
        else if (location.direction>170 && location.direction<=190) {
            directionString = @"南方";
        }
        else if (location.direction>190 && location.direction<=260) {
            directionString = @"西南方";
        }
        else if (location.direction>260 && location.direction<=280) {
            directionString = @"西方";
        }
        else if (location.direction>280 && location.direction<=350) {
            directionString = @"西北方";
        }
        
        if (_isFirst || _isEnd) {
            [labTitle2 setText:@""];
        }
        else {
            [labTitle2 setText:[NSString stringWithFormat:@"类型：%@\n速度：%d KM/h \n方向：%@",tags,location.speed,directionString]];
        }
        
        CGSize title2Size = [labTitle2.text sizeWithFont:labTitle2.font constrainedToSize:CGSizeMake(labTitle2.frame.size.width, 1000) lineBreakMode:NSLineBreakByClipping];
        [labTitle2 setFrame:CGRectMake(labTitle2.frame.origin.x, labTitle2.frame.origin.y, labTitle2.frame.size.width, title2Size.height)];
        
//        CLLocationCoordinate2D pt = (CLLocationCoordinate2D){location.latitude, location.longitude};
//        BOOL flag = [_baiduSearch reverseGeocode:pt];
//        if (flag) {
//            NSLog(@"ReverseGeocode search success.");
//            
//        }
//        else{
//            NSLog(@"ReverseGeocode search failed!");
//        }
        
        if (_isFirst) {
            [lineImageView setFrame:CGRectMake(lineImageView.frame.origin.x, docImageView.frame.origin.y+2, lineImageView.frame.size.width, labTitle2.frame.size.height+labTitle2.frame.origin.y-lineImageView.frame.origin.y)];
        }
        if (_isMidden) {
            [lineImageView setFrame:CGRectMake(lineImageView.frame.origin.x, 0, lineImageView.frame.size.width, labTitle2.frame.size.height+labTitle2.frame.origin.y+5)];
        }
        if (_isEnd) {
            [lineImageView setFrame:CGRectMake(lineImageView.frame.origin.x, 0, lineImageView.frame.size.width, docImageView.frame.origin.y+2)];
        }
    }
    else {
        CMGPSEntity *gpsEntity = (CMGPSEntity *)item_;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy\nMM/dd\nhh:mm"];
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:gpsEntity.time];
        NSString *dateStr = [dateFormatter stringFromDate:date];
        
        [dateFormatter release];
        
        if (_isFirst) {
            [labTitle1 setText:[NSString stringWithFormat:@"开始："]];
            [labTime setText:dateStr];
        }
        if (_isEnd) {
            [labTitle1 setText:[NSString stringWithFormat:@"结束："]];
            [labTime setText:dateStr];
        }
        if (_isMidden) {
            [labTitle1 setText:[NSString stringWithFormat:@"报警%d：",index]];
            [labTime setText:@""];
        }
        
//        CLLocationCoordinate2D pt = (CLLocationCoordinate2D){gpsEntity.lat, gpsEntity.lon};
//        BOOL flag = [_baiduSearch reverseGeocode:pt];
//        if (flag) {
//            NSLog(@"ReverseGeocode search success.");
//            
//        }
//        else{
//            NSLog(@"ReverseGeocode search failed!");
//        }
    }
    
    return labTitle2.frame.size.height+labTitle2.frame.origin.y;
}

- (void)refreshAddress:(NSString *)address index:(int)index_ {
    if (self.isHis) {
        [labTitle1 setText:[NSString stringWithFormat:@"报警%d：%@",index_+1,address]];
    }
    else {
        if (_isFirst) {
            [labTitle1 setText:[NSString stringWithFormat:@"开始：%@",address]];
        }
        if (_isEnd) {
            [labTitle1 setText:[NSString stringWithFormat:@"结束：%@",address]];
        }
        if (_isMidden) {
            [labTitle1 setText:[NSString stringWithFormat:@"报警%d：%@",index_,address]];
        }
    }
}

//- (void)setFrame:(CGRect)frame {
//    [super setFrame:frame];
//    
//    [lineImageView setFrame:CGRectMake(lineImageView.frame.origin.x, docImageView.frame.origin.y+2, lineImageView.frame.size.width, frame.size.height)];
//}

- (void)onGetAddrResult:(BMKAddrInfo*)result_ errorCode:(int)error
{
//    Alarm *alarm = _vReport.alarm;
//    
//    NSMutableString *tags = [NSMutableString string];
//    for (NSString *s in alarm.tags) {
//        [tags appendFormat:@"%@ ",s];
//    }
    
    if (self.isHis) {
        [labTitle1 setText:[NSString stringWithFormat:@"报警%d：%@",_index+1,result_.strAddr]];
    }
    else {
        if (_isFirst) {
            [labTitle1 setText:[NSString stringWithFormat:@"开始：%@",result_.strAddr]];
        }
        if (_isEnd) {
            [labTitle1 setText:[NSString stringWithFormat:@"结束：%@",result_.strAddr]];
        }
        if (_isMidden) {
            [labTitle1 setText:[NSString stringWithFormat:@"报警%d：%@",_index,result_.strAddr]];
        }
    }
}

@end
