//
//  CMWarnView.h
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"
#import "Cm.pb.h"

@class VehicleInfo;

@protocol CMWarnViewDelegate
- (void)warnView:(id)warnView center:(CLLocationCoordinate2D)center vehicleReport:(VehicleReport *)vehicleReport;

@end

@interface CMWarnView : UIView <UITableViewDataSource,UITableViewDelegate,BMKSearchDelegate>

@property (nonatomic,retain) id delegate;
@property (nonatomic,retain) NSArray *currentPolylinesGps;

- (void)setHistoryItems:(NSArray *)items_ vehicle:(VehicleInfo *)vehicle;
- (void)setMonitorItems:(NSArray *)items_ vehicle:(VehicleInfo *)vehicle;

- (void)setIsMinimize:(BOOL)yesOrNo;

@end
