//
//  CMMapMonitorVehicleManager.m
//  CarMonitoring
//
//  Created by Work on 1/17/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMMapMonitorVehicleManager.h"
#import "Cm.pb.h"
#import "CMGPSEntity.h"
#import "BMapKit.h"
#import "CLLocation+YCLocation.h"
#import "BMapKit.h"
#import "BMKMapView.h"
#import "CMMyUserInfoManager.h"

@interface CMMapMonitorVehicleEntity:NSObject

@property (nonatomic,assign) int32_t vehicle_id;
@property (nonatomic,assign) long long time;
@property (nonatomic,retain) NSMutableArray *gpss;
@end

@implementation CMMapMonitorVehicleEntity
@synthesize vehicle_id;
@synthesize gpss;
@synthesize time;

- (void)dealloc
{
    [self setGpss:nil];
    [super dealloc];
}

@end

@interface CMMapMonitorVehicleManager () {
    NSMutableArray *_items;
    
    NSArray *_carItems;
}

@end

static CMMapMonitorVehicleManager *_pMapMonitorVehicleManager;
@implementation CMMapMonitorVehicleManager

+ (id)getInstance {
    @synchronized (self) {
        if (nil == _pMapMonitorVehicleManager) {
            _pMapMonitorVehicleManager = [[CMMapMonitorVehicleManager alloc] init];
        }
    }
    
    return _pMapMonitorVehicleManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _items = [NSMutableArray new];
    }
    return self;
}

- (void)dealloc
{
    [_items release];
    [super dealloc];
}

//设置车的数据
- (void)setCarItem:(NSArray *)items_ {
    [_carItems release];_carItems=nil;
    _carItems = [items_ retain];
}

- (void)addItem:(id)item_ {
    VehicleReport *mv = (VehicleReport *)item_;
    
    [_items addObject:mv];
}

- (void)clearAll {
    [_items removeAllObjects];
}

- (NSArray *)getVehiclesByID:(int32_t)vehicle_id {
    NSMutableArray *array = [NSMutableArray array];
    
    for (int i=0; i<[_items count]; ++i) {
        VehicleReport *mv = [_items objectAtIndex:i];
        VehicleReport_Builder *mvb = [mv toBuilder];
        if (mvb.vehicleId == vehicle_id) {
            [array addObject:mv];
        }
    }
    
    return array;
}

//根据车返回实时地图中的gps数组
- (NSArray *)getVehiclesByIDFromGPS:(int32_t)vehicle_id startTime:(long long)startTime {
    NSMutableArray *gpsArray = [NSMutableArray array];
    
    VehicleInfo *vInfo = [[CMMyUserInfoManager getInstance] getVehicleInfoFromVehicleId:vehicle_id];
    NSString *licencePlateNumber = vInfo.licencePlateNumber;
    
    for (int i=0; i<[_items count]; ++i) {
        VehicleReport *vr = [_items objectAtIndex:i];
        if (vr.vehicleId == vehicle_id) {
            if (startTime>vr.location.time) {
                continue;
            }

            CLLocation *location = [[CLLocation alloc] initWithLatitude:vr.location.latitude longitude:vr.location.longitude];
            CLLocation *newLocation = [location locationMarsFromEarth];
            CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
            
            CMGPSEntity *gpsEntity = [CMGPSEntity new];
            [gpsEntity setLon:baiduLocation.coordinate.longitude];
            [gpsEntity setLat:baiduLocation.coordinate.latitude];
            [gpsEntity setTime:vr.location.time];
            [gpsEntity setLicencePlateNumber:licencePlateNumber];
            
            [gpsArray addObject:gpsEntity];
            
            [gpsEntity release];
        }
    }
    
    return gpsArray;
}

- (NSMutableArray *)getWarnList:(int32_t)vehicle_id {
    NSMutableArray *array = [NSMutableArray array];
    for (VehicleReport *vr in _items) {
        if (vehicle_id == vr.vehicleId && 0 != vr.alarm.alarmId) {
            [array addObject:vr];
        }
    }
    
    //实时地图需要设置开始地主与结束的地方
    NSMutableArray *tmpArray = [NSMutableArray array];
    for (VehicleReport *vr in _items) {
        if (vehicle_id == vr.vehicleId) {
            [tmpArray addObject:vr];
        }
    }
    
    if ([tmpArray count]>0) {
        VehicleReport *vReport = (VehicleReport *)[tmpArray objectAtIndex:0];
        BasicLocation_Builder *bb = [[vReport location] toBuilder];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:bb.latitude longitude:bb.longitude];
        CLLocation *newLocation = [location locationMarsFromEarth];
        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
        
        CMGPSEntity *gpsEntity = [CMGPSEntity new];
        [gpsEntity setLon:baiduLocation.coordinate.longitude];
        [gpsEntity setLat:baiduLocation.coordinate.latitude];
        [gpsEntity setTime:vReport.location.time];
        
        [array insertObject:gpsEntity atIndex:0];
        
        [gpsEntity release];
    }
    
    if ([tmpArray count]>0) {
        VehicleReport *vReport = (VehicleReport *)[tmpArray objectAtIndex:[tmpArray count]-1];
        BasicLocation_Builder *bb = [[vReport location] toBuilder];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:bb.latitude longitude:bb.longitude];
        CLLocation *newLocation = [location locationMarsFromEarth];
        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
        
        CMGPSEntity *gpsEntity = [CMGPSEntity new];
        [gpsEntity setLon:baiduLocation.coordinate.longitude];
        [gpsEntity setLat:baiduLocation.coordinate.latitude];
        [gpsEntity setTime:vReport.location.time];
        
        [array addObject:gpsEntity];
        
        [gpsEntity release];
    }
    
    return array;
}

//获取最后一个gps位置
- (CMGPSEntity *)getLastGPS:(int32_t)vehicle_id {
    VehicleInfo *vInfo = [[CMMyUserInfoManager getInstance] getVehicleInfoFromVehicleId:vehicle_id];
    NSString *licencePlateNumber = vInfo.licencePlateNumber;
    
    CMGPSEntity *gpsEntity = nil;
//    for (int i=[_items count]-1; i>=0; i--) {
    for (int i=0; i<[_items count]; i++) {
        VehicleReport *vr = [_items objectAtIndex:i];
        if (vr.vehicleId == vehicle_id) {
            gpsEntity = [CMGPSEntity new];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:vr.location.latitude longitude:vr.location.longitude];
            CLLocation *newLocation = [location locationMarsFromEarth];
            CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
            
            gpsEntity.lat = baiduLocation.coordinate.latitude;
            gpsEntity.lon = baiduLocation.coordinate.longitude;
            gpsEntity.time = vr.location.time;
            gpsEntity.licencePlateNumber = licencePlateNumber;
            
            [location release];
        }
    }
    
    if (nil == gpsEntity) {
        gpsEntity = [CMGPSEntity new];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:vInfo.lastReport.location.latitude longitude:vInfo.lastReport.location.longitude];
        CLLocation *newLocation = [location locationMarsFromEarth];
        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
        
        gpsEntity.lat = baiduLocation.coordinate.latitude;
        gpsEntity.lon = baiduLocation.coordinate.longitude;
        gpsEntity.time = vInfo.lastReport.location.time;
        gpsEntity.licencePlateNumber = licencePlateNumber;
        
        [location release];
    }
    
    return [gpsEntity autorelease];
}

@end
