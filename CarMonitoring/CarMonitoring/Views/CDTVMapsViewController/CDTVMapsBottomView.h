//
//  CDTVMapsBottomView.h
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyPickerView.h"

@protocol CDTVMapsBottomViewDelegate
- (void)mapsBottomView:(id)mapsBottomView startTime:(long long)startTime endTime:(long long)endTime;

@end

@interface CDTVMapsBottomView : UIView <UIPickerViewDataSource,UIPickerViewDelegate> {
    id _delegate;
}
@property (nonatomic,retain) id delegate;

@property (nonatomic,assign) IBOutlet UIButton *btnTimeLeft;
@property (nonatomic,assign) IBOutlet UIButton *btnTimeRight;
@property (nonatomic,assign) BOOL isHis;

- (void)setItmes:(NSArray *)items_;

- (void)setStartTime:(long long)timer1970;
- (void)setEndTime:(long long)timer1970;

- (void)restoreView:(BOOL)yesOrNo;

@end
