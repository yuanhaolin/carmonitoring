//
//  CMMapHistoryManager.m
//  CarMonitoring
//
//  Created by Work on 1/17/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMMapHistoryManager.h"
#import "Cm.pb.h"
#import "CMGPSEntity.h"
#import "CLLocation+YCLocation.h"
#import "BMapKit.h"
#import "CMMyUserInfoManager.h"

@interface CMMapHistoryManager () {
    NSMutableArray *_items;
}

@end

static CMMapHistoryManager *_pMapHistoryManager;
@implementation CMMapHistoryManager

+ (id)getInstance {
    @synchronized (self) {
        if (nil == _pMapHistoryManager) {
            _pMapHistoryManager = [[CMMapHistoryManager alloc] init];
        }
    }
    
    return _pMapHistoryManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _items = [NSMutableArray new];
    }
    return self;
}

- (void)dealloc
{
    [_items release];
    [super dealloc];
}

- (void)addItem:(id)item_ {
    //SimpleHistory
    [_items addObject:item_];
}

- (void)clearAll {
    [_items removeAllObjects];
}

- (NSArray *)getGPS:(long long)startTime endTime:(long long)endTime {
    NSMutableArray *vehicleArray = [NSMutableArray array];
    
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    int idx = 0;
    for (int i=0; i<[_items count]; ++i) {
        SimpleHistory *sh = [_items objectAtIndex:i];
        SimpleHistory_Builder *shb = [sh toBuilder];
        NSMutableArray *shArray = [NSMutableArray array];
        [vehicleArray addObject:shArray];
        
        int fraction = [sh fraction];
        
        //取车牌号
        NSString *lineNumber = @"";
        if ([sh.details count]>0) {
            VehicleReport *vReport = [sh.details objectAtIndex:0];
            VehicleInfo *vInfo = [[CMMyUserInfoManager getInstance] getVehicleInfoFromVehicleId:vReport.vehicleId];
            
            lineNumber = vInfo.licencePlateNumber;
        }
        
        //获取所有点
        for (int j=0; j<[shb.locationBuckets count]; ++j) {
            LocationBucket *lb = (LocationBucket *)[shb.locationBuckets objectAtIndex:j];
            LocationBucket_Builder *lbb = [lb toBuilder];
            
            float start_time = lbb.startTime;
            float start_latitude = lbb.startLatitude;
            float start_longitude = lbb.startLongitude;
            
            for (int j = 0; j<[lbb.locations count]; ++j) {
                SimpleLocation *sl = [lbb.locations objectAtIndex:j];
                SimpleLocation_Builder *slb = [sl toBuilder];
                
                float time_from_start = slb.timeFromStart;
                long long newTime = start_time+time_from_start/fraction;
                
                if (startTime<=newTime && endTime>=newTime) {
                    float latitude_from_start = slb.latitudeFromStart;
                    float longitude_from_start = slb.longitudeFromStart;
                    
                    //                float newLat = start_latitude+latitude_from_start/fraction+idx*0.01;
                    //                float newLon = start_longitude+longitude_from_start/fraction+idx*0.01;
                    float newLat = start_latitude+latitude_from_start/fraction;
                    float newLon = start_longitude+longitude_from_start/fraction;
                    
                    CLLocation *location = [[CLLocation alloc] initWithLatitude:newLat longitude:newLon];
                    CLLocation *newLocation = [location locationMarsFromEarth];
                    CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
                    
                    CMGPSEntity *gpsEntity = [CMGPSEntity new];
                    gpsEntity.lat = baiduLocation.coordinate.latitude;
                    gpsEntity.lon = baiduLocation.coordinate.longitude;
                    gpsEntity.time = newTime;
                    gpsEntity.licencePlateNumber = lineNumber;
                    
                    [location release];
                    
                    [shArray addObject:gpsEntity];
                    [gpsEntity release];
                }
            }
        }
        
        idx++;
    }
    
    [pool drain];
    
    return vehicleArray;
}

- (NSArray *)getGPS:(long long)startTime endTime:(long long)endTime vehicle_id:(int32_t)vehicle_id {
    NSMutableArray *shArray = [NSMutableArray array];
    
    int idx = 0;
    for (int i=0; i<[_items count]; ++i) {
        SimpleHistory *sh = [_items objectAtIndex:i];
        SimpleHistory_Builder *shb = [sh toBuilder];
        
        int fraction = [sh fraction];
        
        //车牌号
        NSString *lineNumber = @"";
        if ([sh.details count]>0) {
            VehicleReport *vReport = [sh.details objectAtIndex:0];
            if (vReport.vehicleId != vehicle_id) {
                continue;
            }
            VehicleInfo *vInfo = [[CMMyUserInfoManager getInstance] getVehicleInfoFromVehicleId:vReport.vehicleId];
            
            lineNumber = vInfo.licencePlateNumber;
        }
        
        for (int j=0; j<[shb.locationBuckets count]; ++j) {
            LocationBucket *lb = (LocationBucket *)[shb.locationBuckets objectAtIndex:j];
            LocationBucket_Builder *lbb = [lb toBuilder];
            
            float start_time = lbb.startTime;
            float start_latitude = lbb.startLatitude;
            float start_longitude = lbb.startLongitude;
            
            for (int j = 0; j<[lbb.locations count]; ++j) {
                SimpleLocation *sl = [lbb.locations objectAtIndex:j];
                SimpleLocation_Builder *slb = [sl toBuilder];
                
                float time_from_start = slb.timeFromStart;
                long long newTime = start_time+time_from_start/fraction;
                
                if (startTime<=newTime && endTime>=newTime) {
                    float latitude_from_start = slb.latitudeFromStart;
                    float longitude_from_start = slb.longitudeFromStart;
                    
                    //                float newLat = start_latitude+latitude_from_start/fraction+idx*0.01;
                    //                float newLon = start_longitude+longitude_from_start/fraction+idx*0.01;
                    float newLat = start_latitude+latitude_from_start/fraction;
                    float newLon = start_longitude+longitude_from_start/fraction;
                    
                    CLLocation *location = [[CLLocation alloc] initWithLatitude:newLat longitude:newLon];
                    CLLocation *newLocation = [location locationMarsFromEarth];
                    CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
                    
                    CMGPSEntity *gpsEntity = [CMGPSEntity new];
                    gpsEntity.lat = baiduLocation.coordinate.latitude;
                    gpsEntity.lon = baiduLocation.coordinate.longitude;
                    gpsEntity.time = newTime;
                    gpsEntity.licencePlateNumber = lineNumber;
                    
                    [location release];
                    
                    [shArray addObject:gpsEntity];
                    [gpsEntity release];
                }
            }
        }
        
        idx++;
    }
    
    return shArray;
}

- (NSMutableArray *)getWarnList:(int32_t)vehicle_id {
    NSMutableArray *array = [NSMutableArray array];
    for (SimpleHistory *sh in _items) {
        PBArray *details = sh.details;
        for (int i=0; i<[details count]; ++i) {
            VehicleReport *vr = [details objectAtIndex:i];
            if (vehicle_id == vr.vehicleId) {
                [array addObject:vr];
            }
        }
    }
    
    for (int i=0; i<[_items count]; ++i) {
        SimpleHistory *sh = [_items objectAtIndex:i];
        int fraction = [sh fraction];
        
        if ([sh.details count]>0) {
            VehicleReport *vReport = (VehicleReport *)[sh.details objectAtIndex:0];
            if (vReport.vehicleId == vehicle_id) {
                //获取第一个gps坐标
                if ([sh.locationBuckets count]>0) {
                    LocationBucket *lb = (LocationBucket *)[sh.locationBuckets objectAtIndex:0];
                    LocationBucket_Builder *lbb = [lb toBuilder];
                    
                    float start_time = lbb.startTime;
                    float start_latitude = lbb.startLatitude;
                    float start_longitude = lbb.startLongitude;
                    
                    if ([lbb.locations count]>0) {
                        SimpleLocation *sl = [lbb.locations objectAtIndex:0];
                        SimpleLocation_Builder *slb = [sl toBuilder];
                        
                        float time_from_start = slb.timeFromStart;
                        long long newTime = start_time+time_from_start/fraction;
                        
                        float latitude_from_start = slb.latitudeFromStart;
                        float longitude_from_start = slb.longitudeFromStart;
                        
                        float newLat = start_latitude+latitude_from_start/fraction;
                        float newLon = start_longitude+longitude_from_start/fraction;
                        
                        CLLocation *location = [[CLLocation alloc] initWithLatitude:newLat longitude:newLon];
                        CLLocation *newLocation = [location locationMarsFromEarth];
                        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
                        
                        CMGPSEntity *gpsEntity = [CMGPSEntity new];
                        gpsEntity.lat = baiduLocation.coordinate.latitude;
                        gpsEntity.lon = baiduLocation.coordinate.longitude;
                        gpsEntity.time = newTime;
                        [array insertObject:gpsEntity atIndex:0];
                        
                        [location release];
                        
                        [gpsEntity release];
                    }
                }
                
                //获取最后一个gps
                if ([sh.locationBuckets count]>=1) {
                    LocationBucket *lb = (LocationBucket *)[sh.locationBuckets objectAtIndex:[sh.locationBuckets count]-1];
                    LocationBucket_Builder *lbb = [lb toBuilder];
                    
                    float start_time = lbb.startTime;
                    float start_latitude = lbb.startLatitude;
                    float start_longitude = lbb.startLongitude;
                    
                    if ([lbb.locations count]>0) {
                        SimpleLocation *sl = [lbb.locations objectAtIndex:[lbb.locations count]-1];
                        SimpleLocation_Builder *slb = [sl toBuilder];
                        
                        float time_from_start = slb.timeFromStart;
                        long long newTime = start_time+time_from_start/fraction;
                        
                        float latitude_from_start = slb.latitudeFromStart;
                        float longitude_from_start = slb.longitudeFromStart;
                        
                        float newLat = start_latitude+latitude_from_start/fraction;
                        float newLon = start_longitude+longitude_from_start/fraction;
                        
                        CLLocation *location = [[CLLocation alloc] initWithLatitude:newLat longitude:newLon];
                        CLLocation *newLocation = [location locationMarsFromEarth];
                        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
                        
                        CMGPSEntity *gpsEntity = [CMGPSEntity new];
                        gpsEntity.lat = baiduLocation.coordinate.latitude;
                        gpsEntity.lon = baiduLocation.coordinate.longitude;
                        gpsEntity.time = newTime;
                        [array addObject:gpsEntity];
                        
                        [location release];
                        
                        [gpsEntity release];
                    }
                }
                
                break;
            }
        }
        
    }
    
//    if ([array count]>0) {
//        [array insertObject:[array objectAtIndex:0] atIndex:0];
//        [array addObject:[array objectAtIndex:[array count]-1]];
//    }
    
    return array;
}

@end
