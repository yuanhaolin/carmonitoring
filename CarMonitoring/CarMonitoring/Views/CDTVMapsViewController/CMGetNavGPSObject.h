//
//  CMGetNavGPSObject.h
//  CarMonitoring
//
//  Created by Work on 1/28/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentControl.h"
#import "BMapKit.h"

@protocol CMGetNavGPSObjectDelegate
- (void)getNavGPSObject:(id)getNavGPSObject polyline:(BMKPolyline *)polyline lastLocation:(CLLocationCoordinate2D)lastLocation;

@end

@interface CMGetNavGPSObject : NVParentControl <BMKSearchDelegate>

@property (nonatomic,retain) id delegate;

+ (CMGetNavGPSObject *)getInstance ;

- (void)getNavToMe:(CLLocationCoordinate2D )locationCoordinate meLocation:(CLLocationCoordinate2D )meLocation;

- (void)refreshData;

@end
