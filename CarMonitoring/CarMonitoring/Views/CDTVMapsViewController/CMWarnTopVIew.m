//
//  CMWarnTopVIew.m
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMWarnTopVIew.h"

@implementation CMWarnTopVIew
@synthesize btnTap;
@synthesize labTitel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CMWarnTopVIew" owner:self options:nil] lastObject];
        [self addSubview:view];
        
        [labTitel setTextColor:UIColorFromRGB(0x6a6a6a)];
        [labTitel setFont:WFONT(18)];
        
        [self setBackgroundColor:UIColorFromRGB(0xeeeeee)];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
