//
//  CMGetNavGPSObject.m
//  CarMonitoring
//
//  Created by Work on 1/28/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMGetNavGPSObject.h"
#import "MCCGPSObject.h"
#import "CMGPSEntity.h"
#import "CLLocation+YCLocation.h"

@interface CMGetNavGPSObject () {
    BMKSearch *_baiduSearch;
    
    CLLocationCoordinate2D _location;
    CLLocationCoordinate2D _meLocation;
}

@end

static CMGetNavGPSObject *_pGetNavGPSObject;
@implementation CMGetNavGPSObject
@synthesize delegate=_delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _baiduSearch = [[BMKSearch alloc]init];
        [_baiduSearch setDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [_delegate release];
    [_baiduSearch release];
    [super dealloc];
}

+ (CMGetNavGPSObject *)getInstance {
    @synchronized (self) {
        if (nil == _pGetNavGPSObject) {
            _pGetNavGPSObject = [[CMGetNavGPSObject alloc] initWithFrame:CGRectZero];
        }
    }
    
    return _pGetNavGPSObject;
}

- (void)getNavToMe:(CLLocationCoordinate2D )locationCoordinate meLocation:(CLLocationCoordinate2D )meLocation {
    _location = locationCoordinate;
    _meLocation = meLocation;
    
//    BMKPlanNode* start = [[[BMKPlanNode alloc]init] autorelease];
//	[start setPt:CLLocationCoordinate2DMake(meLocation.latitude, meLocation.longitude)];
//	BMKPlanNode* end = [[[BMKPlanNode alloc]init] autorelease];
//    [end setPt:CLLocationCoordinate2DMake(locationCoordinate.latitude, locationCoordinate.longitude)];
//    
//    BOOL flag = [_baiduSearch drivingSearch:@"" startNode:start endCity:@"" endNode:end];
//    
//	if (flag) {
//		NSLog(@"search success.");
//	}
//    else{
//        NSLog(@"search failed!");
//    }
}

- (void)refreshData {
//    //我的位置
//    CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:_meLocation.latitude longitude:_meLocation.longitude];
//    CLLocation *newStartLocation = [startLocation locationMarsFromEarth];
//    CLLocation *baiduStartLocation = [newStartLocation locationBaiduFromMars];
    
    BMKPlanNode* start = [[[BMKPlanNode alloc]init] autorelease];
	[start setPt:_meLocation];
    
    //目标地址
//    CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:_location.latitude longitude:_location.longitude];
//    CLLocation *newEndLocation = [endLocation locationMarsFromEarth];
//    CLLocation *baiduEndLocation = [newEndLocation locationBaiduFromMars];
    
	BMKPlanNode* end = [[[BMKPlanNode alloc]init] autorelease];
    [end setPt:_location];
    
    if (0 == _location.latitude) {
        return;
    }
    
    BOOL flag = [_baiduSearch drivingSearch:@"" startNode:start endCity:@"" endNode:end];
    
	if (flag) {
		NSLog(@"search success.");
	}
    else{
        NSLog(@"search failed!");
        [self alertMsg:@"导航失败，请重试！"];
    }
}

- (void)onGetDrivingRouteResult:(BMKPlanResult*)result errorCode:(int)error
{
    if (result != nil) {
        // error 值的意义请参考BMKErrorCode
        if (error == BMKErrorOk) {
            //            [_mapView setCenterCoordinate:result.startNode.pt];
            
            BMKRoutePlan* plan = (BMKRoutePlan*)[result.plans objectAtIndex:0];
            
            int split = 5;
            
            // 下面开始计算路线，并添加驾车提示点
            int index = 0;
            //            int size = [plan.routes count];
            for (int i = 0; i < 1; i++) {
                BMKRoute* route = [plan.routes objectAtIndex:i];
                for (int j = 0; j < route.pointsCount; j++) {
                    int len = floor([route getPointsNum:j]/split);
                    index += (len+1);
                }
            }
            
//            long long time = [[NSDate date] timeIntervalSince1970];
            
            BMKMapPoint* points = new BMKMapPoint[index];
            index = 0;
            for (int i = 0; i < 1; i++) {
                BMKRoute* route = [plan.routes objectAtIndex:i];
                for (int j = 0; j < route.pointsCount; j++) {
                    int len = floor([route getPointsNum:j]/split);
                    int oldLen = [route getPointsNum:j];
                    
                    BMKMapPoint* pointArrayTmp = (BMKMapPoint*)[route getPoints:j];
                    
                    BMKMapPoint *pointArray = new BMKMapPoint[len+1];
                    for (int m=0; m<len; ++m) {
                        pointArray[m] = pointArrayTmp[m*split];
//                        BMKMapPoint p = pointArray[m];
//                        NSLog(@"%@",BMKStringFromMapPoint(p));
                    }
                    pointArray[len] = pointArrayTmp[oldLen-1];
                    
                    memcpy(points + index, pointArray, (len+1) * sizeof(BMKMapPoint));
                    index += len+1;
                    
                    delete []pointArray;
                }
            }
            
//            BMKMapPoint* newpoints = new BMKMapPoint[index];
//            
//            for (int i=0; i<index; ++i) {
//                BMKMapPoint mapPoint = points[i];
//                CLLocationCoordinate2D point = BMKCoordinateForMapPoint(mapPoint);
//                
//                CLLocation *location = [[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude];
//                CLLocation *newLocation = [location locationMarsFromEarth];
//                CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
//                
//                CLLocationCoordinate2D newPoint = CLLocationCoordinate2DMake(baiduLocation.coordinate.latitude, baiduLocation.coordinate.longitude);

//                BMKMapPoint newMapPoint = BMKMapPointForCoordinate(newPoint);
//                newpoints[i] = newMapPoint;
//            }
            
            BMKPolyline* polyLine = [BMKPolyline polylineWithPoints:points count:index];            
            
            // 根究计算的点，构造并添加路线覆盖物
            delete []points;
            
            if ([self.delegate respondsToSelector:@selector(getNavGPSObject:polyline:lastLocation:)]) {
                [self.delegate getNavGPSObject:self polyline:polyLine lastLocation:_location];
            }
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
