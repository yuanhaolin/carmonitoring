//
//  CMWarnView.m
//  CarMonitoring
//
//  Created by Work on 1/4/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#define ROW_HIG 61
#define TABLEVIEW_TAG   1001

#import "CMWarnView.h"
#import "CMWarnTopVIew.h"
#import "CMWarnCellView.h"
#import "UIUitl.h"
#import "AppManager.h"
#import "Cm.pb.h"
#import "AppDelegate.h"
#import "CMHistoryDescViewController.h"
#import "CMUserInfoAlrmsManager.h"
#import "CDTVMapsViewController.h"
#import "CMGPSEntity.h"
#import "CLLocation+YCLocation.h"

@interface CMWarnView () {
    CMWarnTopVIew *_topView;
    NSArray *_items;
    
    VehicleInfo *_vInfo;
    
    BOOL _isHis;
    
    int _refreshIndex;
    BMKSearch *_baiduSearch;
    
    NSMutableArray *addressList;
    
    BOOL _isMin;
}

@end

@implementation CMWarnView
@synthesize currentPolylinesGps;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:self.bounds];
        [tableView setTag:TABLEVIEW_TAG];
        [tableView setRowHeight:ROW_HIG];
        [tableView setDataSource:self];
        [tableView setDelegate:self];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self addSubview:tableView];
        [tableView release];
        [tableView setBackgroundColor:[UIColor whiteColor]];
        
        _topView = [[CMWarnTopVIew alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        [_topView.btnTap addTarget:self action:NSSelectorFromString(@"btnTapping:") forControlEvents:UIControlEventTouchUpInside];
        
        addressList = [NSMutableArray new];
    }
    return self;
}

- (void)dealloc
{
    [_topView release];
    [_items release];
    [addressList release];
    [self setCurrentPolylinesGps:nil];
    [super dealloc];
}

- (void)setHistoryItems:(NSArray *)items_ vehicle:(VehicleInfo *)vehicle {
    _refreshIndex=0;
    [addressList removeAllObjects];
    
    [_items release];_items=nil;
    _items = [items_ retain];
    
    [_vInfo release];_vInfo=nil;
    _vInfo = [vehicle retain];
    
    _isHis = YES;
    
    [_topView.labTitel setText:vehicle.licencePlateNumber];
    
//    if ([items_ count]>0) {
//        id gpsEntity = [items_ objectAtIndex:0];
//        
//        CLLocation *location = nil;
//        if ([gpsEntity isKindOfClass:[CMGPSEntity class]]) {
//            CMGPSEntity *gEntity = (CMGPSEntity *)gpsEntity;
//            location = [[CLLocation alloc] initWithLatitude:gEntity.lat longitude:gEntity.lon];
//        }
//        else if ([gpsEntity isKindOfClass:[VehicleReport class]]) {
//            VehicleReport *item_ = (VehicleReport *)gpsEntity;
//            BasicLocation *blocation = item_.location;
//            
//            location = [[CLLocation alloc] initWithLatitude:blocation.latitude longitude:blocation.longitude];
//        }
//        
//        CLLocation *newLocation = [location locationMarsFromEarth];
//        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
//        
//        CLLocationCoordinate2D pt = (CLLocationCoordinate2D){baiduLocation.coordinate.latitude, baiduLocation.coordinate.longitude};
//        BOOL flag = [_baiduSearch reverseGeocode:pt];
//        if (flag) {
//            NSLog(@"ReverseGeocode search success.");
//            
//        }
//        else{
//            NSLog(@"ReverseGeocode search failed!");
//        }
//    }
    
    UITableView *tableView = (UITableView *)[self viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

- (void)setMonitorItems:(NSArray *)items_ vehicle:(VehicleInfo *)vehicle {
    _refreshIndex=0;
    [addressList removeAllObjects];
    
    [_items release];_items=nil;
    _items = [items_ retain];
    
    [_vInfo release];_vInfo=nil;
    _vInfo = [vehicle retain];
    
    _isHis = NO;
    
    [_topView.labTitel setText:vehicle.licencePlateNumber];
    
    UITableView *tableView = (UITableView *)[self viewWithTag:TABLEVIEW_TAG];
    [tableView reloadData];
}

- (void)setIsMinimize:(BOOL)yesOrNo {
    _isMin = yesOrNo;
    
    if (yesOrNo) {
        [_baiduSearch setDelegate:nil];
        [_baiduSearch release];_baiduSearch=nil;
    }
    else {
        [_baiduSearch setDelegate:nil];
        [_baiduSearch release];_baiduSearch=nil;
        _baiduSearch = [[BMKSearch alloc]init];
        [_baiduSearch setDelegate:self];
        
        [addressList removeAllObjects];
        _refreshIndex = 0;
        
        if ([_items count]>0) {
            id gpsEntity = [_items objectAtIndex:0];
            
            CLLocation *location = nil;
            if ([gpsEntity isKindOfClass:[CMGPSEntity class]]) {
                CMGPSEntity *gEntity = (CMGPSEntity *)gpsEntity;
                location = [[[CLLocation alloc] initWithLatitude:gEntity.lat longitude:gEntity.lon] autorelease];
            }
            else if ([gpsEntity isKindOfClass:[VehicleReport class]]) {
                VehicleReport *item_ = (VehicleReport *)gpsEntity;
                BasicLocation *blocation = item_.location;
                
                location = [[CLLocation alloc] initWithLatitude:blocation.latitude longitude:blocation.longitude];
            }
            
            CLLocation *newLocation = [location locationMarsFromEarth];
            CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
            
            CLLocationCoordinate2D pt = (CLLocationCoordinate2D){baiduLocation.coordinate.latitude, baiduLocation.coordinate.longitude};
            BOOL flag = [_baiduSearch reverseGeocode:pt];
            if (flag) {
                NSLog(@"ReverseGeocode search success.");
                
            }
            else{
                NSLog(@"ReverseGeocode search failed!");
            }
        }
    }
}

- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error
{
    UITableView *tableView = (UITableView *)[self viewWithTag:TABLEVIEW_TAG];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_refreshIndex inSection:0]];
    CMWarnCellView *cellView = (CMWarnCellView *)[cell.contentView viewWithTag:1];
    [cellView refreshAddress:result.strAddr index:_refreshIndex];
    [addressList addObject:result.strAddr];
    
    _refreshIndex ++;
    
    if (_refreshIndex>=[_items count]) {
        return;
    }
    
    id gpsEntity = [_items objectAtIndex:_refreshIndex];
    
    CLLocation *location = nil;
    if ([gpsEntity isKindOfClass:[CMGPSEntity class]]) {
        CMGPSEntity *gEntity = (CMGPSEntity *)gpsEntity;
        location = [[CLLocation alloc] initWithLatitude:gEntity.lat longitude:gEntity.lon];
    }
    else if ([gpsEntity isKindOfClass:[VehicleReport class]]) {
        VehicleReport *item_ = (VehicleReport *)gpsEntity;
        BasicLocation *blocation = item_.location;
        
        location = [[CLLocation alloc] initWithLatitude:blocation.latitude longitude:blocation.longitude];
    }
    
    CLLocation *newLocation = [location locationMarsFromEarth];
    CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
    
    CLLocationCoordinate2D pt = (CLLocationCoordinate2D){baiduLocation.coordinate.latitude, baiduLocation.coordinate.longitude};
    BOOL flag = [_baiduSearch reverseGeocode:pt];
    if (flag) {
        NSLog(@"ReverseGeocode search success.");
        
    }
    else{
        NSLog(@"ReverseGeocode search failed!");
    }
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
    
    [_vInfo release];
    
    [_baiduSearch setDelegate:nil];
    [_baiduSearch release];_baiduSearch=nil;
}

#pragma mark UIButton
- (void)btnTapping:(id)sender {
    CGRect appFrame = [AppManager getAppFrame];
    [UIUitl toFrameAnimation:self toFrame:CGRectMake(0, appFrame.size.height, self.frame.size.width, self.frame.size.height)];
    
    [self setIsMinimize:YES];
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    int num = 1;
    
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = [_items count];
    
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float hig = 0;
    CMWarnCellView *cellView = [[CMWarnCellView alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
    if (!_isHis) {
        if (0 == indexPath.row) {
            [cellView setIsFirst:YES];
        }
        else if ([_items count]-1 == indexPath.row) {
            [cellView setIsEnd:YES];
        }
        else {
            [cellView setIsMid:YES];
        }
    }
    hig = [cellView setItem:[_items objectAtIndex:indexPath.row] index:indexPath.row];
    [cellView release];
    
    return hig;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return _topView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        CMWarnCellView *cellView = [[CMWarnCellView alloc] initWithFrame:CGRectMake(0, 0, 320, ROW_HIG)];
        [cellView setTag:1];
        [cell.contentView addSubview:cellView];
        [cellView release];
	}
    
    float hig = 0;
    CMWarnCellView *cellView = (CMWarnCellView *)[cell.contentView viewWithTag:1];
    [cellView setIsHis:_isHis];
    
    if (0 == indexPath.row) {
        [cellView setIsFirst:YES];
        
        hig = [cellView setItem:[_items objectAtIndex:indexPath.row] index:indexPath.row];
    }
    else if ([_items count]-1 == indexPath.row) {
        [cellView setIsEnd:YES];
        
        hig = [cellView setItem:[_items objectAtIndex:indexPath.row] index:indexPath.row];
    }
    else {
        [cellView setIsMid:YES];
        
        hig = [cellView setItem:[_items objectAtIndex:indexPath.row] index:indexPath.row];
    }
    [cellView setFrame:CGRectMake(cellView.frame.origin.x, cellView.frame.origin.y, cellView.frame.size.width, hig)];
    
    NSString *address = @"";
    if ([addressList count]>indexPath.row) {
        address = [addressList objectAtIndex:indexPath.row];
        [cellView refreshAddress:address index:indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VehicleReport *vrRet = nil;
    CLLocationCoordinate2D center;
    if (indexPath.row==0) {
        CMGPSEntity *gEntity = (CMGPSEntity *)[_items objectAtIndex:indexPath.row];
        
//        CLLocation *location = [[[CLLocation alloc] initWithLatitude:gEntity.lat longitude:gEntity.lon] autorelease];
//    
//        CLLocation *newLocation = [location locationMarsFromEarth];
//        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
    
        center = (CLLocationCoordinate2D){gEntity.lat, gEntity.lon};
        
        vrRet = (VehicleReport *)gEntity;
    }
    else if (indexPath.row == [_items count]-1) {
        CMGPSEntity *gEntity = (CMGPSEntity *)[_items objectAtIndex:indexPath.row];
        
//        CLLocation *location = [[[CLLocation alloc] initWithLatitude:gEntity.lat longitude:gEntity.lon] autorelease];
//        
//        CLLocation *newLocation = [location locationMarsFromEarth];
//        CLLocation *baiduLocation = [newLocation locationBaiduFromMars];
//        
        center = (CLLocationCoordinate2D){gEntity.lat, gEntity.lon};
        
        vrRet = (VehicleReport *)gEntity;
    }
    else {
        VehicleReport *vr = [_items objectAtIndex:indexPath.row];
        vrRet = vr;
        center = CLLocationCoordinate2DMake(vr.location.latitude, vr.location.longitude);
    }
    
    if ([self.delegate respondsToSelector:@selector(warnView:center:vehicleReport:)]) {
        [self.delegate warnView:self center:center vehicleReport:vrRet];
    }
    
    [self setIsMinimize:YES];
    CGRect appFrame = [AppManager getAppFrame];
    [UIUitl toFrameAnimation:self toFrame:CGRectMake(0, appFrame.size.height, self.frame.size.width, self.frame.size.height)];
    
    /*
    AppDelegate *dele = [AppManager getAppDelegate];
    
    CDTVMapsViewController *mapsViewController = [[CDTVMapsViewController alloc] initWithNibName:@"CDTVMapsViewController" bundle:nil];
    [dele.nav pushViewController:mapsViewController animated:YES];
    [mapsViewController release];
    
    if (indexPath.row==0) {
        CMGPSEntity *vr = (CMGPSEntity *)[_items objectAtIndex:indexPath.row];
        [mapsViewController setCenter:CLLocationCoordinate2DMake(vr.lat, vr.lon) title:@"" gpses:self.currentPolylinesGps];
    }
    else if (indexPath.row == [_items count]-1) {
        CMGPSEntity *vr = (CMGPSEntity *)[_items objectAtIndex:indexPath.row];
        [mapsViewController setCenter:CLLocationCoordinate2DMake(vr.lat, vr.lon) title:@"" gpses:self.currentPolylinesGps];
    }
    else {
        VehicleReport *vr = [_items objectAtIndex:indexPath.row];
        Alarm *alarm = vr.alarm;
        [mapsViewController setCenter:CLLocationCoordinate2DMake(vr.location.latitude, vr.location.longitude) title:alarm.message gpses:self.currentPolylinesGps];
    }
//    CMHistoryDescViewController *hisDescViewController = [[CMHistoryDescViewController alloc] init];
//    [dele.nav pushViewController:hisDescViewController animated:YES];
//    [hisDescViewController release];
//    
//    [hisDescViewController setItemAlarm:alarm vehicleInfo:_vInfo vehicleReport:vr];
     */
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
