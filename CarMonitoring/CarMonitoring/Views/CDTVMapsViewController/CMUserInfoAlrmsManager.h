//
//  CMUserInfoAlrmsManager.h
//  CarMonitoring
//
//  Created by Work on 1/18/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserInfo;
@class VehicleInfo;
@class Alarm;
@class VehicleReport;
@interface CMUserInfoAlrmsManager : NSObject

@property (nonatomic,retain) UserInfo *userInfo;
+ (CMUserInfoAlrmsManager *)getInstance;

- (NSArray *)getWarnList:(int32_t)vehicle_id;
- (NSArray *)getWarnAllList;

- (NSArray *)searchCarWarnList:(NSString *)searchQ_ items:(NSArray *)items_ ;

- (VehicleInfo *)objectVehicleInfoFromAlarm:(Alarm *)alarm items_:(NSArray *)items_ ;
- (VehicleReport *)objectVehicleReportFromAlarm:(Alarm *)alarm items_:(NSArray *)items_ ;

@end
