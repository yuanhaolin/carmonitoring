//
//  CMMyInfoCellView.h
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMMyInfoCellView : UIView

@property (nonatomic,assign) IBOutlet UIImageView *bgImageView;
@property (nonatomic,assign) IBOutlet UILabel *labTitle1;
@property (nonatomic,assign) IBOutlet UILabel *labTitle2;
@property (nonatomic,assign) IBOutlet UIImageView *imgIconView;

@end
