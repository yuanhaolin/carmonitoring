//
//  CMMyInfoCellView.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMMyInfoCellView.h"

@implementation CMMyInfoCellView
@synthesize bgImageView;
@synthesize labTitle1;
@synthesize labTitle2;
@synthesize imgIconView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CMMyInfoCellView" owner:self options:nil] lastObject];
        [self addSubview:view];
        
        [self.labTitle1 setFont:WFONT(15)];
        [self.labTitle1 setTextColor:[UIColor whiteColor]];
        
        [self.labTitle2 setFont:WFONT(15)];
        [self.labTitle2 setTextColor:UIColorFromRGB(0x3357a0)];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
