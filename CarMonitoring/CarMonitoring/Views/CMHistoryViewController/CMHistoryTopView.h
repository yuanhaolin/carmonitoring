//
//  CMHistoryTopView.h
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMHistoryTopView : UIView

@property (nonatomic,assign) IBOutlet UIButton *btnTap;
@property (nonatomic,assign) IBOutlet UITextField *textField;

@end
