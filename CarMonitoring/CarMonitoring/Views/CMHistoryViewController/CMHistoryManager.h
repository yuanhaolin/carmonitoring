//
//  CMHistoryManager.h
//  CarMonitoring
//
//  Created by Work on 1/24/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMHistoryManager : NSObject
+ (CMHistoryManager *)getInstance;

- (NSArray *)searchCarWarnList:(NSString *)searchQ_;

@end
