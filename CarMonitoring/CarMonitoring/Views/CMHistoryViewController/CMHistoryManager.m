//
//  CMHistoryManager.m
//  CarMonitoring
//
//  Created by Work on 1/24/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMHistoryManager.h"
#import "CMMyUserInfoManager.h"

static CMHistoryManager *_pHistoryInfoManager;
@implementation CMHistoryManager

+ (id)getInstance {
    @synchronized (self) {
        if (nil == _pHistoryInfoManager) {
            _pHistoryInfoManager = [[CMHistoryManager alloc] init];
        }
    }
    
    return _pHistoryInfoManager;
}

- (NSArray *)searchCarWarnList:(NSString *)searchQ_ {
    NSMutableArray *alarmSearchResults = [NSMutableArray array];
    
    UserInfo_Builder *uib = [[CMMyUserInfoManager getInstance] getUserInfo];
    for (VehicleInfo *vi in uib.vehicles) {
        //获取每辆车
        PBArray *alrams = vi.processingAlarms;//获取每辆车的警告
        for (Alarm *a in alrams) {
            if (0 == [searchQ_ length]) {
                [alarmSearchResults addObject:a];
            }
            else {
                NSMutableString *tagsString = [NSMutableString string];
                for (NSString *tag in a.tags) {
                    [tagsString appendString:tag];
                }
                
                if (0 != [tagsString rangeOfString:searchQ_].length) {
                    [alarmSearchResults addObject:a];
                }
            }
        }
    }
    
    return alarmSearchResults;
}

@end
