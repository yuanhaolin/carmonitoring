//
//  CMHistoryTopView.m
//  CarMonitoring
//
//  Created by Work on 1/5/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMHistoryTopView.h"

@implementation CMHistoryTopView
@synthesize btnTap;
@synthesize textField;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"CMHistoryTopView" owner:self options:nil] lastObject];
        [self addSubview:view];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
