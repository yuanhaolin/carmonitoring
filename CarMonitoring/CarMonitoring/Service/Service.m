//
//  Service.m
//  Weather
//
//  Created by yuan haolin on 11-7-10.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

//#define USER_AGENT @"android"

#import "Service.h"
#import "AppDelegate.h"
#import "AppManager.h"
#import "CDTVNavigationController.h"
#import <stdlib.h>
#import "Parser.h"

#import "MCCGPSObject.h"
#import "CMMyUserInfoManager.h"
#import "CMUserInfoAlrmsManager.h"

@interface Service () {
    NSURLConnection *aSynConnection;
    NSMutableData *returnInfoData;
    
    NSMutableData *scoketData;
    
    int _index;
    
    NSTimer *_timerReadData;
}

@end

@interface Service(CreateRequest)
- (ASIFormDataRequest *)createRequest:(NSString *)url_ tag:(int)tag;
- (ASIFormDataRequest *)createRequestWithImage:(NSString *)url_ tag:(int)tag;
- (ASIFormDataRequest *)createSendPostJsonRequestNULLHeader:(NSString *)url_ tag:(int)tag;
- (NSData *)makeData:(char *)typename protocbufData:(NSData *)protocbufData;
- (NSData *)parserData:(char *)typename responesData:(NSData *)responesData;

@end

@implementation Service(CreateRequest)

const int MOD_ADLER = 65521;

uint32_t adler32(unsigned char *data, int32_t len) /* where data is the location of the data in physical memory and
                                                    len is the length of the data in bytes */
{
    uint32_t a = 1, b = 0;
    int32_t index;
    
    /* Process each byte of the data in order */
    for (index = 0; index < len; ++index)
    {
        a = (a + data[index]) % MOD_ADLER;
        b = (b + a) % MOD_ADLER;
    }
    
    return (b << 16) | a;
}

- (NSData *)itoBig:(int )i_ {
    int32_t unswapped = i_;
    int32_t swapped = CFSwapInt32HostToBig(unswapped);
    char* a2 = (char*) &swapped;
    
    NSData *data = [NSData dataWithBytes:a2 length:sizeof(int)];
    
    return data;
}

- (int)bigtoI:(NSData *)data {
    uint32_t i = 0;
    [data getBytes:&i length:4];
    
    int32_t swapped = CFSwapInt32BigToHost(i);
    
    return swapped;
}

- (NSString *)urlEncodeValue:(NSString *)str
{
    NSString *result = (NSString *) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, NULL, kCFStringEncodingUTF8);
    return [result autorelease];
}

- (ASIFormDataRequest *)createRequestWithImage:(NSString *)url_ tag:(int)tag {
	NSString *url = [self urlEncodeValue:url_];
    NSURL *urlUrl = [NSURL URLWithString:url];
    NSString *host = [urlUrl host];
    
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:url]];
    if (nil != host) {
        [request addRequestHeader:@"Host" value:host];
        [request addRequestHeader:@"Refresh" value:host];
    }
    [request setRequestMethod:@"GET"];
    [request addRequestHeader:@"User-Agent" value:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0"];
	[request addRequestHeader:@"Accept" value:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"];
	[request addRequestHeader:@"Accept-Encoding" value:@"gzip, deflate"];
	[request addRequestHeader:@"Accept-Language" value:@"en-US,en;q=0.5"];

	NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d",tag] forKey:@"t_tag"];
	[request setUserInfo:userInfo];
	
	[request setTimeOutSeconds:TIMEOUT];
	[_downQueue addOperation:request];
    
    NSLog(@"%@     tag:%d",url_,tag);
	
	return [request autorelease];
}

- (ASIFormDataRequest *)createRequest:(NSString *)url_ tag:(int)tag {
    [self resetDownQueue];
    
	NSString *url = [self urlEncodeValue:url_];
    
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setRequestMethod:@"GET"];
    [request addRequestHeader:@"Accept" value:@"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"];
    [request addRequestHeader:@"Accept-Encoding" value:@"gzip,deflate,sdch"];
    [request addRequestHeader:@"Accept-Language" value:@"zh-CN,zh;q=0.8"];
    [request addRequestHeader:@"Cache-Control" value:@"max-age=0"];
    [request addRequestHeader:@"Connection" value:@"keep-alive"];
    [request addRequestHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [request addRequestHeader:@"Host" value:@"111.9.33.106:4443"];
    [request addRequestHeader:@"Origin" value:@"https://111.9.33.106:4443"];
    [request addRequestHeader:@"Referer" value:@"https://111.9.33.106:4443/login"];
    [request addRequestHeader:@"User-Agent" value:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36"];
	
	NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d",tag] forKey:@"t_tag"];
	[request setUserInfo:userInfo];
	
	[request setTimeOutSeconds:TIMEOUT];
	[_downQueue addOperation:request];
    
    NSLog(@"%@     tag:%d",url_,tag);
	
	return [request autorelease];
}

- (ASIFormDataRequest *)createSendPostJsonRequest:(NSString *)url_ tag:(int)tag {
    [self resetDownQueue];
    
    NSString *url = [self urlEncodeValue:url_];
    
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Accept" value:@"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"];
    [request addRequestHeader:@"Accept-Encoding" value:@"gzip,deflate,sdch"];
    [request addRequestHeader:@"Accept-Language" value:@"zh-CN,zh;q=0.8"];
    [request addRequestHeader:@"Cache-Control" value:@"max-age=0"];
    [request addRequestHeader:@"Connection" value:@"keep-alive"];
    [request addRequestHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [request addRequestHeader:@"Host" value:@"111.9.33.106:4443"];
    [request addRequestHeader:@"Origin" value:@"https://111.9.33.106:4443"];
    [request addRequestHeader:@"Referer" value:@"https://111.9.33.106:4443/login"];
    [request addRequestHeader:@"User-Agent" value:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36"];
    
	NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d",tag] forKey:@"t_tag"];
	[request setUserInfo:userInfo];
	
	[request setTimeOutSeconds:TIMEOUT];
	[_downQueue addOperation:request];
    
    NSLog(@"%@     tag:%d",url_,tag);
	
	return [request autorelease];
}

- (ASIFormDataRequest *)createSendPostJsonRequestNULLHeader:(NSString *)url_ tag:(int)tag {
    [self resetDownQueue];
    
	NSString *url = [self urlEncodeValue:url_];
    NSURL *urlUrl = [NSURL URLWithString:url];
    NSString *host = [urlUrl host];
    
	ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setRequestMethod:@"POST"];
    if ([host length]>0) {
        [request addRequestHeader:@"Host" value:host];
    }
    [request addRequestHeader:@"Refresh" value:url];
    
	NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d",tag] forKey:@"t_tag"];
	[request setUserInfo:userInfo];
	
	[request setTimeOutSeconds:TIMEOUT];
	[_downQueue addOperation:request];
    
    NSLog(@"%@     tag:%d",url_,tag);
	
	return [request autorelease];
}

- (NSData *)makeData:(char *)typename protocbufData:(NSData *)protocbufData {
    NSMutableData *newData = [NSMutableData data];
    NSMutableData *msgData = [NSMutableData data];
    
    NSLog(@"send scoket:%s",typename);
    
    int nLength = strlen(typename);//消息码长度
    
    //    char* newD = (char *)malloc(allLength+4);
    //    if(NULL != newD) {
    //        memset(newD, 0, allLength+4);
    //    }
    
    NSData *a2 = [self itoBig:nLength];
    
    //    memcpy(newD+pos, a2, sizeof(a2));
    //    pos+=sizeof(a2);
    
    [msgData appendData:a2];
    
    [msgData appendData:[NSData dataWithBytes:typename length:nLength]];
    
    [msgData appendData:protocbufData];
    
    uint32_t adler = adler32((unsigned char *)[msgData bytes], [msgData length]);
    
    NSData* a3 = [self itoBig:adler];
    
    
    NSData* a1 = [self itoBig:[msgData length]+4];
    [newData appendData:a1];
    
    [newData appendData:msgData];
    
    [newData appendData:a3];
    
    return newData;
}

- (NSData *)parserData:(char *)typename responesData:(NSData *)responesData {
    int typenamelen = strlen(typename);
    int headLen = 4+4+typenamelen;
    NSData *newData = [responesData subdataWithRange:NSMakeRange(headLen, [responesData length]-headLen-4)];
    
    return newData;
}

@end

@implementation Service
@synthesize delegate=_delegate;
@synthesize downQueue=_downQueue;

- (id) init
{
	self = [super init];
	if (self != nil) {
		[self resetDownQueue];
        
        scoketData = [NSMutableData new];
	}
	return self;
}

- (void) dealloc
{
	[_downQueue cancelAllOperations];
	[_downQueue reset];
	[_downQueue release];
	[_delegate release];
    
    [_socket disconnect];
    [_socket release];_socket = nil;
    
    [scoketData release];scoketData=nil;
    
	[super dealloc];
}

- (void)resetDownQueue {
	[_downQueue cancelAllOperations];
    [_downQueue reset];
	[_downQueue release];_downQueue=nil;
	
	_downQueue = [ASINetworkQueue new];
	[_downQueue setDelegate:self];
	[_downQueue setRequestDidFinishSelector:NSSelectorFromString(@"requestDone:")];
	[_downQueue setRequestDidFailSelector:NSSelectorFromString(@"requestFaild:")];
    
    [aSynConnection cancel];[aSynConnection release];
    [returnInfoData release];returnInfoData =nil;
    
    [_socket disconnect];
    [_socket release];_socket = nil;
}

- (void)scoketConn {
    [_socket disconnect];
    [_socket release];_socket = nil;
    _socket = [[AsyncSocket alloc] initWithDelegate:self];
    
    NSString *ipStringPath = doc_cache_path(@"ipstringPath");
    NSString *ipstring = [NSString stringWithContentsOfFile:ipStringPath encoding:NSUTF8StringEncoding error:nil];
    
    NSArray *array = [ipstring componentsSeparatedByString:@":"];
    NSString *ip = [array objectAtIndex:0];
    NSString *host = [array objectAtIndex:1];
    
    NSError*error = nil;
    if(![_socket connectToHost:ip onPort:[host intValue] error:&error]) {
        NSLog(@"is aready connected");
    }
    NSLog(@"");
    
    [_socket readDataWithTimeout:-1 tag:0];
    
    _timerReadData = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:NSSelectorFromString(@"timerReadData:") userInfo:nil repeats:YES];
}

- (void)scoketDisconn {
    [scoketData setLength:0];
    [_socket disconnect];
    
    [_timerReadData invalidate];_timerReadData=nil;
}

- (void)clearData {
    [scoketData setLength:0];
    _index = 0;
}

- (void)timerReadData:(id)sender {
//    [self parserChangeData];
    
    [_socket readDataWithTimeout:-1 tag:0];
    NSLog(@"");
}

- (void)alertMsg:(NSString *)msg {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

#pragma mark interface
- (void)getImage:(NSString *)url delegate:(id)delegate_ {
    //有些地方出现了//路径，造成图片无法读取，代码修复
    NSString *domain = [[NSURL URLWithString:url] host];
    if (nil != domain) {
        NSMutableString *newUrl = [NSMutableString stringWithString:url];
        [newUrl replaceOccurrencesOfString:[NSString stringWithFormat:@"http://%@",domain] withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [newUrl length])];
        [newUrl replaceOccurrencesOfString:@"//" withString:@"/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [newUrl length])];
        url = [NSString stringWithFormat:@"http://%@%@",domain,newUrl];
    }
    
    NSMutableString *fileName = [NSMutableString stringWithString:[self urlEncodeValue:url]];
    [fileName replaceOccurrencesOfString:@"http://" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [fileName length])];
    [fileName replaceOccurrencesOfString:@"/" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [fileName length])];
    
	NSString *path = doc_cache_path(fileName);
    
    NSArray *tmpNames = [url componentsSeparatedByString:@"/"];
    NSString *name = [tmpNames objectAtIndex:[tmpNames count]-1];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:path]) {
		NSData *imageData = [NSData dataWithContentsOfFile:path];
		
		if ([_delegate respondsToSelector:@selector(service:tag:typename:entity:userInfo:)]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            [dic setValue:fileName forKey:@"fileName"];
            [dic setValue:name forKey:@"name"];
			[_delegate service:self tag:GET_IMAGE_TAG typename:nil entity:imageData userInfo:dic];
		}
	}
	else {
		ASIFormDataRequest *request = [self createRequestWithImage:url
                                                               tag:GET_IMAGE_TAG];
        [request setTimeOutSeconds:60*2];
        [request setDownloadDestinationPath:path];
        [request setShowAccurateProgress:YES];
        [request setDownloadProgressDelegate:delegate_];
		[request.userInfo setValue:fileName forKey:@"fileName"];
        [request.userInfo setValue:name forKey:@"name"];
	}
}

//下载图片地址
- (NSString *)getImageWithLocal:(NSString *)url {
    NSString *domain = [[NSURL URLWithString:url] host];
    if (nil != domain) {
        NSMutableString *newUrl = [NSMutableString stringWithString:url];
        [newUrl replaceOccurrencesOfString:[NSString stringWithFormat:@"http://%@",domain] withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [newUrl length])];
        [newUrl replaceOccurrencesOfString:@"//" withString:@"/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [newUrl length])];
        url = [NSString stringWithFormat:@"http://%@%@",domain,newUrl];
    }
    
    NSMutableString *fileName = [NSMutableString stringWithString:[self urlEncodeValue:url]];
    [fileName replaceOccurrencesOfString:@"http://" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [fileName length])];
    [fileName replaceOccurrencesOfString:@"/" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [fileName length])];
	NSString *path = doc_cache_path(fileName);
    
    return path;
}

- (void)go {
    [_downQueue go];
}

//获取登录html代码 GET_LOGINHTML_TAG
- (void)getLoginHTML {
    NSString *url = [NSString stringWithFormat:@"%@/login",SERVICE_URL];
    ASIFormDataRequest *request = [self createRequest:url tag:GET_LOGINHTML_TAG];
    [request setValidatesSecureCertificate:NO];
}

//登录 SEND_LOGINHTTPS_TAG
- (void)sendLoginHTTPS:(NSString *)user password:(NSString *)password {
    NSString *url = [NSString stringWithFormat:@"%@/login",SERVICE_URL];
    ASIFormDataRequest *request = [self createSendPostJsonRequest:url tag:SEND_LOGINHTTPS_TAG];
    [request setValidatesSecureCertificate:NO];
    
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    [dic setObject:[self urlEncodeValue:user] forKey:@"username"];
//    [dic setObject:[self urlEncodeValue:password] forKey:@"password"];
//    [dic setObject:tickid forKey:@"lt"];
//    [dic setObject:@"e2s1" forKey:@"execution"];
//    
//    NSMutableData *bodyData = nil;
//    [Parser dic2Data:dic request:request outData:&bodyData FileType:0];
//    NSString *str = [NSString stringWithFormat:@"username=lxtapp%%40gnssgis.com&password=lxtapp%%40gnssgis.com&lt=%@&execution=e2s1&_eventId=submit&submit=%%E7%%99%%BB%%E5%%BD%%95",tickid];
//    
//    NSData *bodyData = [str dataUsingEncoding:NSUTF8StringEncoding];
    
//    [request setPostBody:bodyData];
    
    NSString *pathTickid = doc_cache_path(@"ticketPath");
    NSString *tickid = [NSString stringWithContentsOfFile:pathTickid encoding:NSUTF8StringEncoding error:nil];
    
    [request setPostValue:[self urlEncodeValue:user] forKey:@"username"];
    [request setPostValue:[self urlEncodeValue:password] forKey:@"password"];
    [request setPostValue:tickid forKey:@"lt"];
    [request setPostValue:@"e2s1" forKey:@"execution"];
}

//注销 SEND_LOGOUTHTTPS_TAG
- (void)sendLogoutHTTPS {
    NSString *url = [NSString stringWithFormat:@"%@/logout",SERVICE_URL];
    ASIFormDataRequest *request = [self createRequest:url tag:SEND_LOGOUTHTTPS_TAG];
    [request setValidatesSecureCertificate:NO];
    
//    NSString *pathTickid = doc_cache_path(@"ticketPath");
//    NSString *tickid = [NSString stringWithContentsOfFile:pathTickid encoding:NSUTF8StringEncoding error:nil];
//    
//    [request setPostValue:tickid forKey:@"lt"];
//    [request setPostValue:@"e2s1" forKey:@"execution"];
}

//登录 SEND_LOGIN_TAG
- (void)sendLogin:(NSString *)user password:(NSString *)password {
    NSString *pathTickid = doc_cache_path(@"ticketPath");
    NSString *tickid = [NSString stringWithContentsOfFile:pathTickid encoding:NSUTF8StringEncoding error:nil];
    
    NSString *pathUser = doc_cache_path(@"usernamePath");
    [user writeToFile:pathUser atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    UserLogin_Builder *userLogin = [UserLogin builder];
    [userLogin setUserName:user];
    [userLogin setPassword:password];
    NSString *versionSystem = [[UIDevice currentDevice] systemVersion];
    [userLogin setUserAgent:[NSString stringWithFormat:@"iphone %@",versionSystem]];
    [userLogin setTicket:tickid];
    
    UserLogin *ul = [userLogin buildPartial];
    NSData *d = [ul data];
    
    NSData *newData = [self makeData:"UserLogin" protocbufData:d];
    
    //    NSMutableData *newData = [NSMutableData data];
    //    NSMutableData *msgData = [NSMutableData data];
    //
    //    char *typename = "UserLogin";
    //    int nLength = strlen(typename);//消息码长度
    //
    ////    char* newD = (char *)malloc(allLength+4);
    ////    if(NULL != newD) {
    ////        memset(newD, 0, allLength+4);
    ////    }
    //
    //    NSData *a2 = [self itoBig:nLength];
    //
    ////    memcpy(newD+pos, a2, sizeof(a2));
    ////    pos+=sizeof(a2);
    //
    //    [msgData appendData:a2];
    //
    //    [msgData appendData:[NSData dataWithBytes:typename length:nLength]];
    //
    //    [msgData appendData:d];
    //
    //    uint32_t adler = adler32((unsigned char *)[msgData bytes], [msgData length]);
    //
    //    NSData* a3 = [self itoBig:adler];
    //
    //
    //    NSData* a1 = [self itoBig:[msgData length]+4];
    //    [newData appendData:a1];
    //
    //    [newData appendData:msgData];
    //
    //    [newData appendData:a3];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:SEND_LOGIN_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

//注销登录 SEND_LOGOUT_TAG
- (void)sendLogout {
    NSString *pathUser = doc_cache_path(@"usernamePath");
    NSString *user = [NSString stringWithContentsOfFile:pathUser encoding:NSUTF8StringEncoding error:nil];
    
    NSString *pathTickid = doc_cache_path(@"ticketPath");
    NSString *tickid = [NSString stringWithContentsOfFile:pathTickid encoding:NSUTF8StringEncoding error:nil];
    
    UserLogout_Builder *userLogout = [UserLogout builder];
    [userLogout setUserName:user];
    [userLogout setTicket:tickid];
    
    UserLogout *ul = [userLogout buildPartial];
    NSData *d = [ul data];
    
    NSData *newData = [self makeData:"UserLogout" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:SEND_LOGOUT_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

- (void)disconn {
    [_socket disconnect];
}

//获取用户信息 GET_MYUSRINFO_TAG
- (void)getMyUserInfo {
    GetUserInfo_Builder *userInfo = [GetUserInfo builder];
    
    GetUserInfo *gui = [userInfo buildPartial];
    NSData *d = [gui data];
    
    NSData *newData = [self makeData:"GetUserInfo" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:GET_MYUSRINFO_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

//修改用户资料 SEND_MODIFYMYUSRINFO_TAG
- (void)sendModifyMyUserInfo:(UserInfo_Builder *)myuserInfo {
    UserInfo *uinfo = [myuserInfo buildPartial];
    NSData *d = [uinfo data];
    
    NSData *newData = [self makeData:"UserInfo" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:SEND_MODIFYMYUSRINFO_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

//查看/取消查看实时地图 GET_MONITORVEHICLE_TAG
- (void)getMonitorVehicle:(long long)vehicle_id {
    MonitorVehicle_Builder *mb = [MonitorVehicle_Builder new];
    [mb setVehicleId:(int)vehicle_id];
    [mb setEnableMonitor:YES];
    
    MonitorVehicle *mv = [mb buildPartial];
    NSData *d = [mv data];
    
    NSData *newData = [self makeData:"MonitorVehicle" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:GET_MONITORVEHICLE_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

//查看/取消查看实时地图 GET_CENCELMONITORVEHICLE_TAG
- (void)cencelMonitorVehicle:(long long)vehicle_id {
    MonitorVehicle_Builder *mb = [MonitorVehicle_Builder new];
    [mb setVehicleId:(int)vehicle_id];
    [mb setEnableMonitor:NO];
    
    MonitorVehicle *mv = [mb buildPartial];
    NSData *d = [mv data];
    
    NSData *newData = [self makeData:"MonitorVehicle" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:GET_MONITORVEHICLE_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

//查看历史轨迹 GET_QUERYHISTORY_TAG
- (void)getQueryHistory:(QueryHistory_Builder *)queryHistory {
    QueryHistory *qh = [queryHistory buildPartial];
    NSData *d = [qh data];
    
    NSData *newData = [self makeData:"QueryHistory" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:GET_QUERYHISTORY_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

//是否关闭程序 GET_CLOSE_TAG
- (void)getIsClose {
    NSString *url = [NSString stringWithFormat:@"http://api.map.baidu.com/geosearch/v3/nearby?ak=6263333E9AB0db6eefa4c9245bcecc93&geotable_id=54892&q=&location=116.410732,39.919771&radius=100000&qq-pf-to=pcqq.c2c"];
    ASIFormDataRequest *request = [self createRequest:url tag:GET_CLOSE_TAG];
    [request setValidatesSecureCertificate:NO];
    
}

//查看报警信息 GET_ALARM_TAG
- (void)getAlarm:(long long)vehicle_id alarm_id:(long long)alarm_id {
    AlarmResponse_Builder *arb = [AlarmResponse_Builder new];
    [arb setVehicleId:(int)vehicle_id];
    [arb setAlarmId:(int)alarm_id];
    
    AlarmResponse *ar = [arb buildPartial];
    
    NSData *d = [ar data];
    
    NSData *newData = [self makeData:"AlarmResponse" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:GET_ALARM_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

//修改车辆 SEND_MODIFYCAR_TAG
- (void)sendModifyCar:(VehicleInfo *)vi {
    NSData *d = [vi data];
    
    NSData *newData = [self makeData:"VehicleInfo" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:SEND_MODIFYCAR_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

//修改密码 SEND_MODIFYPASS_TAG
- (void)sendModifyPassword:(ChangePassword *)cp {
    NSData *d = [cp data];
    
    NSData *newData = [self makeData:"ChangePassword" protocbufData:d];
    
    [_socket writeData:newData withTimeout:TIMEOUT tag:SEND_MODIFYPASS_TAG];
//    [_socket readDataWithTimeout:-1 tag:0];
}

- (void)readDataRefresh {
    [_socket readDataWithTimeout:-1 tag:0];
}

#pragma mark NSURLConnection delegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    int tag = [[[connection currentRequest] valueForHTTPHeaderField:@"t_tag"] intValue];
    
    NSLog(@"NSURLConnection:::%d",tag);
    [returnInfoData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    int tag = [[[connection currentRequest] valueForHTTPHeaderField:@"t_tag"] intValue];
    
    if ([_delegate respondsToSelector:@selector(serviceFaild:tag:typename:entity:userInfo:)]) {
		[_delegate serviceFaild:self tag:tag typename:nil entity:nil userInfo:nil];
	}
    
    [aSynConnection cancel];
    [aSynConnection release];aSynConnection=nil;
    [returnInfoData release];returnInfoData = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if( [connection isEqual: aSynConnection])
    {
        int tag = [[[connection currentRequest] valueForHTTPHeaderField:@"t_tag"] intValue];
        
        if ([_delegate respondsToSelector:@selector(service:tag:typename:entity:userInfo:)]) {
            [_delegate service:self tag:tag typename:nil entity:nil userInfo:nil];
        }
    }
}

- (void)parserChangeData:(NSData *)data {
    id entity = nil;
//    int tag = 0;
    
    [scoketData appendData:data];
    
    if (_index+4>=[scoketData length]) {
        return;
    }
    
    int len = [self bigtoI:[scoketData subdataWithRange:NSMakeRange(_index, 4)]];
    if (len <0 || isnan(len)) {
//        [scoketData setLength:0];
//        _index = 0;
    }
    if (len+4+_index>[scoketData length]) {
//        [scoketData setLength:0];
        return;
    }
    _index+=4;
    
    if (_index+4>=[scoketData length]) {
        return;
    }
    int typenameLen = [self bigtoI:[scoketData subdataWithRange:NSMakeRange(_index, 4)]];
    _index+=4;
    
    if (_index+typenameLen>=[scoketData length]) {
        return;
    }
    NSString *typename = [[NSString alloc] initWithData:[scoketData subdataWithRange:NSMakeRange(_index, typenameLen)] encoding:NSUTF8StringEncoding];
    _index+=[typename length];
    
    NSLog(@"read_typename:%@",typename);
    
    int protobufLen = len-4-4-[typename length];
    if (_index+protobufLen>=[scoketData length]) {
        return;
    }
    NSData *protobufData = [scoketData subdataWithRange:NSMakeRange(_index, protobufLen)];
    _index+=[protobufData length];
    
    _index+=4;
    if (_index >= [scoketData length]) {
        [scoketData setLength:0];
        _index = 0;
    }
//    if (len <0 || isnan(len)) {
//        [scoketData setLength:0];
//        _index = 0;
//    }
    
    if ([typename isEqualToString:@"RequestResponse"]) {
        RequestResponse *protobuf = [RequestResponse parseFromData:protobufData];
        
        if (RequestResponse_ResponseTypeSuccess == protobuf.type) {
            entity = protobuf;
        }
        
//        tag = SEND_LOGIN_TAG;
    }
    else if ([typename isEqualToString:@"UserInfo"]) {
        entity = protobufData;
        [[CMMyUserInfoManager getInstance] setUserInfo:protobufData];
        
        UserInfo *protobuf = [UserInfo parseFromData:protobufData];
        [(CMUserInfoAlrmsManager *)[CMUserInfoAlrmsManager getInstance] setUserInfo:protobuf];
        
//        tag = GET_MYUSRINFO_TAG;
    }
    else if ([typename isEqualToString:@"VehicleReport"]) {
        VehicleReport *vr = [VehicleReport parseFromData:protobufData];
        
        entity = vr;
        
//        tag = GET_MONITORVEHICLE_TAG;
    }
    else if ([typename isEqualToString:@"History"]) {
        History *h = [History parseFromData:protobufData];
        
        entity = h;
    }
    else if ([typename isEqualToString:@"SimpleHistory"]) {
        SimpleHistory *sh = [SimpleHistory parseFromData:protobufData];
        
        entity = sh;
        
//        tag = GET_QUERYHISTORY_TAG;
    }
    else if ([typename isEqualToString:@"AlarmResponse"]) {
        NSLog(@"abc");
    }
    
    if ([entity isKindOfClass:[RequestResponse class]]) {
        if (RequestResponse_ResponseTypeSuccess == [(RequestResponse *)entity type]) {
            if ([_delegate respondsToSelector:@selector(service:tag:typename:entity:userInfo:)]) {
                [_delegate service:self tag:0 typename:typename entity:entity userInfo:nil];
            }
        }
    }
    else {
        if ([_delegate respondsToSelector:@selector(service:tag:typename:entity:userInfo:)]) {
            [_delegate service:self tag:0 typename:typename entity:entity userInfo:nil];
        }
    }
    [scoketData setLength:0];
    _index = 0;
    
//    if (0 != _index) {
//        [self parserChangeData];
//    }
}

- (void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    NSLog(@"didWriteDataWithTag:%ld",tag);
}

#pragma mark GCDAsyncSocket  delegate
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port {
    NSLog(@"connected to the server");
}

- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    NSLog(@"onSocket:didReadData");
    
    [self parserChangeData:data];
    
//    [scoketData appendData:data];
    
//    NSMutableData *newdata = [NSMutableData data];
    
//    [_socket readDataWithTimeout:-1 tag:0];
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock {
    [self scoketDisconn];
    
    if ([_delegate respondsToSelector:@selector(serviceFaild:tag:typename:entity:userInfo:)]) {
		[_delegate serviceFaild:self tag:-1 typename:nil entity:nil userInfo:nil];
	}
    
    AppDelegate *dele = [AppManager getAppDelegate];
    [dele.nav popToRootViewControllerAnimated:YES];
    
    [self resetDownQueue];
    [self sendLogoutHTTPS];
    [self go];
}

#pragma mark ASI Request
-(void)requestFaild:(ASIHTTPRequest *)request
{
	int tag = [[[request userInfo] objectForKey:@"t_tag"] intValue];
	NSLog(@"%@---tag: %d",[[request error] description],tag);
	
    if ([_delegate respondsToSelector:@selector(serviceFaild:tag:typename:entity:userInfo:)]) {
		[_delegate serviceFaild:self tag:tag typename:nil entity:nil userInfo:nil];
	}
}

- (void)requestDone:(ASIHTTPRequest *)request
{
	int tag = [[[request userInfo] objectForKey:@"t_tag"] intValue];
	
	id entity = nil;
    
    int httpCode = [request responseStatusCode];
	
    if (200 > httpCode || 300 <= httpCode) {
        return;
    }
    
	switch (tag) {
        case GET_IMAGE_TAG:
        {
            if (200 != httpCode) {
                return;
            }
            NSString *fileName = [request.userInfo objectForKey:@"fileName"];
            NSString *path = doc_cache_path(fileName);
            
            NSData *data = [NSData dataWithContentsOfFile:path];
			entity = data;
        }
            break;
        case SEND_LOGINHTTPS_TAG:
        {
            NSString *str = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
            NSRange rang = [str rangeOfString:@"成功" options:NSCaseInsensitiveSearch];
            if (rang.length>0) {
                entity = str;
                
                NSString *ipStrOne = @"<p>服务器地址是<span>";
                NSRange rangeIpOne = [str rangeOfString:ipStrOne options:NSCaseInsensitiveSearch];
                
                NSString *newHtml = [str substringFromIndex:rangeIpOne.location+rangeIpOne.length];
                NSRange rangeIpTwo = [newHtml rangeOfString:@"</span></p>" options:NSCaseInsensitiveSearch];
                
                NSString *ipStringPath = doc_cache_path(@"ipstringPath");
                NSString *ipstring = [newHtml substringWithRange:NSMakeRange(0, rangeIpTwo.location)];
                [ipstring writeToFile:ipStringPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
            }
        }
            break;
        case GET_CLOSE_TAG:
        {
            NSDictionary *dics = nil;
            [Parser parserJson2Dic:[request responseString] outEntity:&dics];
            
            NSArray *array = [dics objectForKey:@"contents"];
            NSDictionary *dic = [array objectAtIndex:0];
            
            if (![[dic objectForKey:@"isopen"] isEqualToString:@"YES"]) {
                @throw [NSException exceptionWithName:@"该给钱了呀" reason:@"" userInfo:nil];
            }
        }
            break;
        case SEND_LOGOUTHTTPS_TAG:
        {
            
        }
            break;
        case GET_LOGINHTML_TAG:
        {
            NSString *html = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
            
            if (0 == [html rangeOfString:@"登录成功"].length) {
                NSString *strOne = @"<input name=\"lt\" type=\"hidden\" value=\"";
                NSRange range = [html rangeOfString:strOne options:NSCaseInsensitiveSearch];
                NSString *ticket = [html substringWithRange:NSMakeRange(range.location+range.length, 32)];
                
                NSString *path = doc_cache_path(@"ticketPath");
                [ticket writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
                
                entity = ticket;
            }
            else {
                NSString *pathTickid = doc_cache_path(@"ticketPath");
                NSString *tickid = [NSString stringWithContentsOfFile:pathTickid encoding:NSUTF8StringEncoding error:nil];
                
                entity = tickid;
                
                tag = SEND_LOGINHTTPS_TAG;
            }
        }
            break;
        default:
        {
            
        }
            break;
	}
	
	if ([_delegate respondsToSelector:@selector(service:tag:typename:entity:userInfo:)]) {
		[_delegate service:self tag:tag typename:nil entity:entity userInfo:[request userInfo]];
	}
}

@end
