//
//  Service.h
//  Weather
//
//  Created by yuan haolin on 11-7-10.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define GET_IMAGE_TAG		101
#define SEND_LOGIN_TAG      102
#define GET_MYUSRINFO_TAG   103
#define SEND_LOGINHTTPS_TAG 106
#define GET_LOGINHTML_TAG   104
#define SEND_MODIFYMYUSRINFO_TAG    105
#define GET_MONITORVEHICLE_TAG 107
#define GET_CENCELMONITORVEHICLE_TAG    108
#define GET_QUERYHISTORY_TAG        109
#define GET_ALARM_TAG       110
#define SEND_MODIFYCAR_TAG  111
#define SEND_MODIFYPASS_TAG 112
#define SEND_LOGOUT_TAG     113
#define SEND_LOGOUTHTTPS_TAG 114
#define GET_CLOSE_TAG       115

#import "ASINetworkQueue.h"
#import "ASIFormDataRequest.h"
#import "AsyncSocket.h"
#import "Cm.pb.h"

@protocol ServiceDelegate
- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo;
- (void)serviceFaild:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo;

@end

@class STUserEntity;
@class STNoteEntity;
@interface Service : NSObject {
	ASINetworkQueue *_downQueue;
    AsyncSocket *_socket;
	
	id _delegate;
}
@property (nonatomic,retain) id delegate;
@property (nonatomic,retain) ASINetworkQueue *downQueue;

- (void)alertMsg:(NSString *)msg;

- (void)scoketConn;
- (void)scoketDisconn;

//下载图片
- (void)getImage:(NSString *)url delegate:(id)delegate_;

- (void)go;

//下载图片地址
- (NSString *)getImageWithLocal:(NSString *)url;

//重设下载队列
- (void)resetDownQueue;

//登录 SEND_LOGIN_TAG
- (void)sendLogin:(NSString *)user password:(NSString *)password;

//注销登录 SEND_LOGOUT_TAG
- (void)sendLogout;

- (void)disconn;

- (void)clearData;

//获取登录html代码 GET_LOGINHTML_TAG
- (void)getLoginHTML;

//登录 SEND_LOGINHTTPS_TAG
- (void)sendLoginHTTPS:(NSString *)user password:(NSString *)password;

//注销 SEND_LOGOUTHTTPS_TAG
- (void)sendLogoutHTTPS;

//获取用户信息 GET_MYUSRINFO_TAG
- (void)getMyUserInfo;

//修改用户资料 SEND_MODIFYMYUSRINFO_TAG
- (void)sendModifyMyUserInfo:(UserInfo_Builder *)myuserInfo;

//查看/取消查看实时地图 GET_MONITORVEHICLE_TAG
- (void)getMonitorVehicle:(long long)vehicle_id;

- (void)readDataRefresh;

//查看/取消查看实时地图 GET_CENCELMONITORVEHICLE_TAG
- (void)cencelMonitorVehicle:(long long)vehicle_id;

//查看历史轨迹 GET_QUERYHISTORY_TAG
- (void)getQueryHistory:(QueryHistory_Builder *)queryHistory;

//查看报警信息 GET_ALARM_TAG
- (void)getAlarm:(long long)vehicle_id alarm_id:(long long)alarm_id ;

//修改车辆 SEND_MODIFYCAR_TAG
- (void)sendModifyCar:(VehicleInfo *)vi;

//修改密码 SEND_MODIFYPASS_TAG
- (void)sendModifyPassword:(ChangePassword *)cp;

//是否关闭程序 GET_CLOSE_TAG
- (void)getIsClose;

@end
