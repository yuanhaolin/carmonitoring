//
//  STOrderHeader.h
//  SmartTrip
//
//  Created by Work on 12/17/13.
//  Copyright (c) 2013 PP. All rights reserved.
//

#ifndef SmartTrip_STOrderHeader_h
#define SmartTrip_STOrderHeader_h

//O=went表示去过，
//O=fav表示人气，
//O=pop表示想去，
//O=distance表示距离。

typedef enum _STOrderType {
    STOrderWent = 0,    //去过
    STOrderFav,         //人气
    STOrderPop,         //想去
    STOrderDistance,    //距离
}STOrderType;

#endif
