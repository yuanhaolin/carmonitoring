//
//  YWTFileTypeManager.h
//  YWTBusinessOL
//
//  Created by Work on 7/15/13.
//  Copyright (c) 2013 YY. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum _FileType {
    FileTypeString = 1,
    FileTypeMiniPhoto = 2,
    FileTypeImageJPG = 3,
    FileTypeImagePNG = 4,
    FileTypeCallingCard = 5,
    FileTypeWord = 6,
    FileTypeExcel = 7,
    FileTypeAMR = 8,
    FileTypeOther = 100,
}FileType;

@interface YWTFileTypeManager : NSObject

+ (NSString *)getFileType2String:(FileType)FileType;

+ (NSString *)getFileType2NameString:(FileType)FileType key:(NSString *)key;

@end
