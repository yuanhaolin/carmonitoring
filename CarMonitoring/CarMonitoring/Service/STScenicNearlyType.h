//
//  STScenicNearlyType.h
//  SmartTrip
//
//  Created by Work on 12/17/13.
//  Copyright (c) 2013 PP. All rights reserved.
//

#ifndef SmartTrip_STScenicNearlyType_h
#define SmartTrip_STScenicNearlyType_h

//0表示自然景观，
//1表示人文古迹，
//2表示城市风光，
//3表示乡村古镇，
//4表示度假休闲，
//5表示博物馆/展馆。

typedef enum STScenicNearlyType_ {
    STScenicNearlyTypeZRJG = 0,
    STScenicNearlyTypeRWGJ = 1,
    STScenicNearlyTypeCSFG = 2,
    STScenicNearlyTypeXCGZ = 3,
    STScenicNearlyTypeDJXX = 4,
    STScenicNearlyTypeBWG = 5,
    STScenicNearlyTypeALL = 6,
}STScenicNearlyType;

#endif
