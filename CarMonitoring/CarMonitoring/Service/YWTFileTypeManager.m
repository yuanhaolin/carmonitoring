//
//  YWTFileTypeManager.m
//  YWTBusinessOL
//
//  Created by Work on 7/15/13.
//  Copyright (c) 2013 YY. All rights reserved.
//

#import "YWTFileTypeManager.h"

@implementation YWTFileTypeManager

+ (NSString *)getFileType2String:(FileType)FileType {
    NSString *stringFileType = @"";
    switch (FileType) {
        case FileTypeCallingCard:
        {
            stringFileType = @"application/callingcard";
        }
            break;
        case FileTypeExcel:
        {
            stringFileType = @"application/vnd.ms-excel";
        }
            break;
        case FileTypeImageJPG:
        {
            stringFileType = @"image/jpeg";
        }
            break;
        case FileTypeImagePNG:
        {
            stringFileType = @"image/png";
        }
            break;
        case FileTypeMiniPhoto:
        {
            stringFileType = @"image/png";
        }
            break;
        case FileTypeOther:
        {
            stringFileType = @"image/otherfile";
        }
            break;
        case FileTypeString:
        {
            stringFileType = @"text/plain";
        }
            break;
        case FileTypeWord:
        {
            stringFileType = @"application/msword";
        }
            break;
        case FileTypeAMR:
        {
            stringFileType = @"audio/x-pn-realaudio";
        }
            break;
        default:
            break;
    }
    
    return stringFileType;
}

+ (NSString *)getFileType2NameString:(FileType)FileType key:(NSString *)key {
    NSString *stringFileType = @"";
    switch (FileType) {
        case FileTypeCallingCard:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.card",@"application/callingcard",key];
        }
            break;
        case FileTypeExcel:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.xls",@"application/vnd.ms-excel",key];
        }
            break;
        case FileTypeImageJPG:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.jpg",@"image/jpeg",key];
        }
            break;
        case FileTypeImagePNG:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.png",@"image/png",key];
        }
            break;
        case FileTypeMiniPhoto:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.png",@"image/png",key];
        }
            break;
        case FileTypeOther:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.other",@"image/png",key];
        }
            break;
        case FileTypeString:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.txt",@"text/plain",key];
        }
            break;
        case FileTypeWord:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.doc",@"application/msword",key];
        }
            break;
        case FileTypeAMR:
        {
            stringFileType = [NSString stringWithFormat:@"%@|%@.amr",@"audio/x-pn-realaudio",key];
        }
            break;
        default:
            break;
    }
    
    return stringFileType;
}

@end
