//
//  CMServiceManager.h
//  CarMonitoring
//
//  Created by Work on 1/14/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "NVParentViewController.h"
#import "Cm.pb.h"

@interface CMServiceManager : NVParentViewController {
    id _myDelegate;
}
@property (nonatomic,retain) id myDelegate;

+ (id)getInstance ;

//登录
- (void)login:(NSString *)name password:(NSString *)password;

//退出登录
- (void)logout;

- (void)disconn;

- (void)getMyUserInfo;

//修改用户资料
- (void)sendModifyMyUserInfo:(UserInfo_Builder *)myuserInfo;

//查看实时地图
- (void)getMonitorVehicle:(long long)vehicle_id_;

//取消查看实时地图
- (void)cancelMonitorVehicle:(long long)vehicle_id_;

//查看历史轨迹
- (void)getQueryHistory:(QueryHistory_Builder *)queryHistory;

//查看报警信息
- (void)getAlarm:(long long)vehicle_id alarm_id:(long long)alarm_id;

//修改车辆信息
- (void)sendModifyCar:(VehicleInfo *)vi;

//修改密码
- (void)sendModifyPassword:(ChangePassword *)cp;

- (void)readRefresh;

- (void)clearData;

@end
