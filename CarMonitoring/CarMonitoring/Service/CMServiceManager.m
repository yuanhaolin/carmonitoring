//
//  CMServiceManager.m
//  CarMonitoring
//
//  Created by Work on 1/14/14.
//  Copyright (c) 2014 PP. All rights reserved.
//

#import "CMServiceManager.h"

@interface CMServiceManager ()

@end

static CMServiceManager *_pServiceManager;
@implementation CMServiceManager
@synthesize myDelegate=_myDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_myDelegate release];
    [super dealloc];
}

+ (id)getInstance {
    @synchronized (self) {
        if (nil == _pServiceManager) {
            _pServiceManager = [[CMServiceManager alloc] init];
        }
    }
    
    return _pServiceManager;
}

- (void)login:(NSString *)name password:(NSString *)password {
//    NSString *pathTickid = doc_cache_path(@"ticketPath");
//    NSString *tickid = [NSString stringWithContentsOfFile:pathTickid encoding:NSUTF8StringEncoding error:nil];
//    if (nil == tickid) {
    [_service resetDownQueue];
    [_service scoketDisconn];
        [_service getLoginHTML];
        [_service go];
//    }
//    else {
//        [_service scoketConn];
//        [_service sendLogin:name password:password];
//    }
}

//退出登录
- (void)logout {
    [_service resetDownQueue];
    [_service sendLogout];
}

- (void)disconn {
    [_service disconn];
}

//修改用户资料
- (void)sendModifyMyUserInfo:(UserInfo_Builder *)myuserInfo {
    [_service sendModifyMyUserInfo:myuserInfo];
}

//查看/取消查看实时地图
- (void)getMonitorVehicle:(long long)vehicle_id_ {
    [_service getMonitorVehicle:vehicle_id_];
}

- (void)cancelMonitorVehicle:(long long)vehicle_id_ {
    [_service cencelMonitorVehicle:vehicle_id_];
}

- (void)getMyUserInfo {
    [_service getMyUserInfo];
}

//查看历史轨迹
- (void)getQueryHistory:(QueryHistory_Builder *)queryHistory {
    [_service getQueryHistory:queryHistory];
}

//查看报警信息
- (void)getAlarm:(long long)vehicle_id alarm_id:(long long)alarm_id {
    [_service getAlarm:vehicle_id alarm_id:alarm_id];
}

- (void)clearData {
    [_service clearData];
}

//修改车辆信息
- (void)sendModifyCar:(VehicleInfo *)vi {
    [_service sendModifyCar:vi];
}

//修改密码
- (void)sendModifyPassword:(ChangePassword *)cp {
    [_service sendModifyPassword:cp];
}

- (void)readRefresh {
    [_service readDataRefresh];
}

- (void)service:(id)service tag:(int)tag typename:(NSString *)typename_ entity:(id)entity userInfo:(NSDictionary *)userInfo {
    if (0 == [typename_ length]) {
        switch (tag) {
            case GET_LOGINHTML_TAG:
            {
                if (nil != entity) {
                    NSString *userNamePath = doc_cache_path(@"userNamePath");
                    NSString *userName = [NSString stringWithContentsOfFile:userNamePath encoding:NSUTF8StringEncoding error:nil];
                    
                    NSString *passwordPath = doc_cache_path(@"passwordPath");
                    NSString *password = [NSString stringWithContentsOfFile:passwordPath encoding:NSUTF8StringEncoding error:nil];
                    
                    [_service resetDownQueue];
                    [_service sendLoginHTTPS:userName password:password];
                    [_service go];
                }
            }
                break;
            case SEND_LOGINHTTPS_TAG:
            {
                if (nil != entity) {
                    NSString *userNamePath = doc_cache_path(@"userNamePath");
                    NSString *userName = [NSString stringWithContentsOfFile:userNamePath encoding:NSUTF8StringEncoding error:nil];
                    
                    NSString *passwordPath = doc_cache_path(@"passwordPath");
                    NSString *password = [NSString stringWithContentsOfFile:passwordPath encoding:NSUTF8StringEncoding error:nil];
                    
                    
                    [_service scoketConn];
                    [_service sendLogin:userName password:password];
                }
                else {
                    [self alertMsg:@"登录失败"];
                    [self showLoading:NO msg:@""];
                }
            }
                break;
            case GET_MYUSRINFO_TAG:
            {
                [self showLoading:NO msg:@""];
            }
                break;
            case SEND_MODIFYMYUSRINFO_TAG:
            {//修改用户资料
                if (nil == entity) {
                    [self alertMsg:@"修改失败"];
                }
            }
                break;
            case GET_QUERYHISTORY_TAG:
            {
                
            }
                break;
            default:
                break;
        }
    }
    else {
        if ([typename_ isEqualToString:@"RequestResponse "]) {
            
        }
    }
    
    if (nil != entity) {
        if ([_myDelegate respondsToSelector:@selector(service:tag:typename:entity:userInfo:)]) {
            [_myDelegate service:service tag:tag typename:typename_ entity:entity userInfo:userInfo];
        }
    }
    else {
        if ([_myDelegate respondsToSelector:@selector(serviceFaild:tag:typename:entity:userInfo:)]) {
            [_myDelegate serviceFaild:service tag:tag typename:typename_ entity:entity userInfo:userInfo];
        }
    }
}

- (void)serviceFaild:(id)service tag:(int)tag entity:(id)entity userInfo:(NSDictionary *)userInfo {
    
}

@end
